import moment from 'moment';

/**
 * Validate if a string is valid or not.
 *
 * @param {string} stringToTest - The string to validate.
 * @param {boolean} [allowEmpty=false] - If a empty string should be valid or not.
 * @param {boolean} allowNull - Returns true if the value is null
 * @returns {boolean} If the string is valid or not.
 */
export const isValidString = (stringToTest, allowEmpty = false, allowNull = false) => {
  const whiteSpace = /^[\s]$/;

  if (stringToTest === null && allowNull) return true;

  if (typeof stringToTest !== 'string') return false;

  if (stringToTest.length === 0 && allowEmpty === false) return false;

  if (whiteSpace.test(stringToTest)) return false;

  return true;
};

/**
 * Validate if a string is a valid number.
 *
 * @param {string} stringToTest - The string to validate as a number.
 * @param {boolean} [allowZero=false] - If the string should accept 0 as a valid number.
 * @param {boolean} [allowNegative=false] - If the string should negative values.
 * @returns {boolean} If the string is valid number or not.
 */
export const isValidNumber = (stringToTest, allowZero = false, allowNegative = false) => {
  const numberToTest = parseInt(stringToTest, 10);

  if (isNaN(numberToTest) || typeof numberToTest !== 'number') return false;

  if (numberToTest === 0 && allowZero === false) return false;

  if (numberToTest < 0 && allowNegative === false) return false;

  return true;
};

/**
 * Validate if a value is a valid number.
 *
 * @returns {boolean} If the value is valid number or not.
 * @param  {any} numberToTest The number To Test
 */
export const isValidNumberRegExp = (numberToTest) => {
  const re = /^[0-9]*$/;
  return re.test(numberToTest);
};

/**
 * Validate if a number is a valid integer.
 *
 * @param {number} numberToTest - The number to validate as integer.
 * @param {boolean} [allowZero=false] - If the number should accept 0 as a valid number.
 * @param {boolean} [allowNegative=false] - If the number should negative values.
 * @returns {boolean} If the number is a valid integer.
 */
export const isValidInteger = (numberToTest, allowZero = false, allowNegative = false) => {
  if (!Number.isInteger(numberToTest)) return false;

  if (numberToTest === 0 && allowZero === false) return false;

  if (numberToTest < 0 && allowNegative === false) return false;

  return true;
};

/**
 * Validate if the provided string is a valid email address.
 *
 * @param {string} email - String to validate as email.
 * @returns {boolean} Wether an email is valid or not.
 */
export const isValidEmail = (email) => {
  let emailExpression = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/;

  return emailExpression.test(email) ? true : false;
};

/**
 * Validate if the provided string is a valid Date.
 *
 * @param {string} date
 */

export const isValidDate = (date) => {
  return moment(date).isValid();
};

export const isValidPhoneNumber = (number) => {
  let phoneRegExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

  return phoneRegExp.test(number);
};
