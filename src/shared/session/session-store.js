import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a session
 *
 * @type {string}
 * @deprecated Replace for NEW_SESSION_EVENT
 */
export const SESSION_EVENT = 'onSession';
export const NEW_SESSION_EVENT = 'onSessionEvent';

/**
 * Event that triggers the acquisition of a Apollo Client Instance
 *
 * @type {string}
 */
export const APOLLO_CLIENT = 'onApolloCLient';
/**
 * Event that triggers a session error
 *
 * @type {string}
 */
export const SESSION_ERROR = 'onSessionError';

/**
 * Event that triggers a session error
 *
 * @type {string}
 */
export const USER_UPDATE_EVENT = 'onUserUpdated';
/**
 * Event that triggers a update error
 *
 * @type {string}
 */
export const USER_ERROR_EVENT = 'onUserUpdateError';

/**
 * Hold the Session Data
 */
class SessionStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(SESSION_EVENT);
    this.addEvent(SESSION_ERROR);
    this.addEvent(APOLLO_CLIENT);
    this.addEvent(USER_ERROR_EVENT);
    this.addEvent(NEW_SESSION_EVENT);
    this.addEvent(USER_UPDATE_EVENT);
  }
}

const sessionStore = new SessionStore();

export { sessionStore };

export default sessionStore;
