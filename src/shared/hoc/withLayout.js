import React from 'react';
import { Layout } from '../components';

const withLayout = WrappedComponent => props => {
  return (
    <Layout>
      <WrappedComponent {...props} />
    </Layout>
  );
};

export default withLayout;
