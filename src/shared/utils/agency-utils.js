import sessionStore, { NEW_SESSION_EVENT } from '../session/session-store';
import { META_AGENCY_SELECTED_NAME } from '../constants/meta';
import { updateMeta } from '../../modules/auth/auth.actions';
import Flux from '@cobuildlab/flux-state';
import { AGENT, MANAGER } from '../constants/index';
import { AGENCY_SET_ACTIVE_EVENT } from '../../modules/agency/agency-store';
import * as toast from 'shared/components/toast/Toast';

/**
 * Get active agency.
 *
 * @returns {object} agency
 */
export const getActiveAgency = () => {
  const sessionData = sessionStore.getState(NEW_SESSION_EVENT);

  if (!sessionData) return null;

  const agencies = getUserAgencies();
  const metaList = sessionData.user.metaUserRelation.items;
  const meta = metaList.find((meta) => meta.name === META_AGENCY_SELECTED_NAME);
  let agencyId;

  if (agencies.length === 0 && meta === undefined) return null;

  // Set agencyId from database.
  if (meta !== undefined) {
    // Here validate if the agency selected still belong to the current user.
    const validAgency = validateUserAgency(meta.value, agencies);

    if (validAgency) {
      agencyId = meta.value;
    }
  }

  // Here there is no agency selected, then select the first agency.
  if (agencyId === undefined && agencies.length > 0) {
    agencyId = agencies[0].id;

    setActiveAgency(agencyId);
  }

  const agency = agencies.find((agency) => agency.id === agencyId);

  return agency;
};

/**
 * Set active agency.
 *
 * @param {string} agencyId agency ID
 * @returns {Function} promise
 */
export const setActiveAgency = (agencyId) => {
  return updateMeta(META_AGENCY_SELECTED_NAME, agencyId).then(() => {
    Flux.dispatchEvent(AGENCY_SET_ACTIVE_EVENT);
  });
};

/**
 * Get the agencies of the current user.
 *
 * @returns {Array} agencies
 */
export const getUserAgencies = () => {
  const sessionData = sessionStore.getState(NEW_SESSION_EVENT);
  const user = sessionData ? sessionData.user : null;
  let agencies = [];

  if (user === null) return agencies;

  const agenciesPerRelation = [
    user.userAgentRelation.items.map((userAgent) => userAgent.agency),
    user.userManagerRelation.items.map((userManager) => userManager.agency),
  ];

  // TODO: avoid duplicates agencies
  agenciesPerRelation.forEach((agenciesRelation) => {
    agencies = agencies.concat(agenciesRelation);
  });

  return agencies;
};

/**
 * Extracts the Role of a User on an Agency, null if it does not have one
 *
 * @param {object} user The User
 * @param {object} agency The selected Agency
 * @returns {string | null} user role
 */
export const getRolesOnSelectedAgency = (user, agency) => {
  if (!user || !agency) return null;
  if (user.userAgentRelation.items.length > 0) {
    if (user.userAgentRelation.items.find((relation) => relation.agency.id === agency.id)) {
      return AGENT;
    }
  }
  if (user.userManagerRelation.items.length > 0) {
    if (user.userManagerRelation.items.find((relation) => relation.agency.id === agency.id)) {
      return MANAGER;
    }
  }
  return null;
};

/**
 * Returns true if the User has the role or one of the roles
 * in the provided Agency
 *
 * @param {object} user The user
 * @param {object} agency The Agency
 * @param {string|string[]} role Role name or array or Role names
 * @returns {boolean} if the user has the role or one of the roles in the agency
 */
export const userHasAnyRoleInAgency = (user, agency, role) => {
  const userRole = getRolesOnSelectedAgency(user, agency);

  if (!userRole) return false;

  if (Array.isArray(role)) {
    return role.includes(userRole);
  }

  return role === userRole;
};

/**
 * Validate if the agency selected still belong to the current user.
 *
 * @param {string} agencyId agency ID
 * @param {Array} userAgencies list of user agencies
 * @returns {boolean} the agency belong to the user
 */
export const validateUserAgency = (agencyId, userAgencies) => {
  const exists = !!userAgencies.find((userAgency) => userAgency.id === agencyId);

  if (!exists) {
    const firstAgency = userAgencies[0];

    toast.warn(`
      The selected Agency don't belong to this user.
      ${firstAgency ? `The first agency ${firstAgency.name} will do selected automatically.` : ''}
    `);
  }

  return exists;
};
