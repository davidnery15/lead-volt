export const fileToBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

/**
 * Helper to remove null keys from objects
 * WARNING: This function
 * mutates the data
 *
 * @param {data} object to mutate
 * @param data
 * @param key
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseNonNull = (data, key) => {
  if (data[key] === null) delete data[key];
};

export const sanitize8BaseEmptyFields = (data) => {
  Object.keys(data).forEach((key) => {
    const value = data[key];

    if (
      value === null ||
      value === undefined ||
      value === '' ||
      (Array.isArray(value) && value.length === 0)
    ) {
      delete data[key];
    }
  });
};

/**
 * Helper to change non null keys to 8base 'connect' reference
 * WARNING: This function mutates the data
 *
 * @param {data} object to mutate
 * @param data
 * @param key
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseReference = (data, key) => {
  if (data[key] !== undefined && data[key] !== null && data[key] !== '')
    data[key] = { connect: { id: data[key] } };
  else delete data[key];
};

/**
 * Helper to change Array keys to 8base 'connect' multiple references
 * WARNING: This function mutates the data
 *
 * @param data
 * @param key
 */
export const sanitize8BaseReferences = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      connect: data[key].map((id) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper function to non null objects to 8base 'connect' reference
 * WARNING: This function mutates the data.
 *
 * @param {object} data - The data to mutate.
 * @param {string} key - The property to mutate.
 */
export const sanitize8BaseReferenceFromObject = (data, key) => {
  if (data[key] !== null && data[key] !== '' && data[key] !== undefined) {
    if (data[key].id === undefined)
      throw new Error(
        `${key} property on 'data' does not have a field 'id' so is not possible to create the 'connect'`,
      );
    data[key] = { connect: { id: data[key].id } };
  } else delete data[key];
};

/*
 * @param property
 * @param {property} string the key of the property to be sorted
 * include "-" in front of the property to sort in decreasing mode
 */
export const dynamicSort = (property) => {
  let sortOrder = 1;
  if (property[0] === '-') {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function(a, b) {
    /*
     * next line only works with strings and numbers
     */
    const result = a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
    return result * sortOrder;
  };
};

/**
 * Helper to change Array keys to 8base 'reconnect' multiple references
 * WARNING: This function mutates the data
 *
 * @param {data} object to mutate
 * @param data
 * @param key
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseReconnects = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      reconnect: data[key].map((id) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference for Documents
 * WARNING: This function mutates the data
 *
 * @param {data} object to mutate
 * @param data
 * @param key
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDocumentUpdate = (data, key) => {
  if (data[key] !== null && data[key].downloadUrl === undefined) data[key] = { create: data[key] };
  else delete data[key];
};
