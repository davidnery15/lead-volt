import { AGENCY_INVITATION_PENDING } from '../constants/agency-invitations';
import { COMPANY_INVITATION_PENDING } from '../constants/company-invitations';

/**
 * get agency and company pending invitations
 *
 * @param {object} agencyInvitationsList agency invitations list
 * @param {object} companyInvitationsList company invitations list
 * @returns {Array} pending invitations
 */
export const getPendingInvitations = (agencyInvitationsList, companyInvitationsList) => {
  const { items: agencyInvitations } = agencyInvitationsList;
  const { items: companyInvitations } = companyInvitationsList;

  const pendingAgencyInvitations = agencyInvitations.filter(
    ({ status }) => status === AGENCY_INVITATION_PENDING,
  );

  const pendingCompanyInvitations = companyInvitations.filter(
    ({ status }) => status === COMPANY_INVITATION_PENDING,
  );

  return [pendingAgencyInvitations, pendingCompanyInvitations];
};
