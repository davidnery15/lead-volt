import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@8base/boost';
import { Link, withRouter } from 'react-router-dom';
import styled from 'styled-components';

export const routes = [
  {
    name: 'Dashboard',
    path: '/dashboard',
    iconUrl: require('../assets/images/dashboard.svg'),
  },
  {
    name: 'Lead Managment',
    path: '/leads',
    iconUrl: require('../assets/images/leads.svg'),
  },
  {
    name: 'Campaigns',
    path: '/campaigns',
    iconUrl: require('../assets/images/campaigns.svg'),
  },
  {
    name: 'Tasks',
    path: '/tasks',
    iconUrl: require('../assets/images/tasks.svg'),
  },
  {
    name: 'Performance & Reports',
    path: '/performance',
    iconUrl: require('../assets/images/performance.svg'),
  },
  {
    name: 'Call',
    path: '/call',
    iconUrl: require('../assets/images/call.svg'),
  },
  {
    name: 'My Account / Settings',
    path: '/settings/general',
    iconUrl: require('../assets/images/settings.svg'),
  },
  {
    name: 'Design System',
    path: '/design-system',
    iconName: 'Schema',
  },
];

const MenuContainer = styled.div`
  height: 100vh;
  width: 56px;
  background-color: var(--color-blue-3);
  display: flex;
  justify-content: center;
  flex-direction: row;
  position: fixed;
  left: 0;
`;

const List = styled.ul`
  width: 100%;
  list-style: none;
`;

const StyledLink = styled(Link)`
  height: 56px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0.5px;
    left: 0px;
    width: 2.5px;
    height: 55px;
    border-radius: 25%;
    ${(props) => (props.active ? 'background-color: white;' : '')}
  }
`;

const Li = styled.li`
  background-color: ${(props) => (props.active ? 'var(--color-primary)' : '')};

  &:first-child {
    height: 56px;
    background-color: #3d4751;
    border-bottom: 0.1px solid #434e56;
    display: flex;
    align-items: center;
  }

  &:first-child:hover {
    border-left: 0px;
    background-color: #3d4751;
  }

  &:hover {
    background-color: var(--color-primary);
  }

  &:hover ${StyledLink}::before {
    background-color: white;
  }
`;

const Image = styled.img`
  width: 100%;
  height: 30px;
  object-fit: contain;
  max-width: 100%;
`;

const Logo = styled.img`
  position: absolute;
  bottom: 30px;
  object-fit: contain;

  @media screen and (max-height: 655px) {
    display: none;
  }
`;

const StyledIcon = styled(Icon)`
  color: white;
  width: 100%;
  height: 30px;
`;

const Menu = ({ history }) => {
  const [pathnameActive, setPathnameActive] = useState('/dashboard');

  history.listen((location, action) => {
    const { pathname } = location;

    setPathnameActive(pathname);
  });

  return (
    <MenuContainer>
      <List>
        <Li>
          <Image alt="Lead Volt" src={require('../assets/images/leadvolt-lightning.svg')} />
        </Li>
        {routes.map((route) => {
          const isActive = pathnameActive.startsWith(route.path);

          return (
            <Li key={route.path} active={isActive}>
              <StyledLink to={route.path} active={isActive}>
                {route.iconUrl ? (
                  <Image src={route.iconUrl} alt={route.name} />
                ) : (
                  <StyledIcon name={route.iconName} size="lg" />
                )}
              </StyledLink>
            </Li>
          );
        })}
      </List>
      <Logo alt="Lead Volt Logo" src={require('../assets/images/sidebar-logo.svg')} />
    </MenuContainer>
  );
};

Menu.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Menu);
