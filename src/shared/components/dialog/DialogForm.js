import React from 'react';
import PropTypes from 'prop-types';
import { Dialog, Button } from '@8base/boost';
import { PrimaryBtn } from '../ui/buttons/PrimaryBtn';

const DialogForm = ({
  title,
  isOpen,
  createText,
  children,
  onCloseDialog,
  onClickCreate,
  isLoading,
  size,
}) => {
  return (
    <Dialog size={size} isOpen={isOpen}>
      <Dialog.Header title={title} onClose={onCloseDialog} />
      <Dialog.Body>{children}</Dialog.Body>
      <Dialog.Footer>
        <Button color="neutral" onClick={onCloseDialog}>
          Cancel
        </Button>
        <PrimaryBtn loading={isLoading} onClick={onClickCreate}>
          {createText}
        </PrimaryBtn>
      </Dialog.Footer>
    </Dialog>
  );
};

DialogForm.defaultProps = {
  size: 'lg',
  createText: 'Create',
  isLoading: false,
};

DialogForm.propTypes = {
  size: PropTypes.string,
  createText: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  onClickCreate: PropTypes.func.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};

export default DialogForm;
