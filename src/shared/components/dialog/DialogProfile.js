import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { AuthBtn } from '../ui/buttons/AuthBtn';
import { Avatar, Grid, Row } from '@8base/boost';
import { Link } from 'react-router-dom';
import Dropdown from '../ui/dropdowns/Dropdown';
import { Status, StatusText, StatusCircle } from '../UserStatus';
import { TextSpacing } from '../ui/text/TextSpacing';
import Loader from '../ui/Loader';
import {
  createUserStatus,
  fetchUserStatuses,
} from '../../../modules/user-status/user-status-actions';
import { useSubscription } from '@cobuildlab/react-flux-state';
import userStatusStore, {
  USER_STATUS_LIST_EVENT,
  USER_STATUS_CREATE_EVENT,
  USER_STATUS_ERROR_EVENT,
} from '../../../modules/user-status/user-status-store';
import { onErrorMixinFC } from '../../mixins';

const Dialog = styled.div`
  position: absolute;
  top: 70px;
  right: 25px;
  background-color: white;
  border-radius: 5px;
  box-sizing: border-box;
  border: 1px solid #d0d7dd;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.04);
  z-index: 1000;
`;

const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid #d0d7dd;
  padding: 16px;
`;

const Body = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-bottom: 1px solid #d0d7dd;
  padding: 12px 16px;
`;

const Footer = styled.div`
  padding: 16px;
  display: flex;
  flex-direction: column;
  text-align: left;
  justify-content: flex-start;
  align-items: flex-start;
`;

const Name = styled.p`
  font-weight: bold;
  font-size: 12px;
`;

const Email = styled.p`
  color: var(--color-primary);
  font-size: 12px;
`;

const JobTitle = styled.p`
  color: #9b9b9b;
  font-size: 12px;
`;

const StyledLink = styled(Link)`
  font-size: 12px;
  text-decoration: none;
  color: #000;
  margin-bottom: 8px;
`;

const DropdownOption = ({ text, isBusy, color }) => (
  <Status>
    <StatusCircle isBusy={isBusy} color={color} />
    <StatusText>{text}</StatusText>
  </Status>
);

DropdownOption.propTypes = {
  isBusy: PropTypes.bool,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

DropdownOption.defaultProps = {
  isBusy: false,
};

const OnCallOption = () => (
  <DropdownOption name="on_call" text="On a Call" isBusy color="#FF0000" />
);
const AwayOption = () => <DropdownOption name="away" text="Away" color="#1E4358" />;
const ActiveOption = () => <DropdownOption name="active" text="Active" color="#3DB4AA" />;
const WithClientOption = () => (
  <DropdownOption name="with_client" text="With Client" color="#4993EA" />
);
const WrappingCallOption = () => (
  <DropdownOption name="wrapping_call" text="Wrapping up a call" color="#F6C01E" />
);

const statusOptions = [
  {
    value: 'ON A CALL',
    Component: OnCallOption,
  },
  {
    value: 'AWAY',
    Component: AwayOption,
  },
  {
    value: 'ACTIVE',
    Component: ActiveOption,
  },
  {
    value: 'WITH CLIENT',
    Component: WithClientOption,
  },
  {
    value: 'WRAPPING ON A CALL',
    Component: WrappingCallOption,
  },
];

const DialogProfile = ({ user, isOpen, activeAgency }) => {
  const [statusOption, setStatusOption] = useState('');
  const handleOnChangeStatus = (name) => {
    createUserStatus({
      status: name.toUpperCase(),
      user: user.id,
    });
  };
  let isAdmin, isManager;

  if (activeAgency) {
    isAdmin =
      user &&
      user.userAdminRelation.items.find((item) => item.company.id === activeAgency.company.id);
    isManager =
      user && user.userManagerRelation.items.find((item) => item.agency === activeAgency.id);
  }

  useEffect(() => {
    if (user && statusOption === '') {
      fetchUserStatuses({
        filterData: {
          user: user.id,
        },
      });
    }
  }, [user, statusOption]);

  useSubscription(userStatusStore, USER_STATUS_LIST_EVENT, (data) => {
    const userStatuses = data.userStatusesList.items;

    if (userStatuses.length > 0) {
      // Get last user status.
      setStatusOption(userStatuses[userStatuses.length - 1].status);
    }
  });
  useSubscription(userStatusStore, USER_STATUS_CREATE_EVENT, (data) => {
    setStatusOption(data.userStatusCreate.status);
  });
  useSubscription(userStatusStore, USER_STATUS_ERROR_EVENT, onErrorMixinFC);

  return (
    <>
      {isOpen ? (
        <Dialog>
          {user ? (
            <>
              <Header>
                <Grid.Layout inline columns="auto auto">
                  <Row alignItems="center">
                    <Avatar
                      size="lg"
                      firstName={user.firstName}
                      lastName={user.lastName}
                      src={user.avatar ? user.avatar.downloadUrl : ''}
                      style={{
                        width: '60px',
                        height: '60px',
                        marginRight: '15px',
                      }}
                    />
                  </Row>
                  <Grid.Box style={{ justifyContent: 'center' }}>
                    <>
                      <Name>
                        {user.firstName} {user.lastName}
                      </Name>
                      {isAdmin ? (
                        <>
                          <JobTitle>Admin</JobTitle>
                          <JobTitle>{user.userAdminRelation.items[0].company.name}</JobTitle>
                        </>
                      ) : (
                        <>
                          <JobTitle>{isManager ? 'Manager' : 'Agent'}</JobTitle>
                          {activeAgency && <JobTitle>{activeAgency.company.name}</JobTitle>}
                        </>
                      )}
                      <Email>{user.email}</Email>
                    </>
                  </Grid.Box>
                </Grid.Layout>
              </Header>
              <Body>
                <TextSpacing>CHANGE STATUS</TextSpacing>
                <Dropdown
                  onChange={handleOnChangeStatus}
                  placeholder="Select an status"
                  currentOption={statusOption}
                  emptyLabel="No more status"
                  options={statusOptions}
                />
              </Body>
              <Footer>
                <StyledLink to="/settings">My Account</StyledLink>
                <AuthBtn />
              </Footer>
            </>
          ) : (
            <Loader />
          )}
        </Dialog>
      ) : null}
    </>
  );
};

DialogProfile.propTypes = {
  activeAgency: PropTypes.object,
  user: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

DialogProfile.defaultProps = {
  activeAgency: null,
};

export default DialogProfile;
