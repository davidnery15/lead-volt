import React from 'react';
import { Label, Row } from '@8base/boost';
import { default as Tag } from 'react-tagsinput';
import { PropTypes } from 'prop-types';
import { IconAskInfo } from '../ui/Icons';
import './tag-input.css';

const TagsInput = ({ onChange, tags, placeholder, label }) => {
  const inputProps = {
    placeholder,
  };

  return (
    <>
      <Row alignItems="center">
        <Label kind={'secondary'} text={label} />
        <IconAskInfo />
      </Row>
      <Tag
        style={{ height: '36px', width: '100%' }}
        value={tags}
        onChange={(array) => onChange([...new Set(array)])}
        inputProps={inputProps}
      />
    </>
  );
};

TagsInput.defaultProps = {
  label: '',
  placeholder: '',
};

TagsInput.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  tags: PropTypes.array.isRequired,
};
export { TagsInput };
