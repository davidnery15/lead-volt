import React from 'react';
import { Card, Column, Text } from '@8base/boost';
import { PropTypes } from 'prop-types';
import moment from 'moment';
import styled from 'styled-components';

const PointerCard = styled(Card)`
  margin: 11px !important;
  cursor: pointer;
  width: 60px;
  height: 108px;
  border: 1px solid ${({ active }) => (active ? '#3DB4AA' : '#EAEFF3')};
  border-radius: 8px;
  background-color: #ffffff;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.06);
`;

const Bar = styled.div`
  width: ${({ width }) => width};
  height: ${({ width }) => width};
  border-radius: 15px;
  background-color: ${({ active }) => (active ? '#3DB4AA' : '')};
  margin: 0 0 7px 0 !important;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const DayComponent = ({ points, active, weekday, date, day, width, onClick }) => {
  const dateToSelect = moment(date).format('YYYY-MM-DD');
  const pointsBackgrounds = ['#3DB4AA', '#F6C01E', '#F46800', '#FF0000', '#9B9B9B'];

  return (
    <PointerCard active={active} onClick={() => onClick(dateToSelect)}>
      <Card.Body style={{ padding: '5px' }}>
        <Column alignItems="center">
          <Text
            width="bold"
            style={{
              color: active ? '#3DB4AA' : '#A5A4A5',
              fontWeight: 600,
              marginTop: '6px',
              marginBottom: '2px',
            }}>
            {weekday}
          </Text>
          <Bar width={width} active={active}>
            <Text
              style={{
                fontSize: '14px',
                color: active ? '#FFFFFF' : '#384A59',
                fontWeight: 600,
              }}>
              {day}
            </Text>
          </Bar>
          {points.length > 0
            ? points.map((point, index) => (
              <div
                key={index}
                style={{
                  height: '5px',
                  width: '5px',
                  borderRadius: '2.5px',
                  marginBottom: '5px',
                  backgroundColor: pointsBackgrounds[point],
                }}
              />
            ))
            : null}
        </Column>
      </Card.Body>
    </PointerCard>
  );
};

DayComponent.defaultProps = {
  width: '30px',
  points: 0,
};

DayComponent.propTypes = {
  points: PropTypes.number,
  width: PropTypes.string,
  date: PropTypes.string.isRequired,
  weekday: PropTypes.string.isRequired,
  day: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default DayComponent;
