import React from 'react';
import { PropTypes } from 'prop-types';
import DayComponent from './DayComponent';
import rightArrow from '../../assets/images/right-arrow.svg';
import leftArrow from '../../assets/images/left-arrow.svg';

/**
 * @param week : array with data of week
 * @param selectedDay : day selected for see active items
 * @param onRightClick : rotate week to right
 * @param onLeftClick : rotate week to left
 * @param onSelectDay
 * @returns {*}
 * @class
 */
const WeekComponent = ({ week, selectedDay, onRightClick, onLeftClick, onSelectDay }) => {
  return (
    <>
      <img
        onClick={onLeftClick}
        src={leftArrow}
        style={{ width: '16px', cursor: 'pointer' }}
        alt="left"
      />
      {week.map((day, key) => {
        const active = day.fullDate === selectedDay;

        return (
          <DayComponent
            key={key}
            onClick={onSelectDay}
            day={day.day}
            active={active}
            weekday={day.weekday}
            date={day.fullDate}
            points={active ? [0, 3, 1] : [4]}
          />
        );
      })}
      <img
        onClick={onRightClick}
        src={rightArrow}
        style={{ width: '16px', cursor: 'pointer' }}
        alt="right"
      />
    </>
  );
};

WeekComponent.propTypes = {
  week: PropTypes.array.isRequired,
  selectedDay: PropTypes.string.isRequired,
  onLeftClick: PropTypes.func.isRequired,
  onRightClick: PropTypes.func.isRequired,
  onSelectDay: PropTypes.func.isRequired,
};

export default WeekComponent;
