import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import styled from 'styled-components';
import { FaTimes } from 'react-icons/fa';
import { fileToBase64 } from '../../shared/utils';
import { Text, Loader } from '@8base/boost';

const Dropzone = styled.div`
	width: 100%;
	height: 124px;
  display: flex;
  cursor: pointer;
  position: relative;
	border-radius: 5px;
  align-items: center;
	box-sizing: border-box;
  justify-content: center;
  background-color: #FFFFFF;
	border: 1px dashed #D0D7DD;
`;

const Label = styled.p`
	color: #878C93;
	font-size: 12px;
  line-height: 16px;
  margin-bottom: 5px;
	font-family: 'Poppins';
`;

const IconUpload = styled.img`
  width: 63px;
  height: 58px;
  color: #E5E5E5;
  max-width: 100%;
  object-fit: contain;
`;

const ImageContainer = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 200px;
  height: 100px;
  max-width: 100%;
  object-fit: contain;
`;

const IconClose = styled(FaTimes)`
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
`;

const TextDanger = styled(Text)`
  margin-top: 5px;
`;

const ImageUpload = ({
  label,
  accept,
  maxFiles,
}) => {
  const [files, setFiles] = useState([]);
  const [message, setMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const onDrop = useCallback(acceptedFiles => {
    // eslint-disable-next-line
    if (acceptedFiles.length <= maxFiles) {
      const newFiles = [];

      setIsLoading(true);
      acceptedFiles.forEach(async file => {
        const base64 = await fileToBase64(file);

        newFiles.push({
          ...file,
          preview: base64,
        });
      });

      setMessage('');
      setFiles(newFiles);
    } else {
      setMessage(`Only accept ${maxFiles} file${maxFiles > 1 ? 's' : ''}`);
    }
    // eslint-disable-next-line
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop, accept });
  const handleOnClickClose = file => {
    setFiles(files.filter(_file => file.preview !== _file.preview));
    setIsLoading(false);
  };

  return (
    <>
      <Label>{label}</Label>
      {files.length ? (
        <Dropzone>
          {files.map(file => {
            return (
              <ImageContainer key={file.path}>
                <Image src={file.preview} alt={file.path} />
                <IconClose onClick={() => handleOnClickClose(file)} />
              </ImageContainer>
            );
          })}
        </Dropzone>
      ) : (
        <Dropzone {...getRootProps()}>
          <input {...getInputProps()} />
          {isLoading ? <Loader size="sm" /> : <IconUpload src={require('../../shared/assets/images/upload.svg')} alt="Upload" />}
        </Dropzone>
      )}
      {message !== '' && <TextDanger color="DANGER">{message}</TextDanger>}
    </>
  );
};

ImageUpload.propTypes = {
  label: PropTypes.string.isRequired,
  accept: PropTypes.array.isRequired,
  maxFiles: PropTypes.number.isRequired,
};

export default ImageUpload;
