import React from 'react';
import PropTypes from 'prop-types';
import TopBar from './TopBar';
import Menu from './Menu';
import styled from 'styled-components';
import Breadcrumbs from './Breadcrumbs';

const LayoutContainer = styled.div`
  display: flex;
  position: relative;
`;

const LayoutContent = styled.div`
  width: 100%;
  margin-top: 56px;
  padding-left: 56px;
`;

const Layout = ({ children }) => {
  return (
    <LayoutContainer>
      <TopBar />
      <Menu />
      <LayoutContent>
        <Breadcrumbs />
        {children}
      </LayoutContent>
    </LayoutContainer>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
