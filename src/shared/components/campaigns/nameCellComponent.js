import React from 'react';
import CustomLink from '../ui/CustomLink';

export const nameCellComponent = (rowData, value) => {
  const component = () => <CustomLink to={`/campaigns/${rowData.id}`}>{value}</CustomLink>;

  return component;
};
