import React from 'react';
import styled from 'styled-components';
import { Avatar } from '@8base/boost';
import { StatusCircle } from './UserStatus';

const AvatarWithStatusWrap = styled.div`
  position: relative;
`;

const AvatarStatus = styled.div`
  position: absolute;
  right: 12px;
  bottom: 2px;
`;

const AvatarWithStatus = (props) => {
  return (
    <AvatarWithStatusWrap>
      <Avatar {...props} />
      <AvatarStatus>
        <StatusCircle isBusy color="#FF0000" />
      </AvatarStatus>
    </AvatarWithStatusWrap>
  );
};

export default AvatarWithStatus;
