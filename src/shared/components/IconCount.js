import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Icon } from '@8base/boost';

const IconCountWrap = styled.div`
  cursor: pointer;
  position: relative;
  display: inline-block;
  margin-left: 5px;
  margin-right: 16px !important;
`;

const Span = styled.span`
  color: var(--color-primary);
  position: absolute;
  font-size: 14px;
  line-height: 28px;
`;

const Image = styled.img`
  object-fit: contain;
  max-width: 100%;
`;

const IconCount = ({ alt, count, imgUrl, iconName, countStyle, ...props }) => {
  return (
    <IconCountWrap>
      {count > 0 ? <Span style={countStyle}>{count}</Span> : null}
      {imgUrl !== '' ? (
        <Image src={imgUrl} alt={alt} {...props} />
      ) : (
        <Icon name={iconName} size="md" {...props} />
      )}
    </IconCountWrap>
  );
};

IconCount.propTypes = {
  imgUrl: PropTypes.string,
  iconName: PropTypes.string,
  alt: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  countStyle: PropTypes.object.isRequired,
};

IconCount.defaultProps = {
  iconName: '',
  imgUrl: '',
};

export default IconCount;
