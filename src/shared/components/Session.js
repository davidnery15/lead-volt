import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { withApollo } from 'react-apollo';
import Flux from '@cobuildlab/flux-state';
import { log } from '@cobuildlab/pure-logger';
import { withRouter } from 'react-router-dom';
import View from '@cobuildlab/react-flux-state';
import { withAuth } from '@8base/react-sdk';
import sessionStore, {
  SESSION_ERROR,
  APOLLO_CLIENT,
  NEW_SESSION_EVENT,
} from '../session/session-store';
import { fetchSession, createDefaultMeta, updateMeta } from '../../modules/auth/auth.actions';
import { NUMBER_OF_ONBOARDING_SCREENS } from '../../modules/onboarding/screens';
import MainLoader from './ui/MainLoader';
import { META_ONBOARDING_STEP_NAME } from '../../shared/constants/meta';
import agencyStore, { AGENCY_SET_ACTIVE_EVENT } from '../../modules/agency/agency-store';

/**
 * User Session component
 */
class Session extends View {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      sessionRequired: true,
      apolloRequired: true,
      sessionError: false,
    };
  }

  async componentDidMount() {
    this.subscribe(sessionStore, SESSION_ERROR, async (e) => {
      log('SESSION_ERROR:message', e.message);
      setTimeout(async () => {
        this.setState({ sessionError: true });
      }, 5000);
    });

    this.subscribe(sessionStore, APOLLO_CLIENT, async () => {
      this.setState({ apolloRequired: false }, fetchSession);
    });

    this.subscribe(sessionStore, NEW_SESSION_EVENT, async (session) => {
      log('NEW_SESSION_EVENT', session);
      const { user } = session;
      const { userAgentRelation, userManagerRelation } = user;
      const metaList = user.metaUserRelation.items;

      let step = metaList.find((meta) => meta.name === META_ONBOARDING_STEP_NAME);
      log('STEP', step);

      // Is a New USER
      if (step === undefined) {
        await createDefaultMeta();
        return await fetchSession();
      }

      const stepValue = parseInt(step.value);

      if (stepValue < NUMBER_OF_ONBOARDING_SCREENS) {
        // skip the onboarding conditions here
      }

      // restart the onboarding if the user has no agencies
      if (stepValue > 0 && !userAgentRelation.count && !userManagerRelation.count) {
        await updateMeta(META_ONBOARDING_STEP_NAME, 0);
        return await fetchSession();
      }

      this.setState({
        isLoading: false,
        sessionRequired: false,
      });
    });

    this.subscribe(agencyStore, AGENCY_SET_ACTIVE_EVENT, () => {
      fetchSession();
    });

    setTimeout(() => Flux.dispatchEvent(APOLLO_CLIENT, this.props.client), 1000);
  }

  render() {
    const { apolloRequired, sessionRequired, sessionError, isLoading } = this.state;
    const { history } = this.props;

    if (sessionError && (apolloRequired || sessionRequired)) {
      return this.props.children;
    }

    if (apolloRequired || sessionRequired || isLoading) return <MainLoader />;

    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    const metaList = user.metaUserRelation.items;
    const step = metaList.find((meta) => meta.name === META_ONBOARDING_STEP_NAME);

    if (step === undefined)
      throw new Error(
        `User data must have a Meta value named: ${META_ONBOARDING_STEP_NAME} Maybe the User hasn't been properly created`,
      );

    if (parseInt(step.value) < NUMBER_OF_ONBOARDING_SCREENS) {
      if (!history.location.pathname.includes('/onboarding/'))
        return <Redirect to={`/onboarding/${step.value}`} />;
    }

    return this.props.children;
  }
}

Session.propTypes = {
  client: PropTypes.any.isRequired,
  children: PropTypes.any.isRequired,
};

export default withRouter(withAuth(withApollo(Session)));
