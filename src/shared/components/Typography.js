import styled from 'styled-components';

export const H1 = styled.h1`
  font-family: 'Poppins';
  font-weight: 500;
  font-size: 32px;
  line-height: 48px;
  letter-spacing: 0.5;
`;

export const H2 = styled.h2`
  font-family: 'Poppins';
  font-weight: 400;
  font-size: 18px;
  line-height: 28px;
  letter-spacing: 0.5;
`;

export const H3 = styled.h3`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.2;
`;

export const H4 = styled.h4`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.5;
`;

export const SubTitle = styled.p`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.5;
`;

export const Body = styled.p`
  font-family: 'Poppins';
  font-weight: 400;
  font-size: 13px;
  line-height: 20px;
  letter-spacing: 0.2;
`;

export const Button = styled.span`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.2;
`;

export const Link = styled.a`
  font-family: 'Poppins';
  font-weight: 400;
  font-size: 13px;
  line-height: 20px;
  letter-spacing: 0.2;
  color: var(--color-blue-light);
`;

export const Overline = styled.p`
  font-family: 'Poppins';
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  letter-spacing: 0.5;
`;

export const OverlineBold = styled.p`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  letter-spacing: 0.5;
  text-transform: uppercase;
`;

export const Small = styled.span`
  font-family: 'Poppins';
  font-weight: 500;
  font-size: 10px;
  line-height: 16px;
  letter-spacing: 0.2;
`;

export const SmallUpper = styled.span`
  font-family: 'Poppins';
  font-weight: 500;
  font-size: 10px;
  line-height: 16px;
  letter-spacing: 0.2;
  text-transform: uppercase;
`;
