import React from 'react';
import { Paper, Heading } from '@8base/boost';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import CustomLink from './ui/CustomLink';
import Margin from './ui/Margin';

const StyledHeading = styled(Heading)`
  color: #3db4aa;
  font-size: 60px;
  line-height: 70px;
  font-family: 'Poppins';
`;

const Text = styled.p`
  color: #323c47;
  font-family: 'Poppins';
  font-size: 16px;
  font-weight: 600;
  line-height: 22px;
`;

const CardWrap = styled.div`
  padding: 0px 24px;
  height: 200px;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
`;

const CardIndicator = ({ title, text, link }) => {
  return (
    <Paper>
      <CardWrap>
        <StyledHeading>{title}</StyledHeading>
        <Text>{text}</Text>
        <Margin top="6px">
          <CustomLink to={link.path}>{link.name}</CustomLink>
        </Margin>
      </CardWrap>
    </Paper>
  );
};

CardIndicator.propTypes = {
  link: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default CardIndicator;
