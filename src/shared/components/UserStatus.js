import styled from 'styled-components';

export const Status = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0px 17px;
`;

export const StatusText = styled.div`
  font-size: 12px;
  color: #384a59;
`;

export const StatusCircle = styled.div`
  width: 15px;
  height: 15px;
  border: 1px solid #efefef;
  border-radius: 100%;
  background-color: ${(props) => props.color};
  margin-right: 5px;
  position: relative;

  ${(props) =>
    props.isBusy
      ? `
    &::before {
      content: '';
      display: block;
      width: 6px;
      height: 2px;
      background-color: white;
      position: absolute;
      top: calc(50% - 1px);
      left: calc(50% - 3.5px);
    }
  `
      : ''}
`;
