import React from 'react';
import { Label, Icon, Avatar } from '@8base/boost';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Margin } from 'shared/components';
import Select, { components } from 'react-select';

const CustomMultiValueContainer = styled.span`
  padding: 5px;
  display: flex;
  height: 42px;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border: 1px solid #d0d7dd;
  border-radius: 5px;
  background-color: #ffffff;
  margin-right: 10px;
  cursor: pointer;
  margin-bottom: 10px;
`;

const CustomOption = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const CustomLabel = styled(Label)`
  cursor: pointer !important;
  margin-left: 5px !important;
`;

const MultiValueContainer = (props) => {
  return (
    <CustomMultiValueContainer>
      <components.MultiValueContainer {...props} />
    </CustomMultiValueContainer>
  );
};

const MultiValueLabel = (props) => {
  return (
    <>
      <Avatar src={props.data.imgUrl} size="sm" />
      <CustomLabel text={props.data.label} kind="secondary" />
    </>
  );
};

const MultiValueRemove = (props) => {
  return (
    <components.MultiValueRemove {...props}>
      <Icon name="Delete" color="GRAY_30" />
    </components.MultiValueRemove>
  );
};

const Option = ({ data, innerProps, isDisabled }) => {
  return !isDisabled ? (
    <Margin left="10px" top="5px" bottom="5px">
      <CustomOption {...innerProps}>
        <Avatar src={data.imgUrl} size="sm" />
        <CustomLabel text={data.label} />
      </CustomOption>
    </Margin>
  ) : null;
};

const IndicatorsContainer = () => {
  return (
    <CustomMultiValueContainer>
      <Icon name="Plus" size="xs" color="GRAY_30" />
      <CustomLabel text="New" kind="secondary" />
    </CustomMultiValueContainer>
  );
};

// Custom components prop types

MultiValueLabel.propTypes = {
  data: PropTypes.shape({
    imgUrl: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
};

Option.propTypes = {
  data: PropTypes.shape({
    imgUrl: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
  isDisabled: PropTypes.bool,
  innerProps: PropTypes.object.isRequired,
};

Option.defaultProps = { isDisabled: false };

const SelectMultipleAgents = ({ name, options, placeholder, defaultValue, onChange }) => {
  return (
    <Select
      isMulti
      name={name}
      options={options}
      onChange={onChange}
      placeholder={placeholder}
      defaultValue={defaultValue}
      components={{
        Option,
        MultiValueLabel,
        MultiValueRemove,
        MultiValueContainer,
        IndicatorsContainer,
      }}
      styles={{
        multiValue: (base) => ({
          ...base,
          alignItems: 'center',
          backgroundColor: '#fff',
          justifyContent: 'center',
        }),
        container: (base) => ({
          ...base,
          borderRadius: '5px',
          border: '1px dashed #D0D7DD',
        }),
        control: (base) => ({
          ...base,
          border: '0px',
          padding: '10px',
        }),
      }}
    />
  );
};

SelectMultipleAgents.propTypes = {
  defaultValue: PropTypes.array,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};

SelectMultipleAgents.defaultProps = {
  defaultValue: [],
};

export default SelectMultipleAgents;
