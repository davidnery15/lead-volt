import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Grid } from '@8base/boost';
import styled from 'styled-components';
import CustomLink from '../ui/CustomLink';

const StyledLayout = styled(Grid.Layout)`
  padding: 16px 24px;

  a {
    text-align: right;
    margin-top: 0px !important;
  }
`;

const StyledHeading = styled(Heading)`
  font-family: 'Poppins';
  font-weight: 600 !important;
  line-height: 28px !important;
  font-size: 16px !important;
  color: #323c47 !important;
`;

const TableHeader = ({ title, link }) => {
  return (
    <StyledLayout inline columns="auto auto">
      <Grid.Box>
        <StyledHeading type="h1">{title}</StyledHeading>
      </Grid.Box>
      <Grid.Box style={{ justifyContent: 'center' }}>
        <CustomLink to={link.path}>{link.name}</CustomLink>
      </Grid.Box>
    </StyledLayout>
  );
};

TableHeader.propTypes = {
  link: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
};

export default TableHeader;
