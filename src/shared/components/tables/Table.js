import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { Table, Menu, Icon, Checkbox, Pagination, Dropdown, Text } from '@8base/boost';
import styled from 'styled-components';
import { DropdownBodyOnTable } from '../ui/dropdowns/DropdownBodyOnTable';
import Loader from '../ui/Loader';
import { IconSort } from '../ui/Icons';
import { TextEllipsis } from '../ui/text/TextEllipsis';

const TableBody = styled(Table.Body)`
  max-height: 50vh;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1.2rem;
  padding: 0 1rem;

  & > div > div > *::before {
    background-color: var(--color-primary);
    transition: all 0.2s;
  }

  & > div > span {
    display: inline-block;
    padding: 0.5rem 1.5rem;
    border: solid 1px #d0d7dd;
    border-radius: 5px;
  }
`;

const ActionsCell = styled(Table.BodyCell)`
  display: flex;
  justify-content: space-between !important;
`;

const StyledCell = styled(Table.BodyCell)`
  padding: 8px 16px !important;
`;

const CustomHeaderCell = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const StyledHeaderCell = styled(Table.HeaderCell)`
  padding-left: 16px !important;
  padding-right: 6px !important;
`;

const DataTable = ({
  data,
  Actions,
  columns,
  isLoading,
  columnsSize,
  dropdownOptions,
  withCheck,
  withLabelActions,
  withPagination,
  withIconSort,
  ignoreColumns,
  leftDropdown,
}) => {
  const paginationCtnRef = useRef();

  return (
    <Table>
      <Table.Header columns={columnsSize}>
        {withCheck && (
          <Table.HeaderCell>
            <Checkbox />
          </Table.HeaderCell>
        )}
        {columns.map((column, index) => {
          return (
            <CustomHeaderCell key={index}>
              <StyledHeaderCell>{column.name}</StyledHeaderCell>
              {withIconSort && <IconSort size="xs" />}
            </CustomHeaderCell>
          );
        })}
        <Table.HeaderCell>{withLabelActions ? 'Actions' : ''}</Table.HeaderCell>
      </Table.Header>
      {isLoading ? (
        <Loader />
      ) : (
        <TableBody data={data}>
          {(row, index) => (
            <Table.BodyRow columns={columnsSize} key={index}>
              {withCheck && (
                <Table.HeaderCell>
                  <Checkbox />
                </Table.HeaderCell>
              )}
              {Object.keys(row)
                .filter((key) => ignoreColumns.indexOf(key) === -1)
                .map((key, index) => {
                  if (columns[index] && columns[index].customBodyRender !== undefined) {
                    const { customBodyRender } = columns[index];
                    const CustomCellValue = customBodyRender(row, row[key]);

                    return (
                      <StyledCell>
                        <CustomCellValue />
                      </StyledCell>
                    );
                  }

                  return (
                    <StyledCell key={key}>
                      <TextEllipsis>{row[key]}</TextEllipsis>
                    </StyledCell>
                  );
                })}
              <ActionsCell>
                {Actions && <Actions id={row.id} />}
                <Dropdown defaultOpen={false}>
                  <Dropdown.Head>
                    <Icon name="More" color="GRAY_40" />
                  </Dropdown.Head>
                  <DropdownBodyOnTable left={leftDropdown}>
                    {() => (
                      <Menu>
                        {dropdownOptions.map((option, index) => (
                          <Menu.Item onClick={() => option.onClick(row.id)} key={index}>
                            <Text color={option.color}>{option.name}</Text>
                          </Menu.Item>
                        ))}
                      </Menu>
                    )}
                  </DropdownBodyOnTable>
                </Dropdown>
              </ActionsCell>
            </Table.BodyRow>
          )}
        </TableBody>
      )}
      {withPagination && (
        <Table.Footer>
          <PaginationContainer ref={paginationCtnRef}>
            <Pagination defaultPage={1} total={data.length} />
          </PaginationContainer>
        </Table.Footer>
      )}
    </Table>
  );
};

DataTable.propTypes = {
  leftDropdown: PropTypes.string,
  ignoreColumns: PropTypes.array,
  data: PropTypes.array.isRequired,
  Actions: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  withCheck: PropTypes.bool.isRequired,
  withIconSort: PropTypes.bool.isRequired,
  columnsSize: PropTypes.string.isRequired,
  withPagination: PropTypes.bool.isRequired,
  withLabelActions: PropTypes.bool.isRequired,
  dropdownOptions: PropTypes.array.isRequired,
};

DataTable.defaultProps = {
  ignoreColumns: [],
  leftDropdown: '0px',
};

export default DataTable;
