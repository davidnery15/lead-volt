import React from 'react';
import PropTypes from 'prop-types';
import { Table, Menu, Icon, Dropdown, Text } from '@8base/boost';
import styled from 'styled-components';
import { DropdownBodyOnTable } from '../ui/dropdowns/DropdownBodyOnTable';
import Loader from '../ui/Loader';
import { TextEllipsis } from '../ui/text/TextEllipsis';

const TableBody = styled(Table.Body)`
  max-height: 50vh;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const ActionsCell = styled(Table.BodyCell)`
  display: flex;
  justify-content: space-between !important;
`;

const StyledCell = styled(Table.BodyCell)`
  padding: 0px 24px !important;
`;

const CustomHeaderCell = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const StyledHeaderCell = styled(Table.HeaderCell)`
  padding-left: 24px !important;
  padding-right: 6px !important;
  font-weight: 700 !important;
`;

const StyledBodyRow = styled(Table.BodyRow)`
  min-height: 37px !important;
`;

const StyledHeader = styled(Table.Header)`
  height: 37px !important;
`;

const TableLite = ({
  data,
  Actions,
  columns,
  isLoading,
  columnsSize,
  dropdownOptions,
  ignoreColumns,
  leftDropdown,
}) => {
  return (
    <Table>
      <StyledHeader columns={columnsSize}>
        {columns.map((column, index) => {
          return (
            <CustomHeaderCell key={index}>
              <StyledHeaderCell>{column.name}</StyledHeaderCell>
            </CustomHeaderCell>
          );
        })}
        <Table.HeaderCell>{''}</Table.HeaderCell>
      </StyledHeader>
      {isLoading ? (
        <Loader />
      ) : (
        <TableBody data={data}>
          {(row, index) => (
            <StyledBodyRow columns={columnsSize} key={index}>
              {Object.keys(row)
                .filter((key) => ignoreColumns.indexOf(key) === -1)
                .map((key, index) => {
                  if (columns[index] && columns[index].customBodyRender !== undefined) {
                    const { customBodyRender } = columns[index];
                    const CustomCellValue = customBodyRender(row, row[key]);

                    return (
                      <StyledCell key={index}>
                        <CustomCellValue />
                      </StyledCell>
                    );
                  }

                  return (
                    <StyledCell key={key}>
                      <TextEllipsis>{row[key]}</TextEllipsis>
                    </StyledCell>
                  );
                })}
              <ActionsCell>
                {Actions && <Actions id={row.id} />}
                <Dropdown defaultOpen={false}>
                  <Dropdown.Head>
                    <Icon name="More" color="GRAY_40" />
                  </Dropdown.Head>
                  <DropdownBodyOnTable left={leftDropdown}>
                    {() => (
                      <Menu>
                        {dropdownOptions.map((option, index) => (
                          <Menu.Item onClick={() => option.onClick(row.id)} key={index}>
                            <Text color={option.color}>{option.name}</Text>
                          </Menu.Item>
                        ))}
                      </Menu>
                    )}
                  </DropdownBodyOnTable>
                </Dropdown>
              </ActionsCell>
            </StyledBodyRow>
          )}
        </TableBody>
      )}
    </Table>
  );
};

TableLite.propTypes = {
  Actions: PropTypes.func,
  leftDropdown: PropTypes.string,
  ignoreColumns: PropTypes.array,
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  columnsSize: PropTypes.string.isRequired,
  dropdownOptions: PropTypes.array.isRequired,
};

TableLite.defaultProps = {
  ignoreColumns: [],
  leftDropdown: '0px',
  Actions: null,
};

export default TableLite;
