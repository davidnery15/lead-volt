import React from 'react';
import PropTypes from 'prop-types';
import { Grid, SelectField } from '@8base/boost';
import SearchInput from '../ui/inputs/SearchInput';
import FilterBtn from '../ui/buttons/FilterBtn';
import styled from 'styled-components';
import { ButtonIcon } from '../ui/buttons/ButtonIcon';
import Margin from '../ui/Margin';

const StyledLayout = styled(Grid.Layout)`
  padding: 16px;
`;

const StyledBox = styled(Grid.Box)`
  display: grid;
  align-items: flex-end;
  justify-content: flex-end;
  flex-direction: row !important;
`;

const TableToolBar = ({
  searchPlaceholder,
  selectSortByProps = {
    value: '',
    options: [],
    stretch: false,
  },
  buttonCreateProps = {
    text: 'Create',
  },
}) => {
  return (
    <StyledLayout inline columns="30% 8% calc(62% - 32px)" gap="md">
      <Grid.Box>
        <SearchInput placeholder={searchPlaceholder} />
      </Grid.Box>
      <Grid.Box>
        <FilterBtn />
      </Grid.Box>
      <StyledBox>
        <Margin right="16px">
          <SelectField {...selectSortByProps} />
        </Margin>
        <ButtonIcon {...buttonCreateProps} />
      </StyledBox>
    </StyledLayout>
  );
};

TableToolBar.propTypes = {
  selectSortByProps: PropTypes.object.isRequired,
  searchPlaceholder: PropTypes.string.isRequired,
  buttonCreateProps: PropTypes.object.isRequired,
};

export default TableToolBar;
