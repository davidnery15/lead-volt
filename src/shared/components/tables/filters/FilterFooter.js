import React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  border-top: 1px solid #d0d7dd;
  padding: 10px 15px;
  align-items: center;
`;

const ConfirmOptions = styled.span`
  color: ${(props) => (props.active ? '#3db4aa' : '#384A59')};
  cursor: pointer;

  &:first-child {
    margin-right: 10px;
  }
`;

const FilterFooter = (props) => {
  return (
    <Container>
      <ConfirmOptions
        active
        onClick={() => {
          props.onConfirm();
          props.closeDropdown();
        }}>
        Apply Filters
      </ConfirmOptions>
      <ConfirmOptions
        onClick={() => {
          props.onCancel();
          props.closeDropdown();
        }}>
        Cancel
      </ConfirmOptions>
    </Container>
  );
};

FilterFooter.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  closeDropdown: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default FilterFooter;
