import React, { useCallback } from 'react';
import { DateInput } from '@8base/boost';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import ItemFilter from '../../ItemFilter';
import RadioInputField from '../../ui/inputs/RadioInputField';

const Container = styled.div`
  width: 204px;
`;

const CategoryTitle = styled.strong`
  line-height: 28px;
  font-weight: 600 !important;
  display: inline-block;
  margin-bottom: 3px !important;
`;

const RadioGroupContainer = styled.form`
  display: flex;
  justify-content: space-around;
  margin-top: 7px;
`;

const FilterCategory = ({ category, onFilter, value }) => {
  const onCategoryChange = useCallback(
    (identifier, value) => {
      onFilter(identifier, value);
    },
    [onFilter],
  );

  let filter;
  switch (category.type) {
  case 'date':
    filter = (
      <DateInput
        placeholder={category.placeholder}
        value={value}
        minDate={category.date.min ? category.date.min : undefined}
        maxDate={category.date.max ? category.date.max : undefined}
        onChange={(value) => {
          onCategoryChange(category.identifier, value);
        }}
      />
    );
    break;
  case 'radioGroup':
    filter = (
      <RadioGroupContainer>
        {category.options.map((title) => (
          <RadioInputField
            key={title}
            label={title}
            onChange={() => {
              onCategoryChange(category.identifier, title);
            }}
            checked={value === title}
          />
        ))}
      </RadioGroupContainer>
    );
    break;
  default:
    filter = (
      <ItemFilter
        placeholder={category.placeholder}
        options={category.options}
        value={value}
        onChange={(value) => {
          onCategoryChange(category.identifier, value);
        }}></ItemFilter>
    );
  }

  return (
    <Container>
      <CategoryTitle>{category.title}</CategoryTitle>
      {filter}
    </Container>
  );
};

FilterCategory.propTypes = {
  category: PropTypes.any.isRequired,
  onFilter: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default FilterCategory;
