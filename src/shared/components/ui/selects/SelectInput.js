import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';

import { Menu, Dropdown, Icon } from '@8base/boost';
import { DropdownBodyOnTable } from '../../index';

const Item = styled(Menu.Item)`
  color: ${({ active }) => (active ? '#FFF' : '#323c47 ')} !important;
  ${({ active }) => (active ? 'background-color: #3DB4AA !important' : '')};
  padding: 10px !important;
  height: auto !important;
  text-transform: capitalize;
  &:hover {
    color: ${({ active }) => (active ? '#FFF' : '#3DB4AA ')} !important;
  }
`;

const HeaderCell = styled.span`
  display: flex;
  align-items: center;
  flex: 1;
  justify-content: space-between;
`;

const HeaderTitle = styled.span`
  ${({ active }) => (active ? 'color: #3DB4AA;' : '')}
  text-transform: capitalize;
`;

const StyledDropdown = styled(Dropdown)`
  flex: 1 !important;
  border-radius: 5px;
  ${({ border }) => (border ? 'padding: 0.5rem 1rem;' : '')}
  ${({ border }) => (border ? 'border: 1px solid;' : '')}
  ${({ active }) => (active ? 'border-color: #3DB4AA' : 'border-color: #D0D7DD')};
`;

const IconContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  flex: 1;
`;

const ItemsContainer = styled.div`
  max-height: 160px;
  min-width: 140px;
  max-width: 250px;
  overflow-y: scroll;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const SelectWithoutBorder = ({ onChange, options, placeholder, value: initialValue, border }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState(initialValue);

  const onItemClickHandler = useCallback(
    (value, closeDropdown) => {
      setValue(value);
      onChange(value);
      closeDropdown();
      setIsOpen((prevValue) => !prevValue);
    },
    [onChange],
  );

  const getValueLabel = () => {
    const selectedOption = options.find((option) => option.value === value);
    if (!selectedOption) {
      return '';
    }
    return selectedOption.label;
  };

  const selectClickHandler = () => {
    setIsOpen((prevValue) => !prevValue);
  };

  return (
    <StyledDropdown defaultOpen={false} border={border} active={isOpen}>
      <Dropdown.Head>
        <HeaderCell onClick={selectClickHandler}>
          <HeaderTitle active={isOpen}>{value ? getValueLabel() : placeholder}</HeaderTitle>
          <IconContainer>
            <Icon
              name={isOpen ? 'ChevronTop' : 'ChevronDown'}
              color={isOpen ? 'PRIMARY' : ''}
              size="xs"
            />
          </IconContainer>
        </HeaderCell>
      </Dropdown.Head>
      <DropdownBodyOnTable closeOnClickOutside={false} pin="left">
        {({ closeDropdown }) => (
          <Menu>
            <ItemsContainer>
              {options.map((option) => (
                <Item
                  key={option.value}
                  onClick={() => {
                    onItemClickHandler(option.value, closeDropdown);
                  }}
                  active={value === option.value}>
                  {option.label.toLowerCase()}
                </Item>
              ))}
            </ItemsContainer>
          </Menu>
        )}
      </DropdownBodyOnTable>
    </StyledDropdown>
  );
};

SelectWithoutBorder.defaultProps = {
  value: undefined,
  placeholder: '',
  border: false,
};

SelectWithoutBorder.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
  options: PropTypes.array.isRequired,
  placeholder: PropTypes.string,
  border: PropTypes.bool,
};

export default SelectWithoutBorder;
