import React from 'react';
import { Loader as BoostLoader } from '@8base/boost';
import styled from 'styled-components';

const LoaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 30px 0px;
`;

const Loader = () => {
  return (
    <LoaderContainer>
      <BoostLoader size="md" color="PRIMARY" />
    </LoaderContainer>
  );
};

export default Loader;
