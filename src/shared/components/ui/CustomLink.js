import { Link } from 'react-router-dom';
import styled from 'styled-components';

const CustomLink = styled(Link)`
  opacity: 0.9;
  color: #3db4aa;
  font-family: 'Poppins';
  font-size: 14px;
  line-height: 21px;
  text-decoration: none;
`;

export default CustomLink;
