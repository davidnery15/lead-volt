import React from 'react';
import { Icon } from '@8base/boost';
import styled from 'styled-components';

const Btn = styled.button`
  box-sizing: border-box;
  height: 36px;
  width: 36px;
  border: 1px solid #e2e9ef;
  border-radius: 4px;
  background-color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const PlusBtn = (props) => {
  return (
    <Btn {...props}>
      <Icon name="Plus" size="xs" color="PRIMARY" />
    </Btn>
  );
};

export default PlusBtn;
