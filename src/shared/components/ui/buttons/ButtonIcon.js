import React from 'react';
import { Button, Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledButton = styled(Button)`
  box-sizing: border-box;
  height: 36px;
  border: 1px solid #d0d7dd;
  opacity: 0.9;
  border-radius: 5px;
  background-color: #ffffff;
  padding: 0px 16px !important;
  width: auto !important;

  &:hover {
    background-color: #ffffff;
    border: 1px solid #d0d7dd;
  }
`;

const StyledText = styled(Text)`
  height: 18px;
  opacity: 0.9;
  color: #323c47;
  font-family: Poppins;
  font-size: 13px;
  line-height: 18px;
  text-align: center;
  font-weight: 600;

  & first-letter {
    text-transform: capitalize;
  }
`;

const ButtonIcon = ({ onClick, text = '', iconSvg, ...rest }) => {
  return (
    <StyledButton onClick={onClick} {...rest}>
      <img src={iconSvg} alt="Lead Volt" height="24" width="24" />
      <StyledText>{text}</StyledText>
    </StyledButton>
  );
};

ButtonIcon.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
  iconSvg: PropTypes.string.isRequired,
};

ButtonIcon.defaultProps = {
  text: '',
  onClick: () => {},
};

export { ButtonIcon };
