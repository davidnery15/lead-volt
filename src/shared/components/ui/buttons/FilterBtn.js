import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from '@8base/boost';

const StyledButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 36px !important;
  font-weight: normal !important;
`;

const Icon = styled.img`
  max-width: 100%;
  object-fit: contain;
  margin-right: 2px;
`;

const FilterBtn = ({ onClick }) => {
  return (
    <StyledButton color="neutral" onClick={onClick}>
      <Icon src={require('../../../assets/images/filter-grey.svg')} alt="Filter Icon" />
      Filters
    </StyledButton>
  );
};

FilterBtn.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default FilterBtn;
