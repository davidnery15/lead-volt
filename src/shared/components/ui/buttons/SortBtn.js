import React from 'react';
import { Button } from '@8base/boost';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  display: flex;
  align-items: center;
  border-radius: 5px;
  border: solid 1px #d0d7dd;
  background-color: #fff;
  padding: 0.7rem;
  box-sizing: border-box;
  cursor: pointer;

  &:focus {
    outline: none;
  }
`;

const Icon = styled.img`
  width: 2rem;
  height: 2rem;
  margin-right: 2px;
`;

const SortBtn = () => {
  return (
    <StyledButton>
      <Icon src={require('../../../assets/images/table-sort-grey.svg')} alt="Sort Icon" />
    </StyledButton>
  );
};

export default SortBtn;
