import { Button } from '@8base/boost';
import styled from 'styled-components';

const PrimaryBtn = styled(Button)`
  width: 180px !important;
  border: 1px solid #3db4aa !important;
  background-color: #3db4aa !important;
`;

export { PrimaryBtn };
