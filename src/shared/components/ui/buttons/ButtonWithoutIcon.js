import React from 'react';
import { Button, Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 *
 * @param props
 * @returns {*}
 * @class
 */

const StyledButton = styled(Button)`
  box-sizing: border-box;
  height: 36px;
  border: 1px solid #d0d7dd;
  opacity: 0.9;
  border-radius: 5px;
  background-color: #ffffff;
  width: auto !important;
  padding: 0px 16px !important;

  &:hover {
    background-color: #ffffff;
    border: 1px solid #d0d7dd;
  }
`;

const StyledText = styled(Text)`
  height: 18px;
  opacity: 0.9;
  color: #323c47;
  font-family: Poppins;
  font-size: 13px;
  line-height: 18px;
  text-align: center;
  font-weight: 600;

  & first-letter {
    text-transform: capitalize;
  }
`;

const ButtonWithoutIcon = ({ onClick, text = '', ...rest }) => {
  return (
    <StyledButton stretch onClick={onClick} {...rest}>
      <StyledText>{text}</StyledText>
    </StyledButton>
  );
};

ButtonWithoutIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
};

ButtonWithoutIcon.defaultProps = {
  text: '',
};

export { ButtonWithoutIcon };
