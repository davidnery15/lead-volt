import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@8base/boost';
import styled from 'styled-components';

const StyledBtn = styled(Button)`
  height: 40px;
  width: 159px;
  border-radius: 4px;
  box-sizing: border-box;
  border: 1px solid #ff0000 !important;
  background-color: #ff0000 !important;
`;

const DeleteBtn = ({ text, onClick }) => {
  return <StyledBtn onClick={onClick}>{text}</StyledBtn>;
};

DeleteBtn.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default DeleteBtn;
