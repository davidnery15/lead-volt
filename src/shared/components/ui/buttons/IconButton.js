import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledButton = styled.div`
  display: inline-flex;
`;

const StyledImg = styled.img`
  cursor: pointer;
  margin-left: 24px;
`;

const IconButton = ({ onClick, iconSvg, ...rest }) => {
  return (
    <StyledButton onClick={onClick} {...rest}>
      <StyledImg src={iconSvg} alt="Lead Volt" height="24" width="24" />
    </StyledButton>
  );
};

IconButton.propTypes = {
  text: PropTypes.string,
  iconSvg: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

IconButton.defaultProps = {
  text: '',
};

export { IconButton };
