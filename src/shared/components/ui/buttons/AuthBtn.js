import React from 'react';
import { compose } from 'recompose';
import { withAuth } from '@8base/react-sdk';
import { Query, withApollo } from 'react-apollo';
import styled from 'styled-components';
import { CURRENT_USER_QUERY } from '../../../graphql';

const Button = styled.button`
  cursor: pointer;
  font-size: 12px;
`;

class AuthBtn extends React.Component {
  renderContent = ({ loading }) => {
    const { auth, client } = this.props;

    if (loading) {
      return null;
    }

    const Logout = () => (
      <Button
        onClick={async () => {
          await client.clearStore();
          auth.authClient.logout();
        }}>
        Logout
      </Button>
    );
    const Login = () => <Button onClick={() => auth.authClient.authorize()}>Sign In</Button>;

    return <>{auth.isAuthorized ? <Logout /> : <Login />}</>;
  };

  render() {
    return <Query query={CURRENT_USER_QUERY}>{this.renderContent}</Query>;
  }
}

AuthBtn = compose(withApollo, withAuth)(AuthBtn);

export { AuthBtn };
