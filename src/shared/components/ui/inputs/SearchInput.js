import React from 'react';
import { Input } from '@8base/boost';
import { PropTypes } from 'prop-types';
import searchIcon from '../../../assets/images/Search-dark.svg';

import styled from 'styled-components';

const SearchIcon = styled.img`
  cursor: pointer;
  transition: all 0.2s;
  &:active {
    transform: translateY(2px);
  }
`;

const SearchInput = ({ onIconClick, onChange, ...rest }) => {
  return (
    <Input
      type="text"
      name="search"
      stretch={true}
      onChange={onChange}
      style={{ color: '#b3b3b3' }}
      rightIcon={<SearchIcon onClick={onIconClick} src={searchIcon} alt="search icon" />}
      {...rest}
    />
  );
};

SearchInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  onIconClick: PropTypes.func.isRequired,
};

export default SearchInput;
