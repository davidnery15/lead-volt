import React from 'react';

import styled from 'styled-components';
import { PropTypes } from 'prop-types';

const InputContainer = styled.div`
  position: relative;
`;

const RadioButton = styled.span`
  height: 22px;
  width: 22px;
  border: 1px solid #d0d7dd;
  border-radius: ${(props) => (props.square ? '4px' : '50%')};
  display: inline-block;
  position: absolute;
  left: ${(props) => (props.left ? '0' : '10px')};
  top: 50%;
  transform: translateY(-50%);

  &::after {
    height: 10px;
    width: 10px;
    content: '';
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    border-radius: ${(props) => (props.square ? '0' : '50%')};
    transform: translate(-50%, -50%);
    background-color: #3db4aa;
    opacity: 0;
  }
`;

const Input = styled.input`
  display: none;
  &:checked ~ label span::after {
    opacity: 1;
  }
`;

const RadioLabel = styled.label`
  padding: 10px 10px 10px 40px;
  display: flex;
  align-items: center;
  width: 100%;
  cursor: pointer;
`;

const RadioInputField = (props) => {
  return (
    <InputContainer>
      <Input type="radio" checked={props.checked} />
      <RadioLabel
        onClick={() => {
          props.onChange();
        }}>
        <RadioButton left={props.left} square={props.square} />
        <p>{props.label}</p>
      </RadioLabel>
    </InputContainer>
  );
};

RadioInputField.defaultProps = {
  label: undefined,
  square: false,
  left: false,
};

RadioInputField.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  square: PropTypes.bool,
  left: PropTypes.bool,
};

export default RadioInputField;
