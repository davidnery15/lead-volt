import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledText = styled(Text)`
  height: 19px;
  color: #323c47;
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 13px;
  line-height: 20px;
`;

const SecondaryTitle = ({ children }) => {
  return <StyledText>{children}</StyledText>;
};

SecondaryTitle.propTypes = {
  children: PropTypes.any.isRequired,
};

export { SecondaryTitle };
