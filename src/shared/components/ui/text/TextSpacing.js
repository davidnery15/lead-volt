import styled from 'styled-components';

export const TextSpacing = styled.p`
  color: #323c47;
  font-size: 12px;
  letter-spacing: 1px;
  line-height: 27px;
  font-weight: 600;
  margin-right: 15px;
  margin-top: -2px;
  text-transform: uppercase;
`;
