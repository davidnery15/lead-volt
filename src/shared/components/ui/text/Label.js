import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 * Styled title into the card
 */

const StyledLabel = styled(Text)`
  font-family: Poppins;
  font-size: 13px;
  line-height: 20px;
  color: #323c47;
  font-weight: 600;
  & first-letter {
    text-transform: capitalize;
  }
`;

const Label = ({ children }) => {
  return <StyledLabel>{children}</StyledLabel>;
};

Label.propTypes = {
  children: PropTypes.any.isRequired,
};

export { Label };
