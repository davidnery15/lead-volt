import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 * Styled text into the table
 */

const StyledText = styled(Text)`
  font-family: Poppins;
  font-size: 13px;
  line-height: 20px;
  color: #323c47;
  text-align: right;
  font-weight: 400;
  & first-letter {
    text-transform: capitalize;
  }
`;

const TextTable = ({ children }) => {
  return <StyledText>{children}</StyledText>;
};

TextTable.propTypes = {
  children: PropTypes.any.isRequired,
};

export { TextTable };
