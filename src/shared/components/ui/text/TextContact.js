import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 * Styled text Contact into the table
 */

const StyledText = styled(Text)`
  font-family: Poppins;
  font-size: 13px;
  line-height: 20px;
  color: #3db4aa;
  text-align: right;
  font-weight: 400;
  & first-letter {
    text-transform: capitalize;
  }
`;

const TextContact = ({ children }) => {
  return <StyledText>{children}</StyledText>;
};

TextContact.propTypes = {
  children: PropTypes.any.isRequired,
};

export { TextContact };
