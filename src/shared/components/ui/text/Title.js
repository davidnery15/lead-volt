import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 * Styled title into the card
 */
const StyledTitle = styled(Text)`
  font-family: Poppins;
  font-size: 16px;
  line-height: 28px;
  color: #323c47;
  font-weight: 600;

  & first-letter {
    text-transform: capitalize;
  }
`;

const Title = ({ children }) => {
  return <StyledTitle>{children}</StyledTitle>;
};

Title.propTypes = {
  children: PropTypes.any.isRequired,
};

export { Title };
