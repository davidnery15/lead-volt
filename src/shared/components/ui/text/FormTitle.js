import React from 'react';
import { Heading, Row } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

/**
 * Styled FormTitle for the forms
 */
const StyledTitle = styled(Heading)`
  color: #7d828c;
  font-size: 12px;
  letter-spacing: 0.5px !important;
`;

const StyledRow = styled(Row)`
  margin: 0 !important;
  min-height: 36px !important;
`;

const FormTitle = ({ text, Icon, offsetBottom, offsetTop }) => {
  return (
    <StyledRow alignItems={'center'} offsetBottom={offsetBottom} offsetTop={offsetTop}>
      {Icon ? <Icon /> : null}
      <StyledTitle type="h5" text={text} />
    </StyledRow>
  );
};

FormTitle.defaultProps = {
  Icon: null,
  offsetBottom: 'md',
  offsetTop: 'md',
};

FormTitle.propTypes = {
  text: PropTypes.string.isRequired,
  Icon: PropTypes.any,
  offsetBottom: PropTypes.string,
  offsetTop: PropTypes.string,
};

export { FormTitle };
