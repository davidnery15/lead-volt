import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Paper } from '@8base/boost';
import styled from 'styled-components';

const StyledHeading = styled(Heading)`
  opacity: 0.9;
  color: #323c47 !important;
  font-family: 'Poppins';
  font-size: 22px !important;
  line-height: 24px !important;
  padding: 16px 24px;
  font-weight: 600 !important;
`;

const TitleDetail = ({ text }) => {
  return (
    <Paper borderRadius="none">
      <StyledHeading type="h1">{text}</StyledHeading>
    </Paper>
  );
};

TitleDetail.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TitleDetail;
