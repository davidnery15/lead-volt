import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const colorsAndText = {
  1: { text: 'Lo-Priority', textColor: '#3DB4AA', bg: 'rgba(61,180,170,0.1)' },
  2: { text: 'Mid-Priority', textColor: '#F6C01E', bg: 'rgba(246,192,30,0.15)' },
  3: { text: 'Hi-Priority', textColor: '#eb7734', bg: 'rgba(235, 119, 52, 0.1)' },
  4: { text: 'Very Hi-Priority', textColor: '#FF0000', bg: 'rgba(255,0,0,0.08)' },
};
const Badge = styled.span`
  opacity: 0.9;
  color: ${({ priority }) => colorsAndText[priority].textColor};
  font-family: 'Poppins';
  font-size: 12px;
  font-weight: 600;
  line-height: 18px;
  text-align: center;
  box-sizing: border-box;
  height: 26px;
  min-width: 110px;
  border: 1px solid ${({ priority }) => colorsAndText[priority].textColor};
  border-radius: 8px;
  background-color: ${({ priority }) => colorsAndText[priority].bg};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const PriorityBadge = ({ priority }) => {
  return (
    <Badge priority={priority}>
      <span>{colorsAndText[priority].text}</span>
    </Badge>
  );
};

PriorityBadge.propTypes = {
  priority: PropTypes.number.isRequired,
};

export default PriorityBadge;
