import styled from 'styled-components';

export const DropdownOption = styled.div`
  color: #384a59;
  font-family: 'Poppins';
  font-size: 13px;
  line-height: 28px;
  padding: 0px 22px;
  display: flex;
  justify-content: flex-start;
  align-items: center;

  ${(props) => (props.active ? 'color: var(--color-primary);' : '')}
`;
