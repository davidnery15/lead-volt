import React from 'react';
import styled from 'styled-components';

import { PropTypes } from 'prop-types';

const Container = styled.div`
  border-top: 1px solid #d0d7dd;
  padding: 10px 15px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const Option = styled.span`
  color: ${(props) => (props.active ? '#3db4aa' : '#384A59')};
  cursor: pointer;
`;

const DropdownFooter = (props) => {
  return (
    <Container>
      <Option active onClick={props.onConfirm}>
        Accept
      </Option>
      <Option onClick={props.onCancel}>Cancel</Option>
    </Container>
  );
};

DropdownFooter.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default DropdownFooter;
