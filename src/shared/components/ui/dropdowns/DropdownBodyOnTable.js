import React from 'react';
import { Dropdown } from '@8base/boost';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';

const StyledBody = styled(Dropdown.Body)`
  left: ${(props) => props.left} !important;
`;

/**
 * Use this instead of Dropdown.Body for tables, the withPortal
 * prop will fix the dropdown visibility problems
 * To change the dropdown position update the placement & pin props
 *
 * @param props
 * @param {Component} children the children component
 */
const DropdownBodyOnTable = ({ children, left, closeOnClickOutside }) => {
  return (
    <StyledBody
      forceRender
      withPortal
      pin="left"
      left={left}
      closeOnClickOutside={closeOnClickOutside}>
      {children}
    </StyledBody>
  );
};

DropdownBodyOnTable.propTypes = {
  left: PropTypes.string,
  children: PropTypes.any.isRequired,
  closeOnClickOutside: PropTypes.bool,
};

DropdownBodyOnTable.defaultProps = {
  pin: 'left',
  left: undefined,
  closeOnClickOutside: true,
};

export { DropdownBodyOnTable };
