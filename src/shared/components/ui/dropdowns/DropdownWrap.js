import styled from 'styled-components';

export const DropdownWrap = styled.div`
  border: 1px solid #ced7dd;
  border-radius: 4px;
  min-width: 200px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
