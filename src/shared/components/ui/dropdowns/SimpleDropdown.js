import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { Icon, Dropdown, Menu } from '@8base/boost';
import { DropdownBodyOnTable } from './DropdownBodyOnTable';
import DropdownFooter from './DropdownFooter';

const DropdownMenu = styled(Menu)`
  padding-bottom: 0 !important;
`;

const Item = styled.div`
  color: #323c47 !important;
  padding: 10px 0 !important;
  height: auto !important;
  margin-left: 15px;
`;

const HeaderCell = styled.span`
  display: flex;
  align-items: center;
  flex: 1;
  justify-content: space-between;
`;

const StyledDropdown = styled(Dropdown)`
  flex: 1 !important;
`;

const IconContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  flex: 1;
`;

const ItemsContainer = styled.div`
  max-height: 160px;
  min-width: 140px;
  max-width: 250px;
  overflow-y: scroll;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const SimpleDropdown = ({ header, itemList }) => {
  const items = itemList.map((item) => <Item key={item.id}>{item.label}</Item>);

  return (
    <StyledDropdown defaultOpen={false}>
      <Dropdown.Head>
        <HeaderCell>
          <span>{header}</span>
          <IconContainer>
            <Icon name="ChevronDown" size="xs" />
          </IconContainer>
        </HeaderCell>
      </Dropdown.Head>
      <DropdownBodyOnTable closeOnClickOutside={false} pin="left">
        {({ closeDropdown }) => (
          <DropdownMenu>
            <ItemsContainer>{items}</ItemsContainer>
            <DropdownFooter
              onConfirm={() => {
                closeDropdown();
              }}
              onCancel={() => {
                closeDropdown();
              }}
            />
          </DropdownMenu>
        )}
      </DropdownBodyOnTable>
    </StyledDropdown>
  );
};

SimpleDropdown.defaultProps = {
  header: '',
};

SimpleDropdown.propTypes = {
  header: PropTypes.string,
  itemList: PropTypes.array.isRequired,
};

export default SimpleDropdown;
