import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { Dropdown, Icon, Menu } from '@8base/boost';
import { DropdownBodyOnTable } from '../../index';

const MenuItem = styled(Menu.Item)`
  color: ${({ danger }) => (danger ? '#FF0000' : 'rgb(50, 60, 71)')}!important;
  &:hover {
    ${({ danger }) => (danger ? '' : 'color: #3db4aa !important;')}
  }
`;
const OptionsIcon = styled.span`
  & i {
    height: 30px !important;
    width: 30px !important;
  }
`;

const OptionsDropdown = ({ options }) => {
  const body = (
    <DropdownBodyOnTable>
      {({ closeDropdown }) => (
        <Menu>
          {options.map((option) => (
            <MenuItem
              key={option.label}
              danger={option.danger}
              onClick={() => {
                option.onClick();
                closeDropdown();
              }}>
              {option.label}
            </MenuItem>
          ))}
        </Menu>
      )}
    </DropdownBodyOnTable>
  );

  return (
    <Dropdown defaultOpen={false}>
      <Dropdown.Head>
        <OptionsIcon>
          <Icon name="More" color="GRAY_40" />
        </OptionsIcon>
      </Dropdown.Head>
      {body}
    </Dropdown>
  );
};

OptionsDropdown.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
      danger: PropTypes.bool,
    }),
  ).isRequired,
};

export default OptionsDropdown;
