import React, { useState, useCallback } from 'react';
import { Menu, Dropdown, Icon } from '@8base/boost';
import * as R from 'ramda';
import styled from 'styled-components';
import RadioInputField from '../../inputs/RadioInputField';
import DropdownFooter from '../DropdownFooter';
import { DropdownBodyOnTable } from '../DropdownBodyOnTable';
import { PropTypes } from 'prop-types';

const DropdownMenu = styled(Menu)`
  padding-bottom: 0 !important;
`;

const Item = styled(Menu.Item)`
  color: #323c47 !important;
  padding: 0 !important;
  height: auto !important;
`;

const HeaderCell = styled.span`
  display: flex;
  align-items: center;
  flex: 1;
  justify-content: space-between;
`;

const StyledDropdown = styled(Dropdown)`
  flex: 1 !important;
`;

const IconContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  flex: 1;
`;

const ItemsContainer = styled.div`
  max-height: 160px;
  min-width: 140px;
  max-width: 250px;
  overflow-y: scroll;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const DropdownWithCheck = ({ onConfirm, itemList, header }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [items, setItems] = useState(itemList);
  const [control, setControl] = useState(
    items.reduce((ac, cv) => {
      if (cv.checked) {
        ac.push(cv.id);
      }

      return ac;
    }, []),
  );
  const inputChangeHandler = (id) => {
    let newControl;

    if (control.includes(id)) {
      newControl = control.filter((oldId) => oldId !== id);
    } else {
      newControl = control.concat([id]);
    }

    setControl(newControl);
  };
  const onConfirmHandler = useCallback(() => {
    onConfirm(control);
    const newItemList = R.clone(items);

    for (const item of newItemList) {
      item.checked = control.includes(item.id);
    }

    setItems(newItemList);
  }, [items, control, onConfirm]);
  const onCancelHandler = useCallback(() => {
    const newControl = items.reduce((ac, cv) => {
      if (cv.checked) {
        ac.push(cv.id);
      }
      return ac;
    }, []);

    setControl(newControl);
  }, [items]);
  const switchHandler = () => {
    if (isOpen) {
      onCancelHandler();
    }

    setIsOpen((prevValue) => !prevValue);
  };
  const renderItems = items.map((item) => (
    <Item key={item.id}>
      <RadioInputField
        square
        checked={control.includes(item.id)}
        label={item.label}
        onChange={() => {
          inputChangeHandler(item.id);
        }}
      />
    </Item>
  ));

  return (
    <StyledDropdown defaultOpen={false}>
      <Dropdown.Head>
        <HeaderCell onClick={switchHandler}>
          <span>{header}</span>
          <IconContainer>
            <Icon name="ChevronDown" size="xs" />
          </IconContainer>
        </HeaderCell>
      </Dropdown.Head>
      <DropdownBodyOnTable closeOnClickOutside={false} pin="left">
        {({ closeDropdown }) => (
          <DropdownMenu>
            <ItemsContainer>{renderItems}</ItemsContainer>
            <DropdownFooter
              onConfirm={() => {
                onConfirmHandler();
                closeDropdown();
              }}
              onCancel={() => {
                onCancelHandler();
                closeDropdown();
              }}
            />
          </DropdownMenu>
        )}
      </DropdownBodyOnTable>
    </StyledDropdown>
  );
};

DropdownWithCheck.propTypes = {
  itemList: PropTypes.array.isRequired,
  header: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default DropdownWithCheck;
