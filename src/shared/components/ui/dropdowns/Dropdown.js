import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Dropdown as DropdownBoost } from '@8base/boost';
import { TextSpacing } from '../text/TextSpacing';

const Chevron = styled.img`
  transform: rotate(${(props) => props.rotate});
  margin-left: 10px;
  position: absolute;
  right: ${({ right }) => right};
  top: 12px;
  width: 8px;
  height: 12px;
`;

const OptionContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  border-radius: 4px;
  padding: 11px 0px;

  &:hover {
    background-color: rgb(244, 247, 249);
  }
`;

const HeadDropdown = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  position: relative;
  height: 36px;
`;

const BodyHeader = styled.div`
  padding: 11px 22px;
  width: 205px;
  border-bottom: 1px solid #e9eff4;
`;

const Dropdown = ({
  options,
  placeholder,
  currentOption,
  defaultOpen,
  onChange,
  bodyHeader,
  showActiveOption,
  offset,
  minWidth,
}) => {
  const Head =
    currentOption !== ''
      ? options.find((option) => option.value === currentOption).Component
      : () => <span>{placeholder}</span>;
  const optionsFiltered = options.filter((option) => option.value !== currentOption);

  return (
    <DropdownBoost defaultOpen={defaultOpen}>
      <DropdownBoost.Head style={{ position: 'relative', minWidth }}>
        {({ isOpen, toggleDropdown }) => {
          return (
            <>
              <HeadDropdown onClick={optionsFiltered.length > 0 ? toggleDropdown : null}>
                <Head isHead />
              </HeadDropdown>
              <Chevron
                rotate={isOpen ? '-90deg' : '90deg'}
                src={require('../../../assets/images/chevron.svg')}
                right={minWidth === '' ? '0px' : '16px'}
              />
            </>
          );
        }}
      </DropdownBoost.Head>
      <DropdownBoost.Body offset={offset} background="white" closeOnClickOutside>
        {({ toggleDropdown }) => {
          const renderOptionsMap = (option) => {
            const active = option.value === currentOption;

            return (
              <OptionContainer
                onClick={
                  active
                    ? null
                    : () => {
                      onChange(option.value);
                      toggleDropdown();
                    }
                }
                key={option.value}>
                <option.Component key={option.value} active={active} />
              </OptionContainer>
            );
          };

          return (
            <div>
              {bodyHeader !== '' && (
                <BodyHeader>
                  <TextSpacing>{bodyHeader}</TextSpacing>
                </BodyHeader>
              )}
              {showActiveOption
                ? options.map(renderOptionsMap)
                : optionsFiltered.length > 0
                  ? optionsFiltered.map(renderOptionsMap)
                  : null}
            </div>
          );
        }}
      </DropdownBoost.Body>
    </DropdownBoost>
  );
};

Dropdown.propTypes = {
  currentOption: PropTypes.string,
  options: PropTypes.array.isRequired,
  placeholder: PropTypes.string.isRequired,
  defaultOpen: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  bodyHeader: PropTypes.string,
  offset: PropTypes.string,
  minWidth: PropTypes.string,
  showActiveOption: PropTypes.bool,
};

Dropdown.defaultProps = {
  currentOption: '',
  bodyHeader: '',
  defaultOpen: false,
  offset: 'none',
  minWidth: '',
  showActiveOption: false,
};

export default Dropdown;
