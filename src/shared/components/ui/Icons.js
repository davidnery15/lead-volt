import React from 'react';
import styled from 'styled-components';
import { Icon as IconBoost } from '@8base/boost';
import COMPANY_INFORMATION_SVG from '../../assets/images/create-company.svg';
import BILLING_INFORMATION_SVG from '../../assets/images/Billing-information.svg';
import AGENCY_INFORMATION_SVG from '../../assets/images/First-Agency-Info.svg';
import SUBSCRIPTION_PLAN_SVG from '../../assets/images/Subscription-plan.svg';
import INFO_SVG from '../../assets/images/info.svg';

const sizes = {
  xs: '16px',
  sm: '20px',
  md: '24px',
};

export const Icon = styled.img`
  width: ${(props) => sizes[props.size]};
  height: ${(props) => sizes[props.size]};
  cursor: pointer;
`;

const StyledIconBoost = styled(IconBoost)`
  cursor: pointer;
`;

export const IconAskInfo = (props) => <Icon alt="Icon Info" src={INFO_SVG} {...props} />;

export const IconCompanyInformation = (props) => (
  <Icon alt="Icon Company Info" src={COMPANY_INFORMATION_SVG} {...props} />
);

export const IconBillingInformation = (props) => (
  <Icon alt="Icon Billing Info" src={BILLING_INFORMATION_SVG} {...props} />
);

export const IconAgencyInformation = (props) => (
  <Icon alt="Icon Agency Info" src={AGENCY_INFORMATION_SVG} {...props} />
);

export const IconSubscriptionPlan = (props) => (
  <Icon alt="Icon Subscription plan Info" src={SUBSCRIPTION_PLAN_SVG} {...props} />
);

export const IconInfo = (props) => (
  <Icon alt="Icon Info" src={require('../../assets/images/information.svg')} {...props} />
);

export const IconSort = (props) => (
  <Icon alt="Icon Sort" src={require('../../assets/images/sort.svg')} {...props} />
);

export const IconLeftArrow = (props) => (
  <Icon alt="IconLeftArrow" src={require('../../assets/images/left-arrow.svg')} {...props} />
);

export const IconRightArrow = (props) => (
  <Icon alt="IconRightArrow" src={require('../../assets/images/right-arrow.svg')} {...props} />
);

export const IconPerformanceGreen = (props) => (
  <Icon
    alt="Performance Icon"
    src={require('../../assets/images/performance-green.svg')}
    {...props}
  />
);

export const CustomIconBoost = (props) => <StyledIconBoost {...props} />;
