import React from 'react';
import styled from 'styled-components';

import moment from 'moment';
import { PropTypes } from 'prop-types';

const DateContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const DateBox = ({ date }) => {
  return (
    <DateContainer>
      <p>{moment(date).format('MM-DD-YYYY')}</p>
      <p>{moment(date).format('LT')}</p>
    </DateContainer>
  );
};

DateBox.propTypes = {
  date: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]).isRequired,
};

export default DateBox;
