import PropTypes from 'prop-types';
import styled from 'styled-components';

const Margin = styled.div`
  ${(props) => {
    const { top, right, left, bottom } = props;

    return `
      margin: ${top} ${right} ${bottom} ${left};
    `;
  }}
`;

Margin.propTypes = {
  top: PropTypes.string,
  right: PropTypes.string,
  left: PropTypes.string,
  bottom: PropTypes.string,
};

Margin.defaultProps = {
  top: '0px',
  right: '0px',
  left: '0px',
  bottom: '0px',
};

export default Margin;
