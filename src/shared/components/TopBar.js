import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import IconCount from './IconCount';
import DialogProfile from './dialog/DialogProfile';
import AvatarWithStatus from './AvatarWithStatus';
import { routes } from './Menu';
import { withRouter } from 'react-router-dom';
import { Grid, Row, Text } from '@8base/boost';
import Dropdown from './ui/dropdowns/Dropdown';
import { getUserAgencies, getActiveAgency, setActiveAgency } from '../utils/agency-utils';
import { DropdownOption } from './ui/dropdowns/DropdownOption';
import { DropdownWrap } from './ui/dropdowns/DropdownWrap';
import sessionStore, { NEW_SESSION_EVENT } from '../session/session-store';
import { DialogInvitations } from '../../modules/onboarding/components/DialogInvitations';
import {
  declineAgencyInvitation,
  acceptAgencyInvitation,
} from '../../modules/agency-invitation/agency-invitation-actions';
import {
  declineCompanyInvitation,
  acceptCompanyInvitation,
} from '../../modules/company-invitation/company-invitation-actions';
import * as R from 'ramda';
import { useSubscription } from '@cobuildlab/react-flux-state';
import agencyInvitationStore, {
  AGENCY_INVITATION_ERROR_EVENT,
  DECLINE_AGENCY_INVITATION_EVENT,
  ACCEPT_AGENCY_INVITATION_EVENT,
} from '../../modules/agency-invitation/agency-invitation-store';
import companyInvitationStore, {
  COMPANY_INVITATION_ERROR_EVENT,
  DECLINE_COMPANY_INVITATION_EVENT,
  ACCEPT_COMPANY_INVITATION_EVENT,
} from '../../modules/company-invitation/company-invitation-store';
import { fetchSession } from '../../modules/auth/auth.actions';
import agencyStore, { AGENCY_ERROR_EVENT } from '../../modules/agency/agency-store';
import { onErrorMixinFC } from '../mixins';
import { getPendingInvitations } from '../utils/invitations-utils';

const TopBarWrap = styled.div`
  top: 0px;
  left: 56px;
  display: flex;
  height: 56px;
  position: absolute;
  width: calc(100% - 56px);
  background-color: white;
  border-bottom: 1px solid #e9eff4;
`;

const H1 = styled.h1`
  text-align: left;
  margin-left: 24px;
  font-weight: normal;
  font-size: 14px !important;
  color: var(--color-primary);
`;

const Agency = styled.div`
  height: 36px;
  padding: 11px 22px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CurrentBadge = styled.span`
  box-sizing: border-box;
  height: 20px;
  width: 57px;
  border: 1px solid #eaeff3;
  border-radius: 5px;
  color: #9b9b9b;
  font-family: 'Poppins';
  font-size: 11px;
  line-height: 20px;
  text-align: center;
  margin-left: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const TopBar = ({ history }) => {
  const getTitle = (pathname) => {
    const route = routes.find((route) => pathname.startsWith(route.path));

    return route ? route.name : '';
  };
  const [dialogIsOpen, setDialogIsOpen] = useState(false);
  const [title, setTitle] = useState(getTitle(window.location.pathname));
  const [agencyId, setAgencyId] = useState('');
  const [invitationsCount, setInvitationsCount] = useState(0);
  const [dialogInvitationIsOpen, setDialogInvitationIsOpen] = useState(false);
  const [agencyInvitations, setAgencyInvitations] = useState([]);
  const [companyInvitations, setCompanyInvitations] = useState([]);
  const [isLoadingInvitations, setIsLoadingInvitations] = useState(false);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT) || {};

  const setInvitations = () => {
    const session = sessionStore.getState(NEW_SESSION_EVENT);

    if (session) {
      const { agencyInvitationsList, companyInvitationsList } = session;
      const [pendingAgencyInvitations, pendingCompanyInvitations] = getPendingInvitations(
        agencyInvitationsList,
        companyInvitationsList,
      );

      setInvitationsCount(pendingAgencyInvitations.length + pendingCompanyInvitations.length);
      setAgencyInvitations(pendingAgencyInvitations);
      setCompanyInvitations(pendingCompanyInvitations);
    }
  };

  useEffect(() => {
    if (agencyId === '') {
      const agency = getActiveAgency();

      if (agency) setAgencyId(agency.id);
    }

    setInvitations();
  }, [agencyId]);

  history.listen((location, action) => {
    const { pathname } = location;

    setTitle(getTitle(pathname));
  });

  const handleOnSubscription = async () => {
    await fetchSession();
    setIsLoadingInvitations(false);
    setDialogInvitationIsOpen(false);
    setInvitations();
  };

  useSubscription(agencyInvitationStore, ACCEPT_AGENCY_INVITATION_EVENT, handleOnSubscription);
  useSubscription(agencyInvitationStore, DECLINE_AGENCY_INVITATION_EVENT, handleOnSubscription);
  useSubscription(agencyInvitationStore, AGENCY_INVITATION_ERROR_EVENT, onErrorMixinFC);
  useSubscription(companyInvitationStore, ACCEPT_COMPANY_INVITATION_EVENT, handleOnSubscription);
  useSubscription(companyInvitationStore, DECLINE_COMPANY_INVITATION_EVENT, handleOnSubscription);
  useSubscription(companyInvitationStore, COMPANY_INVITATION_ERROR_EVENT, onErrorMixinFC);
  useSubscription(agencyStore, AGENCY_ERROR_EVENT, onErrorMixinFC);
  useSubscription(sessionStore, NEW_SESSION_EVENT, () => {
    const agency = getActiveAgency();

    if (agency) setAgencyId(agency.id);
  });

  const handleOnChangeAgency = (agencyId) => {
    setActiveAgency(agencyId);
    setAgencyId(agencyId);
  };
  const handleOnAcceptAgencyInvitation = (invitation) => {
    setIsLoadingInvitations(true);
    acceptAgencyInvitation(R.clone(invitation));
  };
  const handleOnDeclineAgencyInvitation = (invitation) => {
    setIsLoadingInvitations(true);
    declineAgencyInvitation(R.clone(invitation));
  };
  const handleOnAcceptCompanyInvitation = (invitation) => {
    setIsLoadingInvitations(true);
    acceptCompanyInvitation(R.clone(invitation));
  };
  const handleOnDeclineCompanyInvitation = (invitation) => {
    setIsLoadingInvitations(true);
    declineCompanyInvitation(R.clone(invitation));
  };
  const optionComponent = (agencyName) => (props) => {
    // eslint-disable-next-line
    const { active } = props;

    return (
      <DropdownOption {...props}>
        {agencyName}
        {active ? <CurrentBadge> Current</CurrentBadge> : ''}
      </DropdownOption>
    );
  };
  const agencies = getUserAgencies();

  return (
    <TopBarWrap>
      <Grid.Layout columns="40% 20% 40%" style={{ width: '100%' }}>
        <Row alignItems="center">
          <H1>{title}</H1>
        </Row>
        <Row justifyContent="center" alignItems="center">
          {agencyId !== '' ? (
            <DropdownWrap>
              {agencies.length > 1 ? (
                <Dropdown
                  minWidth="205px"
                  offset="lg"
                  showActiveOption
                  bodyHeader="CHANGE AGENCY"
                  currentOption={agencyId}
                  onChange={handleOnChangeAgency}
                  placeholder="Select an agency"
                  emptyLabel="No more agencies"
                  options={agencies.map((agency) => ({
                    value: agency.id,
                    Component: optionComponent(agency.name),
                  }))}
                />
              ) : (
                <Agency>
                  {agencyId !== '' && (
                    <Text>{agencies.find((agency) => agency.id === agencyId).name}</Text>
                  )}
                </Agency>
              )}
            </DropdownWrap>
          ) : null}
        </Row>
        <Row justifyContent="end" alignItems="center">
          <IconCount
            alt="Invitations"
            count={invitationsCount}
            imgUrl={require('../assets/images/invitations.svg')}
            onClick={() => {
              if (invitationsCount > 0) {
                setDialogInvitationIsOpen(true);
              }
            }}
            style={{
              width: '30px',
              height: '26px',
            }}
            countStyle={{
              right: '-6px',
              top: '-12px',
            }}
          />
          <IconCount
            alt="Notifications"
            count={3}
            imgUrl={require('../assets/images/notifications.svg')}
            style={{
              width: '25px',
              height: '30px',
            }}
            countStyle={{
              right: '-8px',
              top: '-9px',
            }}
          />
          <IconCount
            alt="Chat"
            count={28}
            imgUrl={require('../assets/images/chat.svg')}
            style={{
              width: '30px',
              height: '25px',
            }}
            countStyle={{
              right: '-12px',
              top: '-12px',
            }}
          />
          {user && (
            <AvatarWithStatus
              size="md"
              src={user.avatar ? user.avatar.downloadUrl : ''}
              firstName={user.firstName}
              lastName={user.lastName}
              style={{
                width: '38px',
                height: '38px',
                cursor: 'pointer',
                marginRight: '24px',
                marginLeft: '15px',
              }}
              onClick={() => setDialogIsOpen(!dialogIsOpen)}
            />
          )}
        </Row>
      </Grid.Layout>
      <DialogProfile
        user={user}
        isOpen={dialogIsOpen}
        activeAgency={agencies.find((agency) => agency.id === agencyId)}
      />
      <DialogInvitations
        isOpen={dialogInvitationIsOpen}
        agencyInvitations={agencyInvitations}
        companyInvitations={companyInvitations}
        onCloseDialog={() => setDialogInvitationIsOpen(false)}
        isLoading={isLoadingInvitations}
        onClickAcceptAgencyInvitation={handleOnAcceptAgencyInvitation}
        onClickDeclineAgencyInvitation={handleOnDeclineAgencyInvitation}
        onClickAcceptCompanyInvitation={handleOnAcceptCompanyInvitation}
        onClickDeclineCompanyInvitation={handleOnDeclineCompanyInvitation}
      />
    </TopBarWrap>
  );
};

TopBar.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(TopBar);
