import React from 'react';
import PropTypes from 'prop-types';
import { Breadcrumbs as BreadcrumbsBoost, Paper } from '@8base/boost';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { matchPath } from 'react-router';

const BreadcrumbsContainer = styled.div`
  padding: 5px 24px;

  a {
    text-decoration: none !important;
  }
`;

const Breadcrumb = styled.span`
  opacity: 0.9;
  color: #384a59;
  font-size: 14px;
  line-height: 20px;
  font-family: 'Poppins';
`;

const routeComponent = (name) => () => <Breadcrumb>{name}</Breadcrumb>;

const routes = [
  {
    path: '/app',
    component: routeComponent('Lead Volt'),
    matchOptions: { exact: true },
  },
  {
    path: '/app/campaigns',
    component: routeComponent('Campaigns'),
    matchOptions: { exact: true },
  },
  {
    path: '/app/leads',
    component: routeComponent('Lead Managment'),
    matchOptions: { exact: true },
  },
  {
    path: '/app/settings/general',
    component: routeComponent('Settings > General'),
    matchOptions: { exact: true },
  },
  {
    path: '/app/settings/email',
    component: routeComponent('Settings > Email'),
    matchOptions: { exact: true },
  },
];
const ignoreRoutes = ['/dashboard'];

const Breadcrumbs = ({ location }) => {
  return (
    <>
      {ignoreRoutes.indexOf(location.pathname) === -1 ? (
        <Paper borderRadius="none">
          <BreadcrumbsContainer>
            <BreadcrumbsBoost
              routes={routes}
              matchPath={matchPath}
              pathname={`/app${location.pathname}`}
            />
          </BreadcrumbsContainer>
        </Paper>
      ) : null}
    </>
  );
};

Breadcrumbs.propTypes = {
  location: PropTypes.object.isRequired,
};

export default withRouter(Breadcrumbs);
