import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import AgentSlide from './AgentSlide';
import { IconRightArrow, IconLeftArrow } from '../ui/Icons';
import styled from 'styled-components';

const Arrow = styled.div`
  top: 45% !important;

  &::before {
    display: none !important;
  }
`;

const NextArrow = (props) => {
  return (
    <Arrow {...props}>
      <IconRightArrow size="xs" />
    </Arrow>
  );
};

const PrevArrow = (props) => {
  return (
    <Arrow {...props}>
      <IconLeftArrow size="xs" />
    </Arrow>
  );
};

const AgentsSlider = ({ agents }) => {
  const settings = {
    dots: false,
    infinite: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  return (
    <div style={{ width: '550px', margin: '0 auto' }}>
      <Slider {...settings}>
        {agents.map((agent, key) => (
          <AgentSlide
            key={key}
            name={agent.name}
            imgUrl={agent.imgUrl}
            position={agent.position}
            performance={agent.performance}
          />
        ))}
      </Slider>
    </div>
  );
};

AgentsSlider.propTypes = {
  agents: PropTypes.array.isRequired,
};

export default AgentsSlider;
