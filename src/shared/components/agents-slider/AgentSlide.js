import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Image = styled.img`
  width: 90px;
  height: 90px;
  border-radius: 100%;
  border: 1.5px solid #a0a0a0;
`;

const Slide = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: relative;
  margin-top: 16px;
`;

const Performance = styled.span`
  width: 114px;
  color: #3db4aa;
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 16px;
  line-height: 28px;
  text-align: center;
  margin-top: 12px;
`;

const Name = styled.span`
  height: 16px;
  width: 114px;
  opacity: 0.9;
  color: #323c47;
  font-family: 'Poppins';
  font-size: 13px;
  line-height: 16px;
  text-align: center;
`;

const positions = {
  '1': {
    textColor: '#D09D07',
    bgColor: '#F6C01E',
  },
  '2': {
    textColor: '#9B9B9B',
    bgColor: '#DBDBDB',
  },
  '3': {
    textColor: '#AB5D1D',
    bgColor: '#DE953D',
  },
};

const Position = styled.div`
  width: 20px;
  height: 20px;
  color: ${({ value }) => positions[value].textColor};
  font-family: 'Poppins';
  font-size: 12px;
  font-weight: bold;
  line-height: 28px;
  text-align: center;
  position: absolute;
  left: calc(50% - 10px);
  top: -10px;
  background-color: ${({ value }) => positions[value].bgColor};
  border: 1px solid ${({ value }) => positions[value].textColor};
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const AgentSlide = ({ imgUrl, name, performance, position }) => {
  return (
    <Slide>
      {position > 0 && (
        <Position value={position}>
          <span>{position}</span>
        </Position>
      )}
      <Image src={imgUrl} alt={name} />
      <Performance>{performance}</Performance>
      <Name>{name}</Name>
    </Slide>
  );
};

AgentSlide.propTypes = {
  position: PropTypes.number,
  name: PropTypes.string.isRequired,
  imgUrl: PropTypes.string.isRequired,
  performance: PropTypes.string.isRequired,
};

AgentSlide.defaultProps = {
  position: 0,
};

export default AgentSlide;
