import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PaperHeaderContainer = styled.div`
  padding: 16px 24px;
  border-bottom: 1px solid #eaf0f5;
`;

const PaperHeader = ({ children }) => {
  return <PaperHeaderContainer>{children}</PaperHeaderContainer>;
};

PaperHeader.propTypes = {
  children: PropTypes.any.isRequired,
};

export default PaperHeader;
