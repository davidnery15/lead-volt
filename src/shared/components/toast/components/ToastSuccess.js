import React from 'react';
import { PropTypes } from 'prop-types';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';

const ToastSuccess = ({ title, text }) => {
  return (
    <>
      <div>
        <ToastTitle color="var(--color-primary)">{title}</ToastTitle>
      </div>
      <div>
        <ToastText>{text}</ToastText>
      </div>
    </>
  );
};

ToastSuccess.propTypes = {
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default ToastSuccess;
