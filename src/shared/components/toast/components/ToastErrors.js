import React from 'react';
import { PropTypes } from 'prop-types';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';

const ToastErrors = ({ arr }) => {
  let values = arr.map((value) => {
    return (
      <li key={value}>
        <ToastText>{value}</ToastText>
      </li>
    );
  });

  return (
    <>
      <div>
        <ToastTitle color="var(--color-red)">Ooops</ToastTitle>
      </div>
      <div>
        <ol>{values}</ol>
      </div>
    </>
  );
};

ToastErrors.propTypes = {
  arr: PropTypes.array.isRequired,
};

export default ToastErrors;
