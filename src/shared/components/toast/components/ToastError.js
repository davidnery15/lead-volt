import React from 'react';
import { PropTypes } from 'prop-types';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';

const ToastError = ({ text = '' }) => {
  return (
    <>
      <div>
        <ToastTitle color="var(--color-red)">Ooops!</ToastTitle>
      </div>
      <div>
        <ToastText>{text}</ToastText>
      </div>
    </>
  );
};

ToastError.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ToastError;
