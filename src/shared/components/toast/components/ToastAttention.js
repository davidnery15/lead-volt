import React from 'react';
import { PropTypes } from 'prop-types';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';

const ToastAttention = ({ text = '' }) => {
  return (
    <>
      <div>
        <ToastTitle color="var(--color-yellow)">Look!</ToastTitle>
      </div>
      <div>
        <ToastText>{text}</ToastText>
      </div>
    </>
  );
};

ToastAttention.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ToastAttention;
