import React from 'react';
import { PropTypes } from 'prop-types';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';

const ToastInfo = ({ title, text }) => {
  return (
    <>
      <div>
        <ToastTitle color="var(--color-blue-light)">{title}</ToastTitle>
      </div>
      <div>
        <ToastText>{text}</ToastText>
      </div>
    </>
  );
};

ToastInfo.propTypes = {
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default ToastInfo;
