import { Text } from '@8base/boost';
import styled from 'styled-components';

export const ToastText = styled(Text)`
  font-size: 14px;
  line-height: 28px;
  color: #384a59 !important;
  display: inline-block;
  height: 28px;
  font-family: 'Poppins';
`;
