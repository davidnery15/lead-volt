import { Text } from '@8base/boost';
import styled from 'styled-components';

export const ToastTitle = styled(Text)`
  font-size: 14px;
  line-height: 28px;
  letter-spacing: 0.44px;
  font-weight: bold !important;
  color: ${({ color }) => color} !important;
  height: 28px;
  display: inline-block;
  font-family: 'Poppins';
  font-weight: 500;
`;
