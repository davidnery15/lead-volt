import React from 'react';
import { PropTypes } from 'prop-types';
import { Row } from '@8base/boost';
import { ToastText } from './ToastText';
import { ToastTitle } from './ToastTitle';
import styled from 'styled-components';

const IconWarn = styled.img`
  height: 15px;
  width: 17.5px;
  margin-right: 6px;
`;

const ToastWarn = ({ text = '' }) => {
  return (
    <>
      <div>
        <ToastTitle color="var(--color-yellow)">
          <Row alignItems="center">
            <IconWarn src={require('../../../assets/images/alert.svg')} alt="Alert" />
            Warning
          </Row>
        </ToastTitle>
      </div>
      <div>
        <ToastText>{text}</ToastText>
      </div>
    </>
  );
};

ToastWarn.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ToastWarn;
