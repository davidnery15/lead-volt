import React from 'react';
import { toast } from 'react-toastify';
import {
  ToastSuccess,
  ToastError,
  ToastAttention,
  ToastInfo,
  ToastErrors,
  ToastWarn,
} from './components';

const config = {
  autoClose: 15000,
  position: toast.POSITION.BOTTOM_LEFT,
};

export const success = (title, text) => {
  toast.success(<ToastSuccess title={title} text={text} />, config);
};

export const error = (text) => {
  toast.error(<ToastError text={text} />, config);
};

export const errors = (arr) => {
  toast.error(<ToastErrors arr={arr} />, config);
};

export const info = (title, text) => {
  toast.info(<ToastInfo title={title} text={text} />, config);
};

export const attention = (text) => {
  toast.warn(<ToastAttention text={text} />, config);
};

export const warn = (text) => {
  toast.warn(<ToastWarn text={text} />, config);
};
