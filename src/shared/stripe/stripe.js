import { GRAY_30 } from '../constants/colors';
import { CardElement } from '@stripe/react-stripe-js';
import { error } from '@cobuildlab/pure-logger';

/**
 * Options for stripe input elements.
 *
 * @type {object}
 */
export const options = {
  iconStyle: 'solid',
  style: {
    base: {
      '::placeholder': { color: GRAY_30 },
    },
  },
};

/**
 * Creates a stripe token, use this on submit form handler, and pass the
 * token & error to the action.
 *
 * @param  {object}  stripe -  Stripe object.
 * @param  {object}  elements - Stripe elements.
 * @param  {object}  data  -  the data to get the stripeData
 * @returns {Promise} The stripe response.
 */
export const createToken = async (stripe, elements, data) => {
  const { cardHolderName: name } = data;
  const cardData = { name };
  const cardElement = elements.getElement(CardElement);

  let response;
  try {
    response = await stripe.createToken(cardElement, cardData);
  } catch (e) {
    error('stripeError: ', e);
    return { error: e };
  }

  return response;
};
