export const TOAST_SUCCESS_MESSAGE = 'TOAST_SUCCESS_MESSAGE';

export const CAMPAIGN_TYPES = ['MANUAL', 'WORKFLOW PROCESS', 'UPLOADED LIST', 'FILTERS', 'VENDOR'];

export const CAMPAIGN_STATUSES = ['NEW', 'FROZEN', 'ACTIVE', 'CLOSED'];

export const PRIORITIES = ['LOW', 'NORMAL', 'HIGH', 'VERY HIGH'];

/**
 * constants for user roles.
 */

export const MANAGER = 'manager';

export const ADMIN = 'admin';

export const AGENT = 'agent';

export const PRIORITY_OPTIONS = [
  { label: 'LOW', value: '1' },
  { label: 'MEDIUM', value: '2' },
  { label: 'HIGH', value: '3' },
  { label: 'VERY HIGH', value: '4' },
];
