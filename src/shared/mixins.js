import { error } from '@cobuildlab/pure-logger';
import * as toast from './components/toast/Toast';

/**
 * The onChange data method for forms
 *
 * @param {string} name  The Name of the Field
 * @param {any} value The Value of the Field to be changed
 */
export function onChangeDataMixin(name, value) {
  const { data } = this.state;
  data[name] = value;
  this.setState({ data });
}

/**
 * @param {string} name  The Name of the Field
 * @param {any} value  The Value of the Field to be changed
 */
export function onChangeFilterMixin(name, value) {
  this.setState({
    filters: {
      ...this.state.filters,
      [name]: value,
    },
  });
}

/**
 * The onError method for View, shows the error on a Toast
 *
 * @param {any} err The Error Object
 */
export function onErrorMixin(err) {
  error(err);

  if (Array.isArray(err.arr)) {
    toast.errors(err.arr);
  } else {
    toast.error(err.message);
  }
  this.setState({ isLoading: false, isLoadingForm: false });
}

/**
 * The onError method for View, shows the error on a Toast
 * This is only for functional components
 *
 * @param {any} err The Error Object
 */
export function onErrorMixinFC(err) {
  error(err);

  if (Array.isArray(err.arr)) {
    toast.errors(err.arr);
  } else {
    toast.error(err.message);
  }
}
