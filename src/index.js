import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './App';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import 'react-toastify/dist/ReactToastify.css';
import 'shared/components/toast/toast-overrides.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './shared/stripe/stripe.css';

ReactDOM.render(<App />, document.getElementById('root'));

registerServiceWorker();
