import gql from 'graphql-tag';

export const CREATE_COMPANY_MUTATION = gql`
  mutation CreateCompany($data: CompanyCreateInput!) {
    companyCreate(data: $data) {
      id
      name
    }
  }
`;

export const SUBSCRIPTION_PLANS_QUERY = gql`
  query {
    subscriptionPlansList {
      count
      items {
        id
        name
        price
        description
      }
    }
  }
`;
