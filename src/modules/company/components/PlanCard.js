import React from 'react';
import { Paper, Row, Column, Radio, Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledPaper = styled(Paper)`
  cursor: pointer;
  width: 100%;
  border: ${({ selected }) => (selected ? ' 2px solid #3db4aa' : '1px solid #d0d7dd')};
  border-radius: 5px;
  box-shadow: none !important;
`;

const PlanCard = ({ plan, selected, onSelect }) => {
  const { id, name, price, description } = plan;
  const selectedName = selected ? selected.name : '';
  const isSelected = selected && selected.id === id;
  const capitalizeName = name.toUpperCase();

  return (
    <StyledPaper onClick={() => onSelect(plan)} padding={'md'} selected={isSelected}>
      <Row>
        <Column>
          <Radio.Item style={{ marginTop: '2px' }} value={name} selectedValue={selectedName} />
        </Column>
        <Column stretch>
          <Text color="PRIMARY" weight="bold">
            {capitalizeName}
          </Text>
          <Text color={'TEXT_LIGHT_GRAY'}>{description}</Text>
        </Column>
        <Column style={{ width: '100px' }}>
          <Text weight="bold">
            {`$${price}`}
            <Text weight="normal">{' / mo.'}</Text>
          </Text>
        </Column>
      </Row>
    </StyledPaper>
  );
};

PlanCard.propTypes = {
  plan: PropTypes.object.isRequired,
  selected: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export { PlanCard };
