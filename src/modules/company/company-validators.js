import { ValidationError } from '../../shared/errors';
import { isValidString } from '../../shared/validators';

export const createCompanyValidator = (company, token, stripeError) => {
  const errorMessages = [];

  if (!isValidString(company.name)) errorMessages.push('The Company must have a valid name');

  if (!isValidString(company.taxId)) errorMessages.push('The company must have a valid Tax ID');

  if (!company.subscriptionPlan) errorMessages.push('You must select a Subscription Plan');

  if (stripeError) errorMessages.push(stripeError.message);

  if (!token) errorMessages.push('You must enter a valid Credit Card');

  if (!isValidString(company.agencyName)) errorMessages.push('You must enter a valid Agency name');

  if (!isValidString(company.cardHolderName))
    errorMessages.push('You must enter a Card Holder Name for the Credit Card');

  if (errorMessages.length) throw new ValidationError(errorMessages);
};
