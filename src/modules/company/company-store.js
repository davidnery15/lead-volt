import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a subscription plans list.
 *
 * @type {string}
 */
export const SUBSCRIPTION_PLANS_LIST_EVENT = 'onSubscriptionPlansList';

/**
 * Event that triggers a company error.
 *
 * @type {string}
 */
export const COMPANY_ERROR_EVENT = 'onCompanyError';

/**
 * Hold the Company Data
 */
class CompanyStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(SUBSCRIPTION_PLANS_LIST_EVENT);
    this.addEvent(COMPANY_ERROR_EVENT);
  }
}

const companyStore = new CompanyStore();

export { companyStore };

export default companyStore;
