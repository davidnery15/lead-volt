export const CompanyModel = {
  id: null,
  name: '',
  taxId: '',
  subscriptionPlan: null,
  agencyName: '',
  agentEmails: [],
  managerEmails: [],
  adminEmails: [],
  cardHolderName: '',
};
