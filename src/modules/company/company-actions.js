import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';
import sessionStore, { NEW_SESSION_EVENT, APOLLO_CLIENT } from '../../shared/session/session-store';
import { SUBSCRIPTION_PLANS_QUERY } from './company-queries';
import { COMPANY_ERROR_EVENT, SUBSCRIPTION_PLANS_LIST_EVENT } from './company-store';

/**
 * Fetches the subscription plans for the company.
 *
 * @returns {Promise<void>} The data.
 */
export const fetchSubscriptionPlans = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: SUBSCRIPTION_PLANS_QUERY,
      fetchPolicy: 'network-only',
      variables: { userId: user.id },
    });
  } catch (e) {
    error('fetchSubscriptionPlans: ', e);
    Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  log('fetchSubscriptionPlans', response.data);
  Flux.dispatchEvent(SUBSCRIPTION_PLANS_LIST_EVENT, response.data);
  return response.data;
};
