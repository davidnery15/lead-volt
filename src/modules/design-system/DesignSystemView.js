import React from 'react';
import { Code, Grid, Paper, Table, Divider } from '@8base/boost';
import components from './components';
import styled from 'styled-components';

const Text = styled.p`
  margin: 10px 0;
  color: #323c47;
  font-size: 16px;

  strong {
    font-weight: bold !important;
  }
`;

const DesignSystem = () => {
  return (
    <div className="p24">
      <Paper padding="md">
        <Grid.Layout>
          {components.map((component) => {
            return (
              <Grid.Box key={component.name}>
                <Text>
                  Name: <strong>{component.name}</strong>
                </Text>
                <Text>Description: {component.description}</Text>
                <Text>Props:</Text>
                {component.props.length ? (
                  <Table>
                    <Table.Header columns="repeat(3, 1fr)">
                      <Table.HeaderCell>Name</Table.HeaderCell>
                      <Table.HeaderCell>Type</Table.HeaderCell>
                      <Table.HeaderCell>Is Required</Table.HeaderCell>
                    </Table.Header>
                    <Table.Body>
                      {component.props.map((prop) => {
                        return (
                          <Table.BodyRow columns="repeat(3, 1fr)" key={prop.name}>
                            <Table.BodyCell>{prop.name}</Table.BodyCell>
                            <Table.BodyCell>{prop.type}</Table.BodyCell>
                            <Table.BodyCell>{prop.isRequired ? 'true' : 'false'}</Table.BodyCell>
                          </Table.BodyRow>
                        );
                      })}
                    </Table.Body>
                  </Table>
                ) : (
                  <strong>None</strong>
                )}
                <Text>Code:</Text>
                <Code>{component.code}</Code>
                <Text>Result:</Text>
                <div>
                  <component.Component />
                </div>
                <Divider />
              </Grid.Box>
            );
          })}
        </Grid.Layout>
      </Paper>
    </div>
  );
};

export default DesignSystem;
