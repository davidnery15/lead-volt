import React from 'react';
import { Row } from '@8base/boost';
import {
  Title,
  TextTable,
  TextContact,
  ButtonWithoutIcon,
  ButtonIcon,
  PrimaryBtn,
  AuthBtn,
  CardIndicator,
  DateBox,
  PriorityBadge,
  TitleDetail,
  CustomLink,
  Loader,
  SecondaryTitle,
  DeleteBtn,
  FilterBtn,
  SortBtn,
  RadioInputField,
  OptionsDropdown,
} from 'shared/components';
import {
  H1,
  H2,
  H3,
  H4,
  SubTitle,
  Body,
  Button,
  Link,
  Overline,
  OverlineBold,
  Small,
  SmallUpper,
} from 'shared/components/Typography';

const TitleExample = () => <Title>My Account / Settings</Title>;
const TextTableExample = () => <TextTable>123456</TextTable>;
const TextContactExample = () => (
  <TextContact>
    George Franklin <br />
    (562) 353-0543
  </TextContact>
);
const ButtonWithoutIconExample = () => <ButtonWithoutIcon text={'Edit Lead Info'} />;
const ButtonIconExample = () => (
  <ButtonIcon
    text={'Create Campaign'}
    iconSvg={require('../../shared/assets/images/campaigns-gray.svg')}
  />
);
const PrimaryBtnExample = () => <PrimaryBtn>Create</PrimaryBtn>;
const CardIndicatorExample = () => (
  <div style={{ width: '20%' }}>
    <CardIndicator
      title="126"
      text="New Leads in the last 24 hours"
      link={{ name: 'Manage Leads', path: '/leads' }}
    />
  </div>
);
const AuthBtnExample = () => <AuthBtn />;
const DateBoxExample = () => <DateBox date={new Date()} />;
const PriorityBadgeExample = () => (
  <Row>
    <PriorityBadge priority="1" />
    <PriorityBadge priority="2" />
    <PriorityBadge priority="3" />
  </Row>
);
const TitleDetailExample = () => <TitleDetail text="Dogde Ram - September 2019" />;
const CustomLinkExample = () => <CustomLink to="/campaigns">View all Campaigns</CustomLink>;
const LoaderExample = () => <Loader />;
const SecondaryTitleExample = () => <SecondaryTitle>Top Performers</SecondaryTitle>;
const DeleteBtnExample = () => <DeleteBtn text="Yes, Delete" />;
const FilterBtnExample = () => <FilterBtn />;
const SortBtnExample = () => <SortBtn />;
const RadioInputFieldExample = () => <RadioInputField checked={true} label="Label" />;
const TypographyExample = () => (
  <>
    <H1>H1</H1>
    <H2>H2</H2>
    <H3>H3</H3>
    <H4>H4</H4>
    <SubTitle>SubTitle</SubTitle>
    <Body>Body</Body>
    <Button>Button</Button>
    <br />
    <Link>Link</Link>
    <Overline>Overline</Overline>
    <OverlineBold>OverlineBold</OverlineBold>
    <Small>Small</Small>
    <br />
    <SmallUpper>SmallUpper</SmallUpper>
  </>
);

const OptionsDropdownExample = () => (
  <OptionsDropdown
    options={[
      { onClick: () => {}, label: 'TEST' },
      { onClick: () => {}, label: 'Danger', danger: true },
    ]}
  />
);

export default [
  {
    name: 'Title',
    description: 'Received a children',
    Component: TitleExample,
    code: `<Title>My Account / Settings</Title>`,
    props: [],
  },
  {
    name: 'TextTable',
    description: 'Received a children',
    Component: TextTableExample,
    code: `<TextTable>123456</TextTable>`,
    props: [],
  },
  {
    name: 'TextContact',
    description: 'Received a children',
    Component: TextContactExample,
    code: `<TextContact>George Franklin <br />(562) 353-0543</TextContact>`,
    props: [],
  },
  {
    name: 'ButtonWithoutIcon',
    description: 'Received a Text and function action by props onClick',
    Component: ButtonWithoutIconExample,
    code: `<ButtonWithoutIcon text={'Edit Lead Info'} />`,
    props: [
      {
        name: 'text',
        type: 'string',
        isRequired: true,
      },
      {
        name: 'onClick',
        type: 'func',
        isRequired: true,
      },
    ],
  },
  {
    name: 'ButtonIcon',
    description:
      'Received a Text, image icon svg by props iconSvg and function action by props onClick',
    Component: ButtonIconExample,
    code: `<ButtonIcon text={'Create Campaign'} iconSvg={iconUrl} />`,
    props: [
      {
        name: 'text',
        type: 'string',
        isRequired: true,
      },
      {
        name: 'onClick',
        type: 'func',
        isRequired: true,
      },
      {
        name: 'iconSvg',
        type: 'image',
        isRequired: true,
      },
    ],
  },
  {
    name: 'PrimaryBtn',
    description: 'Received a children and onClick prop function',
    Component: PrimaryBtnExample,
    code: `<PrimaryBtn>Create</PrimaryBtn>`,
    props: [
      {
        name: 'onClick',
        type: 'func',
        isRequired: true,
      },
    ],
  },
  {
    name: 'AuthBtn',
    description: 'This component is from 8base repo starter, show button for logout and sign in',
    Component: AuthBtnExample,
    code: `<AuthBtn />`,
    props: [],
  },
  {
    name: 'CardIndicator',
    description: 'Card with title, subtitle and a link',
    Component: CardIndicatorExample,
    code: `
<CardIndicator
  title="126"
  text="New Leads in the last 24 hours"
  link={{ name: 'Manage Leads', path: '/leads' }}
/>
    `,
    props: [
      {
        name: 'title',
        type: 'string',
        isRequired: true,
      },
      {
        name: 'text',
        type: 'string',
        isRequired: true,
      },
      {
        name: 'link',
        type: 'object',
        isRequired: true,
      },
    ],
  },
  {
    name: 'DateBox',
    description: 'Received a date via props',
    Component: DateBoxExample,
    code: '<DateBox date={new Date()} />',
    props: [
      {
        name: 'date',
        type: 'string',
        isRequired: true,
      },
    ],
  },
  {
    name: 'PriorityBadge',
    description: 'Badge for tasks, values for priority prop: lo, mid and hi',
    Component: PriorityBadgeExample,
    code: `
<>
  <PriorityBadge priority="1" />
  <PriorityBadge priority="2" />
  <PriorityBadge priority="3" />
</>
    `,
    props: [
      {
        name: 'priority',
        type: 'string',
        isRequired: true,
      },
    ],
  },
  {
    name: 'TitleDetail',
    description: 'Title for use it in details view',
    Component: TitleDetailExample,
    code: `
<TitleDetail text="Dogde Ram - September 2019" />
    `,
    props: [
      {
        name: 'text',
        type: 'string',
        isRequired: true,
      },
    ],
  },
  {
    name: 'CustomLink',
    description: 'Custom react-router-dom link',
    Component: CustomLinkExample,
    code: `
<CustomLink to="/campaigns">View all Campaigns</CustomLink>
    `,
    props: [],
  },
  {
    name: 'Loader',
    description: 'Loader usign 8base-boost',
    Component: LoaderExample,
    code: '<Loader />',
    props: [],
  },
  {
    name: 'SecondaryTitle',
    description: 'Received a children',
    Component: SecondaryTitleExample,
    code: '<SecondaryTitle>Top Performers</SecondaryTitle>',
    props: [],
  },
  {
    name: 'DeleteBtn',
    description: 'Red button that received a children as text',
    Component: DeleteBtnExample,
    code: '<DeleteBtn text="Yes, Delete" />',
    props: [],
  },
  {
    name: 'FilterBtn',
    description: 'Button with filter icon',
    Component: FilterBtnExample,
    code: '<FilterBtn />',
    props: [
      {
        name: 'text',
        type: 'string',
        isRequired: true,
      },
      {
        name: 'onClick',
        type: 'func',
        isRequired: true,
      },
    ],
  },
  {
    name: 'SortBtn',
    description: 'Button with sort icon',
    Component: SortBtnExample,
    code: '<SortBtn />',
    props: [],
  },
  {
    name: 'RadioInputField',
    description: 'Radio Button with an optional Label',
    Component: RadioInputFieldExample,
    code: '<RadioInputField checked={true} label="Label"/>',
    props: [
      {
        name: 'checked',
        type: 'bool',
        isRequired: true,
      },
      {
        name: 'onChange',
        type: 'func',
        isRequired: true,
      },
      {
        name: 'label',
        type: 'string',
        isRequired: false,
      },
      {
        name: 'square',
        type: 'bool',
        isRequired: false,
      },
      {
        name: 'left',
        type: 'bool',
        isRequired: false,
      },
    ],
  },
  {
    name: 'Typography',
    description: 'All components for tipography',
    Component: TypographyExample,
    code: `
<H1>H1</H1>
<H2>H2</H2>
<H3>H3</H3>
<H4>H4</H4>
    `,
    props: [],
  },
  {
    name: 'OptionsDropdown',
    description: 'A dropdown for options',
    Component: OptionsDropdownExample,
    code: `
    <OptionsDropdown
          options={[
            { onClick: () => {}, label: 'TEST' },
            { onClick: () => {}, label: 'Danger', danger: true },
          ]}
         />
    `,
    props: [
      {
        name: 'options',
        type: `[{
            label: string.isRequired, 
            onClick: func.isRequired, 
            danger: bool}
        ]`,
        isRequired: true,
      },
    ],
  },
];

// ,
//   {
//     name: 'OptionsDropdown',
//     description: 'A dropdown for options',
//     component: OptionsDropdownExample,
//     code: `
//     <OptionsDropdown
//       options={[
//         { onClick: () => {}, label: 'TEST' },
//         { onClick: () => {}, label: 'Danger', danger: true },
//       ]}
//      />`,
//     props: [
//       { name: 'options', type: '{label: string, onClick: func, danger: bool}', isRequired: true },
//     ],
//   },
