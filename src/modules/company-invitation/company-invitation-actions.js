import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/session/session-store';
import {
  COMPANY_INVITATION_ERROR_EVENT,
  DECLINE_COMPANY_INVITATION_EVENT,
  ACCEPT_COMPANY_INVITATION_EVENT,
} from './company-invitation-store';
import {
  UPDATE_COMPANY_INVITATION_MUTATION,
  CREATE_ADMIN_MUTATION,
} from './company-invitation-queries';
import {
  canAcceptCompanyInvitation,
  canDeclineCompanyInvitation,
} from './company-invitation-permissions';
import {
  COMPANY_INVITATION_REJECTED,
  COMPANY_INVITATION_APPROVED,
} from '../../shared/constants/company-invitations';
import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';
import { createManagerAgencies } from '../agency-invitation/agency-invitation-actions';

/**
 * Creates a Company Admin.
 *
 * @param  {string}  companyId - The companyId to create the admin.
 * @param  {string}  userId  - The userId to create the admin.
 * @returns {Promise}  The created admin data.
 */
export const createAdmin = async (companyId, userId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {
    company: { connect: { id: companyId } },
    user: { connect: { id: userId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_ADMIN_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAdmin', e);
    return Flux.dispatchEvent(COMPANY_INVITATION_ERROR_EVENT, e);
  }
  log('createAdmin', response);

  return response;
};

/**
 * Accept Company invitation.
 *
 * @param  {object}  invitation - The invitation data.
 * @returns {Promise} The updated invitation.
 */
export const acceptCompanyInvitation = async (invitation) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (!canAcceptCompanyInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      COMPANY_INVITATION_ERROR_EVENT,
      new Error(`Permission Denied. Can't accept this company invitation.`),
    );
  }

  const {
    id,
    company: { agencyCompanyRelation, id: companyId },
  } = invitation;
  const data = {
    id,
    status: COMPANY_INVITATION_APPROVED,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_COMPANY_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('acceptCompanyInvitation', e);
    return Flux.dispatchEvent(COMPANY_INVITATION_ERROR_EVENT, e);
  }

  try {
    await createAdmin(companyId, user.id);
  } catch (e) {
    return Flux.dispatchEvent(COMPANY_INVITATION_ERROR_EVENT, e);
  }

  try {
    await createManagerAgencies(agencyCompanyRelation.items, user.id);
  } catch (e) {
    return Flux.dispatchEvent(COMPANY_INVITATION_ERROR_EVENT, e);
  }

  log('acceptCompanyInvitation', response);
  Flux.dispatchEvent(ACCEPT_COMPANY_INVITATION_EVENT, response.data);

  return response.data;
};

export const declineCompanyInvitation = async (invitation) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (!canDeclineCompanyInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      COMPANY_INVITATION_ERROR_EVENT,
      new Error(`Permission Denied. Can't decline this company invitation.`),
    );
  }

  const { id } = invitation;
  const data = {
    id,
    status: COMPANY_INVITATION_REJECTED,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_COMPANY_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('declineCompanyInvitation', e);
    return Flux.dispatchEvent(COMPANY_INVITATION_ERROR_EVENT, e);
  }

  log('declineCompanyInvitation', response);
  Flux.dispatchEvent(DECLINE_COMPANY_INVITATION_EVENT, response.data);

  return response.data;
};
