import gql from 'graphql-tag';

export const UPDATE_COMPANY_INVITATION_MUTATION = gql`
  mutation UpdateCompanyInvitation($data: CompanyInvitationUpdateInput!) {
    companyInvitationUpdate(data: $data) {
      id
      email
    }
  }
`;

export const CREATE_ADMIN_MUTATION = gql`
  mutation CreateAdmin($data: AdminCreateInput!) {
    adminCreate(data: $data) {
      id
    }
  }
`;
