import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Company invitation error.
 *
 * @type {string}
 */
export const COMPANY_INVITATION_ERROR_EVENT = 'onCompanyInvitationError';

/**
 * Event that triggers a decline company invitation.
 *
 * @type {string}
 */
export const DECLINE_COMPANY_INVITATION_EVENT = 'onDeclineCompanyInvitation';

/**
 * Event that triggers a acccept company invitation.
 *
 * @type {string}
 */
export const ACCEPT_COMPANY_INVITATION_EVENT = 'onAcceptCompanyInvitation';

class CompanyInvitationStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(COMPANY_INVITATION_ERROR_EVENT);
    this.addEvent(DECLINE_COMPANY_INVITATION_EVENT);
    this.addEvent(ACCEPT_COMPANY_INVITATION_EVENT);
  }
}

export default new CompanyInvitationStore();
