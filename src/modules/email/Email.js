import React, { Component } from 'react';
import { Icon, Scrollable, InputField, Dropdown, Menu, Row, Column, Card } from '@8base/boost';

import { ButtonIcon } from '../../shared/components/ui/buttons/ButtonIcon';
import { IconButton } from '../../shared/components/ui/buttons/IconButton';
import { ButtonWithoutIcon } from '../../shared/components/ui/buttons/ButtonWithoutIcon';
import { Title } from '../../shared/components/ui/text/Title';
import { TextTable } from '../../shared/components/ui/text/Text';
import composeEmail from '../../shared/assets/images/compose-email.svg';
import archive from '../../shared/assets/images/archive.svg';
import trash from '../../shared/assets/images/trash.svg';
import reply from '../../shared/assets/images/reply.svg';
import action from '../../shared/assets/images/action-menu.svg';
import styled from '@emotion/styled';

const StyledColumn = styled(Column)`
  position: relative;
  margin-left: -30px !important;
`;

const StyledDivBorder = styled(Column)`
  border-bottom: 1px solid #d8d8d8;
`;

const StyledColumn2 = styled(Column)`
  margin-left: 23px;
`;

const StyledRow = styled(Row)`
  margin-top: 16px;
  margin-bottom: 24px;
  padding-left: 20px;
  width: 360px;
`;

const StyledCardBody = styled(Card.Body)`
  min-height: 52vh;
`;

const StyledCardBodyMinimized = styled(Card.Body)`
  height: 130px;
  overflowy: hidden;
`;

const StyledDivSection1 = styled.div`
  width: 385px;
`;

const StyledDivButtomCompose = styled.div`
  position: absolute;
  right: 0px;
  padding-right: 24px;
`;

const StyledDivIcon = styled.div`
  position: absolute;
  right: 24px;
`;

const StyledDivCard = styled.div`
  height: 113px;
  border-bottom: 1px solid #d8d8d8;
  position: relative;
`;

const StyledDivCard2 = styled.div`
  margin-top: 25px;
  padding-left: 62px;
`;

const StyledDivCard3 = styled.div`
  width: 100%;
`;

const StyledDivCard4 = styled.div`
  position: absolute;
  left: 98px;
  top: 3px;
`;

const StyledPapperClip = styled.div`
  position: absolute;
  top: 3px;
  right: 36px;
`;

const StyledCircleColor = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 50%;
  display: inline-block;
  background-color: #f6c01e;
`;

const StyledCircleColor2 = styled.div`
  width: 27px;
  height: 27px;
  border-radius: 50%;
  display: inline-block;
  background-color: #3db4aa;
`;

const StyledDivContainer = styled.div`
  max-height: 90vh;
  overflow-y: scroll;
`;

const StyledDivContainer2 = styled.div`
  max-height: 74vh;
  overflow-y: scroll;
`;

const StyledDivTextDate = styled.div`
  position: absolute;
  right: 90px;
  top: 85px;
`;

const StyledDivButton = styled.div`
  display: inline-flex;
  margin-left: 25px;
  margin-top: 21px;
  margin-bottom: 21px;
`;

const StyledDivSpacing = styled.div`
  width: 15px;
`;

const StyledPTo = styled.p`
  margin-left: 40px;
  margintop: -23px;
`;

export class Email extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpenForm: false,
    };
  }

  render() {
    return (
      <>
        <Row>
          <StyledColumn>
            <StyledDivSection1>
              <StyledDivBorder>
                <Row>
                  <StyledColumn2>
                    <Dropdown defaultOpen={false}>
                      <Dropdown.Head>
                        <Title>Inbox (15)</Title>
                        <Icon name="ChevronDown" color="BLACK" size="xs" />
                      </Dropdown.Head>
                      <Dropdown.Body forceRender>
                        <Menu>
                          <Menu.Item>Inbox</Menu.Item>
                          <Menu.Item>Send</Menu.Item>
                          <Menu.Item>Archived</Menu.Item>
                          <Menu.Item>Trash</Menu.Item>
                        </Menu>
                      </Dropdown.Body>
                    </Dropdown>
                  </StyledColumn2>
                  <Column alignContent={'end'}>
                    <StyledDivButtomCompose>
                      <ButtonIcon text={'Compose'} iconSvg={composeEmail}></ButtonIcon>
                    </StyledDivButtomCompose>
                  </Column>
                </Row>
                <StyledRow>
                  <InputField
                    placeholder="Search"
                    rightIcon={<Icon name="Search" size="xs" />}
                    input={{ name: 'search', value: '' }}
                  />
                </StyledRow>
              </StyledDivBorder>
              <StyledDivContainer2>
                <Scrollable>
                  {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((data, key) => {
                    return (
                      <div key={key}>
                        <StyledDivCard>
                          <StyledDivCard2>
                            <StyledCircleColor2 />
                            <StyledDivCard3>
                              <StyledDivCard4>
                                <p>
                                  <TextTable>
                                    Michael Scot
                                    <StyledCircleColor />
                                  </TextTable>
                                </p>
                                <p>
                                  <TextTable>Asimet et al</TextTable>
                                </p>
                                <p>
                                  <TextTable>
                                    Suspendisse suscipit lobortis urna consenec…
                                  </TextTable>
                                </p>
                              </StyledDivCard4>
                              <StyledPapperClip>
                                <p>
                                  <Icon name="PaperClip" color="BLACK" size="xs" /> Jan 15 - 5:30 PM{' '}
                                </p>
                              </StyledPapperClip>
                            </StyledDivCard3>
                          </StyledDivCard2>
                        </StyledDivCard>
                      </div>
                    );
                  })}
                </Scrollable>
              </StyledDivContainer2>
            </StyledDivSection1>
          </StyledColumn>
          <Column>
            <StyledDivContainer>
              <Scrollable>
                <Card>
                  <Card.Header padding="xl">
                    Lorem ipsum dolor asimet et al
                    <StyledDivIcon>
                      <IconButton iconSvg={archive} />
                      <IconButton iconSvg={trash} />
                      <IconButton iconSvg={action} />
                    </StyledDivIcon>
                  </Card.Header>
                  <StyledCardBody padding="xl">
                    <div style={{ display: 'flex' }}>
                      <StyledCircleColor2 />
                      <p style={{ marginLeft: '11px' }}>Brad Franklin (b.franklin2@gmail.com)</p>
                    </div>
                    <StyledDivTextDate>
                      <p>Wed, Jan 15, 2020 - 5:30 PM</p>
                    </StyledDivTextDate>
                    <br />
                    <StyledPTo>
                      <b>To Nicolas (klausabio@gmail.com)</b>
                    </StyledPTo>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet turpis
                    blandit, cursus lacus placerat, tempus ipsum. Fusce cursus elit eget urna
                    porttitor hendrerit. Nullam et ipsum vel justo congue auctor vitae et est. Donec
                    vel mattis diam, a commodo urna. In hac habitasse platea dictumst. Mauris
                    viverra eleifend orci eget volutpat. Nullam vitae lectus eros. Aenean eu auctor
                    urna. Quisque egestas eleifend diam rutrum blandit. Sed porta tellus sit amet
                    placerat volutpat. Praesent justo lectus, eleifend non aliquet id, porta ut
                    nisi. Nam in nibh at eros iaculis laoreet gravida sed sem. Nullam id tincidunt
                    elit. In placerat lacus sit amet dolor commodo, in blandit neque tempus. Quisque
                    eros urna, accumsan sit amet suscipit et, iaculis sit amet nisi. In porttitor
                    est in tellus laoreet, quis pellentesque tellus placerat. Nulla hendrerit
                    ultrices lacus, a convallis massa hendrerit aliquam. Ut sem est, aliquam at
                    mollis viverra, condimentum id metus. Nunc varius eget justo sed dapibus.
                    Quisque mollis maximus dictum. Cras faucibus purus eu augue imperdiet laoreet.
                    Nullam dapibus risus ut turpis interdum, ut molestie lorem facilisis. Sed
                    ultricies ante odio, non commodo ligula interdum ac. Vestibulum ac orci eu
                    tortor pharetra finibus at a felis.
                  </StyledCardBody>
                  <StyledDivButton>
                    <ButtonIcon text={'Reply'} iconSvg={reply} />
                    <StyledDivSpacing></StyledDivSpacing>
                    <ButtonWithoutIcon text={'Forward Email'} />
                  </StyledDivButton>
                </Card>
                <Card>
                  <Card.Header padding="xl">
                    Lorem ipsum dolor asimet et al
                    <StyledDivIcon>
                      <IconButton iconSvg={archive} />
                      <IconButton iconSvg={trash} />
                      <IconButton iconSvg={action} />
                    </StyledDivIcon>
                  </Card.Header>
                  <StyledCardBodyMinimized padding="xl">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet turpis
                    blandit, cursus lacus placerat, tempus ipsum. Fusce cursus elit eget urna
                    porttitor hendrerit. Nullam et ipsum vel justo congue auctor vitae et est. Donec
                    vel mattis diam, a commodo urna. In hac habitasse platea dictumst. Mauris
                    viverra eleifend orci eget volutpat. Nullam vitae lectus eros. Aenean eu auctor
                    urna. Quisque egestas eleifend diam rutrum blandit. Sed porta tellus sit amet
                    placerat volutpat. Praesent justo lectus, eleifend non aliquet id, porta ut
                    nisi. Nam in nibh at eros iaculis laoreet gravida sed sem. Nullam id tincidunt
                    elit. In placerat lacus sit amet dolor commodo, in blandit neque tempus. Quisque
                    eros urna, accumsan sit amet suscipit et, iaculis sit amet nisi. In porttitor
                    est in tellus laoreet, quis pellentesque tellus placerat. Nulla hendrerit
                    ultrices lacus, a convallis massa hendrerit aliquam. Ut sem est, aliquam at
                    mollis viverra, condimentum id metus. Nunc varius eget justo sed dapibus.
                    Quisque mollis maximus dictum. Cras faucibus purus eu augue imperdiet laoreet.
                    Nullam dapibus risus ut turpis interdum, ut molestie lorem facilisis. Sed
                    ultricies ante odio, non commodo ligula interdum ac. Vestibulum ac orci eu
                    tortor pharetra finibus at a felis.
                  </StyledCardBodyMinimized>
                  <StyledDivButton>
                    <ButtonIcon text={'Reply'} iconSvg={reply} />
                    <StyledDivSpacing></StyledDivSpacing>
                    <ButtonWithoutIcon text={'Forward Email'} />
                  </StyledDivButton>
                </Card>
                <Card>
                  <Card.Header padding="xl">
                    Lorem ipsum dolor asimet et al
                    <StyledDivIcon>
                      <IconButton iconSvg={archive} />
                      <IconButton iconSvg={trash} />
                      <IconButton iconSvg={action} />
                    </StyledDivIcon>
                  </Card.Header>
                  <StyledCardBodyMinimized padding="xl">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet turpis
                    blandit, cursus lacus placerat, tempus ipsum. Fusce cursus elit eget urna
                    porttitor hendrerit. Nullam et ipsum vel justo congue auctor vitae et est. Donec
                    vel mattis diam, a commodo urna. In hac habitasse platea dictumst. Mauris
                    viverra eleifend orci eget volutpat. Nullam vitae lectus eros. Aenean eu auctor
                    urna. Quisque egestas eleifend diam rutrum blandit. Sed porta tellus sit amet
                    placerat volutpat. Praesent justo lectus, eleifend non aliquet id, porta ut
                    nisi. Nam in nibh at eros iaculis laoreet gravida sed sem. Nullam id tincidunt
                    elit. In placerat lacus sit amet dolor commodo, in blandit neque tempus. Quisque
                    eros urna, accumsan sit amet suscipit et, iaculis sit amet nisi. In porttitor
                    est in tellus laoreet, quis pellentesque tellus placerat. Nulla hendrerit
                    ultrices lacus, a convallis massa hendrerit aliquam. Ut sem est, aliquam at
                    mollis viverra, condimentum id metus. Nunc varius eget justo sed dapibus.
                    Quisque mollis maximus dictum. Cras faucibus purus eu augue imperdiet laoreet.
                    Nullam dapibus risus ut turpis interdum, ut molestie lorem facilisis. Sed
                    ultricies ante odio, non commodo ligula interdum ac. Vestibulum ac orci eu
                    tortor pharetra finibus at a felis.
                  </StyledCardBodyMinimized>
                  <StyledDivButton>
                    <ButtonIcon text={'Reply'} iconSvg={reply} />
                    <StyledDivSpacing></StyledDivSpacing>
                    <ButtonWithoutIcon text={'Forward Email'} />
                  </StyledDivButton>
                </Card>
              </Scrollable>
            </StyledDivContainer>
          </Column>
        </Row>
      </>
    );
  }
}

export default Email;
