import {
  AGENCY_AGENT_INVITATION,
  AGENCY_MANAGER_INVITATION,
} from '../../shared/constants/agency-invitations';

export const agentsMapSelectOptions = (agent) => {
  if (agent.value !== undefined && agent.label !== undefined) {
    return agent;
  }

  return {
    id: agent.id,
    value: agent.id,
    label: `${agent.user.firstName} ${agent.user.lastName}`,
    imgUrl: agent.user.avatar
      ? agent.user.avatar.previewUrl
      : 'https://randomuser.me/api/portraits/women/17.jpg',
  };
};

export const agentsSelectValues = (agents) => {
  return agents !== null ? agents.map(agentsMapSelectOptions) : [];
};

/**
 * Function for transform the array of agents
 * to an string like "Jonh Due & 6 others"
 *
 * @param {Array} agents - The list of agents.
 * @returns {string} - The formated text.
 */
export const getAssignedAgents = (agents) => {
  if (!agents || agents.length === 0) {
    return '';
  }

  const firstUser = agents[0];
  let firstUserFullName;

  if (firstUser.label !== undefined) {
    firstUserFullName = firstUser.label;
  } else {
    firstUserFullName = `${firstUser.user.firstName} ${firstUser.user.lastName}`;
  }

  const othersUsers = agents.length > 1 ? `& ${agents.length - 1} others` : '';

  return `${firstUserFullName} ${othersUsers}`;
};

/**
 * Function for transform the array of agentEmails & managerEmails
 * to a list of invitations.
 *
 * @param {Array} agentEmails - The list of agent emails.
 * @param {Array} managerEmails - The list of manager emails.
 * @param {Array} adminEmails - The list of admin emails.
 * @returns {Array} The agencyInvitations.
 */
export const generateInvitationsFromEmails = (agentEmails, managerEmails, adminEmails) => {
  const agentInvitations = agentEmails.map((email) => {
    return {
      email,
      type: AGENCY_AGENT_INVITATION,
    };
  });
  const managerInvitations = managerEmails.map((email) => {
    return {
      email,
      type: AGENCY_MANAGER_INVITATION,
    };
  });
  const agencyInvitations = agentInvitations.concat(managerInvitations);
  const companyInvitations = adminEmails.map((email) => {
    return {
      email,
    };
  });

  return { agencyInvitations, companyInvitations };
};
