import gql from 'graphql-tag';

export const AGENTS_LIST_QUERY = gql`
  query {
    agentsList {
      items {
        id
        user {
          id
          firstName
          lastName
          avatar {
            id
            downloadUrl
          }
        }
        agency {
          id
          name
        }
      }
    }
  }
`;

export const CREATE_AGENCY_MUTATION = gql`
  mutation CreateAgency($data: AgencyCreateInput!) {
    agencyCreate(data: $data) {
      id
      name
    }
  }
`;
