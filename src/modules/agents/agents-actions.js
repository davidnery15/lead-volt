import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';

import { AGENTS_LIST_QUERY } from './agents-queries';

import sessionStore, { APOLLO_CLIENT } from '../../shared/session/session-store';
import { AGENTS_ERROR_EVENT, AGENTS_LIST_EVENT } from './agents-store';

export const fetchAgents = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    response = await client.query({
      query: AGENTS_LIST_QUERY,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAgents', e);

    return Flux.dispatchEvent(AGENTS_ERROR_EVENT, e);
  }

  log('fetchAgents', response);
  Flux.dispatchEvent(AGENTS_LIST_EVENT, response.data);

  return response.data;
};
