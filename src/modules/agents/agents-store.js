import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Agents error.
 *
 * @type {string}
 */
export const AGENTS_ERROR_EVENT = 'onAgentsError';

/**
 * Event that triggers a Agents List event.
 *
 * @type {string}
 */
export const AGENTS_LIST_EVENT = 'onAgentsList';

class AgentsStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(AGENTS_ERROR_EVENT);
    this.addEvent(AGENTS_LIST_EVENT);
  }
}

export default new AgentsStore();
