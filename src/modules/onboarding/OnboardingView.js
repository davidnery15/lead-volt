import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { META_ONBOARDING_STEP_NAME } from '../../shared/constants/meta';
import { fetchSession, updateMeta } from '../auth/auth.actions';
import * as R from 'ramda';
import { ONBOARDING_SCREENS, NUMBER_OF_ONBOARDING_SCREENS } from './screens';
import MainLoader from '../../shared/components/ui/MainLoader';

class OnboardingView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      currentScreen: 0,
    };
    this.selectScreen = this.selectScreen.bind(this);
    this.nextScreen = this.nextScreen.bind(this);
    this.prevScreen = this.prevScreen.bind(this);
  }

  UNSAFE_componentWillMount() {
    // Placed this Process here to avoid the the blink of the first page of the onboarding between steps
    const { computedMatch } = this.props;
    if (computedMatch.params.step) {
      this.selectScreen(parseInt(computedMatch.params.step));
    }
  }

  selectScreen(selection) {
    this.setState({ currentScreen: selection });
  }

  async nextScreen(callUpdateMeta = true) {
    const currentScreen = R.clone(this.state.currentScreen);

    this.setState({ loading: true });

    if (callUpdateMeta) {
      await updateMeta(META_ONBOARDING_STEP_NAME, currentScreen + 1);
      await fetchSession();
    }

    this.setState({ currentScreen: currentScreen + 1, loading: false });
  }

  prevScreen() {
    this.setState({ currentScreen: this.state.currentScreen - 1 });
  }

  render() {
    const { prevScreen, nextScreen } = this;
    const { currentScreen, loading } = this.state;

    if (loading) return <MainLoader />;

    if (!currentScreen < NUMBER_OF_ONBOARDING_SCREENS) return <Redirect to={`/dashboard`} />;

    const Screen = ONBOARDING_SCREENS[this.state.currentScreen].component();

    return <Screen prevScreen={prevScreen} nextScreen={nextScreen} />;
  }
}

OnboardingView.propTypes = {
  computedMatch: PropTypes.object,
};

OnboardingView.defaultProps = {
  computedMatch: PropTypes.object,
};

export default OnboardingView;
