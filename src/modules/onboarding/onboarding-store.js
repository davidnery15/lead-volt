import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a createCompany.
 *
 * @type {string}
 */
export const CREATE_COMPANY_EVENT = 'onCreateCompany';

/**
 * Event that triggers a onboarding error.
 *
 * @type {string}
 */
export const ONBOARDING_ERROR_EVENT = 'onOnboardingError';

/**
 * Hold the Onboarding Data
 */
class OnboardingStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(CREATE_COMPANY_EVENT);
    this.addEvent(ONBOARDING_ERROR_EVENT);
  }
}

const onboardingStore = new OnboardingStore();

export { onboardingStore };

export default onboardingStore;
