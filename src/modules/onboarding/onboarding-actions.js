import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';
import { CREATE_COMPANY_MUTATION } from '../company/company-queries';
import sessionStore, { NEW_SESSION_EVENT, APOLLO_CLIENT } from '../../shared/session/session-store';
import { ONBOARDING_ERROR_EVENT, CREATE_COMPANY_EVENT } from './onboarding-store';
import { sanitize8BaseReferenceFromObject } from '../../shared/utils';
import { generateInvitationsFromEmails } from '../agents/agents-utils';
import { createCompanyValidator } from '../company/company-validators';

/**
 * Creates a company.
 *
 * @param {object} company - The companyData.
 * @param {string} token - The stripe token.
 * @param {string} stripeError - The stripe error object.
 * @returns {Promise} the data.
 */
export const createCompany = async (company, token, stripeError) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  try {
    createCompanyValidator(company, token, stripeError);
  } catch (e) {
    return Flux.dispatchEvent(ONBOARDING_ERROR_EVENT, e);
  }

  // set user as admin of the company
  company.adminCompanyRelation = {
    create: [
      {
        user: { connect: { id: user.id } },
      },
    ],
  };
  // Get stripeToken data
  const {
    id: tokenId,
    card: { name: cardHolderName, last4 },
  } = token;
  company.paymentMethodCompanyRelation = {
    create: [
      {
        tokenId,
        cardHolderName,
        last4,
      },
    ],
  };
  sanitize8BaseReferenceFromObject(company, 'subscriptionPlan');

  // Get agency data from company
  const { agencyName, agentEmails, managerEmails, adminEmails } = company;
  const { agencyInvitations, companyInvitations } = generateInvitationsFromEmails(
    agentEmails,
    managerEmails,
    adminEmails,
  );
  company.companyInvitationCompanyRelation = {
    create: companyInvitations,
  };
  company.agencyCompanyRelation = {
    create: [
      {
        name: agencyName,
        agencyInvitationAgencyRelation: { create: agencyInvitations },
        agencyManagerRelation: {
          create: [
            {
              user: { connect: { id: user.id } },
            },
          ],
        },
      },
    ],
  };
  // remove unncesary data
  delete company.id;
  delete company.cardHolderName;
  delete company.adminEmails;
  delete company.agencyName;
  delete company.agentEmails;
  delete company.managerEmails;

  log('company: ', company);
  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_COMPANY_MUTATION,
      variables: { data: company },
    });
  } catch (e) {
    error('createCompany: ', e);
    return Flux.dispatchEvent(ONBOARDING_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(CREATE_COMPANY_EVENT, response.data);

  log(`createCompany: `, response.data);
  return response.data;
};
