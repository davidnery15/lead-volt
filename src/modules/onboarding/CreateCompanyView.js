import React from 'react';
import { withApollo } from 'react-apollo';
import { withAuth } from '@8base/react-sdk';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin } from '../../shared/mixins';
import YesNoDialog from '../../shared/components/YesNoDialog';
import { CompanyModel } from '../company/company-model';
import * as R from 'ramda';
import { createCompany } from './onboarding-actions';
import onboardingStore, { ONBOARDING_ERROR_EVENT, CREATE_COMPANY_EVENT } from './onboarding-store';
import companyStore, {
  COMPANY_ERROR_EVENT,
  SUBSCRIPTION_PLANS_LIST_EVENT,
} from '../company/company-store';
import { fetchSubscriptionPlans } from '../company/company-actions';
import { CompanyForm } from './components/CompanyForm';
import { onErrorMixin } from '../../shared/mixins';
import { ElementsConsumer } from '@stripe/react-stripe-js';
import { createToken } from '../../shared/stripe/stripe';

class CreateCompanyView extends View {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isLoading: true,
      logoutIsOpen: false,
      subscriptionPlans: [],
      data: R.clone(CompanyModel),
    };

    this.onChangeData = onChangeDataMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.subscribe(onboardingStore, ONBOARDING_ERROR_EVENT, this.onError);

    this.subscribe(companyStore, COMPANY_ERROR_EVENT, this.onError);

    this.subscribe(companyStore, SUBSCRIPTION_PLANS_LIST_EVENT, (data) => {
      const {
        subscriptionPlansList: { items: subscriptionPlans },
      } = data;
      this.onChangeData('subscriptionPlan', subscriptionPlans[0]);
      this.setState({ isLoading: false, subscriptionPlans });
    });

    this.subscribe(onboardingStore, CREATE_COMPANY_EVENT, () => {
      this.setState({ isLoading: false, isOpen: false });
      this.props.nextScreen();
    });

    fetchSubscriptionPlans();
  }

  logout = async () => {
    const { auth, client } = this.props;
    await client.clearStore();
    auth.authClient.logout();
  };

  onOpenLogout = () => {
    this.setState({ logoutIsOpen: true });
  };

  onCloseLogout = () => {
    this.setState({ logoutIsOpen: false });
  };

  onSubmit = async (stripe, elements) => {
    const { data } = this.state;
    if (!stripe || !elements) return;
    this.setState({ isLoading: true });
    const company = R.clone(data);
    const { token, error } = await createToken(stripe, elements, company);
    createCompany(company, token, error);
  };

  render() {
    const { onChangeData, onSubmit, logout, onCloseLogout, onOpenLogout } = this;
    const { isOpen, data, logoutIsOpen, subscriptionPlans, isLoading } = this.state;

    return (
      <>
        <ElementsConsumer>
          {({ elements, stripe }) => {
            return (
              <CompanyForm
                isOpen={isOpen}
                data={data}
                subscriptionPlans={subscriptionPlans}
                onChange={onChangeData}
                onSubmit={() => onSubmit(stripe, elements)}
                onCloseForm={onOpenLogout}
                isLoading={isLoading}
              />
            );
          }}
        </ElementsConsumer>
        <YesNoDialog
          onYes={logout}
          onNo={onCloseLogout}
          text={'Are you sure you want to log out?'}
          isOpen={logoutIsOpen}
          title={'Logout'}
          onClose={onCloseLogout}
        />
      </>
    );
  }
}

export default withAuth(withApollo(CreateCompanyView));
