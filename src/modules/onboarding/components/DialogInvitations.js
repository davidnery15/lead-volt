import React from 'react';
import PropTypes from 'prop-types';
import { Dialog, Button, Card, Row, Column, Heading, Text, Avatar } from '@8base/boost';
import { PrimaryBtn } from '../../../shared/components/ui/buttons/PrimaryBtn';
import { AGENCY_INVITATION_PENDING } from '../../../shared/constants/agency-invitations';
import { COMPANY_INVITATION_PENDING } from '../../../shared/constants/company-invitations';

const DialogInvitations = ({
  isOpen,
  onCloseDialog,
  onClickAcceptAgencyInvitation,
  onClickDeclineAgencyInvitation,
  onClickAcceptCompanyInvitation,
  onClickDeclineCompanyInvitation,
  isLoading,
  size,
  agencyInvitations,
  companyInvitations,
  title,
}) => {
  return (
    <Dialog size={size} isOpen={isOpen}>
      <Dialog.Header title={title} onClose={onCloseDialog} />
      <Dialog.Body padding={'none'}>
        {agencyInvitations.map((invitation, index) => {
          const { type, createdBy, agency, status } = invitation;
          const { firstName, lastName, avatar } = createdBy;
          const avatarUrl = avatar ? avatar.previewUrl : '';
          const username = `${firstName} ${lastName}`;
          const agencyName = agency ? `${agency.name}` : '';
          const header = `${username} invited you to join ${agencyName}`;
          const description = `Agency ${type} Invitation.`;
          const showButtons = status === AGENCY_INVITATION_PENDING;

          return (
            <Card.Section key={index} padding={'none'}>
              <Row offsetY={'lg'} offsetX={'lg'}>
                <Column>
                  <Avatar size={'sm'} firstName={firstName} lastName={lastName} src={avatarUrl} />
                </Column>
                <Column stretch>
                  <Row style={{ paddingTop: '5px' }}>{<Heading type="h3" text={header} />}</Row>
                  <Row>{<Text color={'TEXT_LIGHT_GRAY'}>{description}</Text>}</Row>
                  <Row>
                    {showButtons ? (
                      <>
                        <PrimaryBtn
                          loading={isLoading}
                          onClick={() => onClickAcceptAgencyInvitation(invitation)}>
                          {'Accept'}
                        </PrimaryBtn>
                        <Button
                          disabled={isLoading}
                          color="neutral"
                          onClick={() => onClickDeclineAgencyInvitation(invitation)}>
                          {'Decline'}
                        </Button>
                      </>
                    ) : (
                      <Text>{status}</Text>
                    )}
                  </Row>
                </Column>
              </Row>
            </Card.Section>
          );
        })}

        {companyInvitations.map((invitation, index) => {
          const { createdBy, company, status } = invitation;
          const { firstName, lastName, avatar } = createdBy;
          const avatarUrl = avatar ? avatar.previewUrl : '';
          const username = `${firstName} ${lastName}`;
          const companyName = company ? `${company.name}` : '';
          const header = `${username} invited you to join ${companyName}`;
          const description = `Company ADMIN Invitation.`;
          const showButtons = status === COMPANY_INVITATION_PENDING;

          return (
            <Card.Section key={index} padding={'none'}>
              <Row offsetY={'lg'} offsetX={'lg'}>
                <Column>
                  <Avatar size={'sm'} firstName={firstName} lastName={lastName} src={avatarUrl} />
                </Column>
                <Column stretch>
                  <Row style={{ paddingTop: '5px' }}>{<Heading type="h3" text={header} />}</Row>
                  <Row>{<Text color={'TEXT_LIGHT_GRAY'}>{description}</Text>}</Row>
                  <Row>
                    {showButtons ? (
                      <>
                        <PrimaryBtn
                          loading={isLoading}
                          onClick={() => onClickAcceptCompanyInvitation(invitation)}>
                          {'Accept'}
                        </PrimaryBtn>
                        <Button
                          disabled={isLoading}
                          color="neutral"
                          onClick={() => onClickDeclineCompanyInvitation(invitation)}>
                          {'Decline'}
                        </Button>
                      </>
                    ) : (
                      <Text>{status}</Text>
                    )}
                  </Row>
                </Column>
              </Row>
            </Card.Section>
          );
        })}
      </Dialog.Body>
    </Dialog>
  );
};

DialogInvitations.defaultProps = {
  size: 'md',
  title: 'Pending Invitations',
  isLoading: false,
};

DialogInvitations.propTypes = {
  size: PropTypes.string,
  title: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  agencyInvitations: PropTypes.array.isRequired,
  companyInvitations: PropTypes.array.isRequired,
  onClickAcceptAgencyInvitation: PropTypes.func.isRequired,
  onClickDeclineAgencyInvitation: PropTypes.func.isRequired,
  onClickAcceptCompanyInvitation: PropTypes.func.isRequired,
  onClickDeclineCompanyInvitation: PropTypes.func.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};

export { DialogInvitations };
