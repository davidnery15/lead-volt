import React from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Column, InputField } from '@8base/boost';
import { PlanCard } from '../../company/components/PlanCard';
import { DialogForm, FormTitle, TagsInput } from '../../../shared/components';
import {
  IconCompanyInformation,
  IconBillingInformation,
  IconAgencyInformation,
  IconSubscriptionPlan,
} from '../../../shared/components/ui/Icons';
import { CardElement } from '@stripe/react-stripe-js';
import { Label } from '../../../shared/components/ui/inputs/Label';
import { options } from '../../../shared/stripe/stripe';

const CompanyForm = ({
  isOpen,
  onCloseForm,
  onChange,
  data,
  onSubmit,
  subscriptionPlans,
  isLoading,
}) => {
  return (
    <div>
      <DialogForm
        size={'xxl'}
        isOpen={isOpen}
        title="Please complete the onboarding process to finalize"
        onCloseDialog={onCloseForm}
        onClickCreate={onSubmit}
        createText={'Pay and Continue'}
        isLoading={isLoading}>
        <Form>
          <Form.SectionBody>
            <Row gap={'xl'}>
              <Column style={{ width: '50%' }}>
                <FormTitle
                  offsetTop={'none'}
                  text={'COMPANY INFORMATION'}
                  Icon={IconCompanyInformation}
                />
                <InputField
                  stretch
                  label="Company Name"
                  placeholder="Company Name"
                  input={{
                    name: 'name',
                    type: 'text',
                    value: data.name,
                    onChange: (value) => onChange('name', value),
                  }}
                />
                <InputField
                  stretch
                  label="Tax ID"
                  placeholder="Tax ID"
                  input={{
                    name: 'taxId',
                    type: 'text',
                    value: data.taxId,
                    onChange: (value) => onChange('taxId', value),
                  }}
                />
                <FormTitle text={'SUBSCRIPTION PLAN'} Icon={IconSubscriptionPlan} />
                {subscriptionPlans.map((plan, index) => {
                  return (
                    <PlanCard
                      key={index}
                      plan={plan}
                      selected={data.subscriptionPlan}
                      onSelect={(value) => onChange('subscriptionPlan', value)}
                    />
                  );
                })}
              </Column>
              <Column style={{ width: '50%' }}>
                <FormTitle
                  text={'BILLING INFORMATION'}
                  Icon={IconBillingInformation}
                  offsetTop={'none'}
                />
                <Label text={'Card information'} />
                <CardElement options={options} />
                <InputField
                  label="CardHolder Name"
                  placeholder="CardHolder Name"
                  input={{
                    name: 'cardHolderName',
                    type: 'text',
                    value: data.cardHolderName,
                    onChange: (value) => onChange('cardHolderName', value),
                  }}
                />
                <FormTitle text={'FIRST AGENCY INFORMATION'} Icon={IconAgencyInformation} />
                <InputField
                  label="First Agency Name"
                  placeholder="Agency Name"
                  input={{
                    name: 'agencyName',
                    type: 'text',
                    value: data.agencyName,
                    onChange: (value) => onChange('agencyName', value),
                  }}
                />
                <TagsInput
                  label={'Invite agents to this agency'}
                  placeholder={'Type an email and press enter'}
                  onChange={(email) => onChange('agentEmails', email)}
                  tags={data.agentEmails}
                />
                <TagsInput
                  label={'Invite managers to this agency'}
                  placeholder={'Type an email and press enter'}
                  onChange={(email) => onChange('managerEmails', email)}
                  tags={data.managerEmails}
                />
                <TagsInput
                  label={'Invite admins to this company'}
                  placeholder={'Type an email and press enter'}
                  onChange={(email) => onChange('adminEmails', email)}
                  tags={data.adminEmails}
                />
              </Column>
            </Row>
          </Form.SectionBody>
        </Form>
      </DialogForm>
    </div>
  );
};

CompanyForm.propTypes = {
  data: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCloseForm: PropTypes.func.isRequired,
  subscriptionPlans: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export { CompanyForm };
