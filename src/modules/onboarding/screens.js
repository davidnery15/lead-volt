import { CreateCompanyView, InvitationsView } from './';
import sessionStore, { NEW_SESSION_EVENT } from 'shared/session/session-store';
import { getPendingInvitations } from '../../shared/utils/invitations-utils';

/**
 * ONBOARDING_SCREENS.
 *
 * @type {Array}
 */
export const ONBOARDING_SCREENS = [
  {
    component: () => {
      const { agencyInvitationsList, companyInvitationsList } = sessionStore.getState(
        NEW_SESSION_EVENT,
      );
      const [pendingAgencyInvitations, pendingCompanyInvitations] = getPendingInvitations(
        agencyInvitationsList,
        companyInvitationsList,
      );

      // show invitations view if the user has invitations
      if (pendingAgencyInvitations.length > 0 || pendingCompanyInvitations.length > 0)
        return InvitationsView;
      else return CreateCompanyView;
    },
  },
];

export const NUMBER_OF_ONBOARDING_SCREENS = ONBOARDING_SCREENS.length;
