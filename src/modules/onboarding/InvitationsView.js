import React from 'react';
import { withApollo } from 'react-apollo';
import { withAuth } from '@8base/react-sdk';
import View from '@cobuildlab/react-flux-state';
import YesNoDialog from '../../shared/components/YesNoDialog';
import { fetchSession } from '../auth/auth.actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../shared/session/session-store';
import { DialogInvitations } from './components/DialogInvitations';
import agencyInvitationStore, {
  AGENCY_INVITATION_ERROR_EVENT,
  DECLINE_AGENCY_INVITATION_EVENT,
  ACCEPT_AGENCY_INVITATION_EVENT,
} from '../agency-invitation/agency-invitation-store';
import companyInvitationStore, {
  COMPANY_INVITATION_ERROR_EVENT,
  DECLINE_COMPANY_INVITATION_EVENT,
  ACCEPT_COMPANY_INVITATION_EVENT,
} from '../company-invitation/company-invitation-store';
import {
  declineAgencyInvitation,
  acceptAgencyInvitation,
} from '../agency-invitation/agency-invitation-actions';
import {
  declineCompanyInvitation,
  acceptCompanyInvitation,
} from '../company-invitation/company-invitation-actions';
import * as R from 'ramda';
import { onErrorMixin } from '../../shared/mixins';
import { getPendingInvitations } from '../../shared/utils/invitations-utils';

class InvitationsView extends View {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isLoading: false,
      logoutIsOpen: false,
      agencyInvitations: [],
      companyInvitations: [],
    };

    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.subscribe(agencyInvitationStore, AGENCY_INVITATION_ERROR_EVENT, this.onError);

    this.subscribe(agencyInvitationStore, ACCEPT_AGENCY_INVITATION_EVENT, async () => {
      await fetchSession();
      this.setInvitations();
    });

    this.subscribe(agencyInvitationStore, DECLINE_AGENCY_INVITATION_EVENT, async () => {
      await fetchSession();
      this.setInvitations();
    });

    this.subscribe(companyInvitationStore, COMPANY_INVITATION_ERROR_EVENT, this.onError);

    this.subscribe(companyInvitationStore, ACCEPT_COMPANY_INVITATION_EVENT, async () => {
      await fetchSession();
      this.setInvitations();
    });

    this.subscribe(companyInvitationStore, DECLINE_COMPANY_INVITATION_EVENT, async () => {
      await fetchSession();
      this.setInvitations();
    });

    this.setInvitations();
  }

  setInvitations = () => {
    const { agencyInvitationsList, companyInvitationsList } = sessionStore.getState(
      NEW_SESSION_EVENT,
    );
    const [pendingAgencyInvitations, pendingCompanyInvitations] = getPendingInvitations(
      agencyInvitationsList,
      companyInvitationsList,
    );

    if (!pendingAgencyInvitations.length && !pendingCompanyInvitations.length) {
      return this.props.nextScreen();
    }

    this.setState({
      isLoading: false,
      agencyInvitations: agencyInvitationsList.items,
      companyInvitations: companyInvitationsList.items,
    });
  };

  logout = async () => {
    const { auth, client } = this.props;

    await client.clearStore();
    auth.authClient.logout();
  };

  onOpenLogout = () => {
    this.setState({ logoutIsOpen: true });
  };

  onCloseLogout = () => {
    this.setState({ logoutIsOpen: false });
  };

  onAcceptAgencyInvitation = (invitation) => {
    this.setState({ isLoading: true });

    acceptAgencyInvitation(R.clone(invitation));
  };

  onDeclineAgencyInvitation = (invitation) => {
    this.setState({ isLoading: true });

    declineAgencyInvitation(R.clone(invitation));
  };

  onAcceptCompanyInvitation = (invitation) => {
    this.setState({ isLoading: true });

    acceptCompanyInvitation(R.clone(invitation));
  };

  onDeclineCompanyInvitation = (invitation) => {
    this.setState({ isLoading: true });

    declineCompanyInvitation(R.clone(invitation));
  };

  render() {
    const {
      onAcceptAgencyInvitation,
      onDeclineAgencyInvitation,
      onAcceptCompanyInvitation,
      onDeclineCompanyInvitation,
      logout,
      onCloseLogout,
      onOpenLogout,
    } = this;
    const { isOpen, agencyInvitations, companyInvitations, logoutIsOpen, isLoading } = this.state;

    return (
      <>
        <DialogInvitations
          isOpen={isOpen}
          agencyInvitations={agencyInvitations}
          companyInvitations={companyInvitations}
          onClickAcceptAgencyInvitation={onAcceptAgencyInvitation}
          onClickDeclineAgencyInvitation={onDeclineAgencyInvitation}
          onClickAcceptCompanyInvitation={onAcceptCompanyInvitation}
          onClickDeclineCompanyInvitation={onDeclineCompanyInvitation}
          onCloseDialog={onOpenLogout}
          isLoading={isLoading}
        />
        <YesNoDialog
          onYes={logout}
          onNo={onCloseLogout}
          text={'Are you sure you want to log out?'}
          isOpen={logoutIsOpen}
          title={'Logout'}
          onClose={onCloseLogout}
        />
      </>
    );
  }
}

export default withAuth(withApollo(InvitationsView));
