import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Agency error.
 *
 * @type {string}
 */
export const AGENCY_ERROR_EVENT = 'onAgencyError';

/**
 * Event that triggers a Get Active Agency event.
 *
 * @type {string}
 */
export const AGENCY_GET_ACTIVE_EVENT = 'onGetActiveAgency';

/**
 * Event that triggers a Set Active Agency event.
 *
 * @type {string}
 */
export const AGENCY_SET_ACTIVE_EVENT = 'onSetActiveAgency';

class AgencyStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(AGENCY_ERROR_EVENT);
    this.addEvent(AGENCY_GET_ACTIVE_EVENT);
    this.addEvent(AGENCY_SET_ACTIVE_EVENT);
  }
}

export default new AgencyStore();
