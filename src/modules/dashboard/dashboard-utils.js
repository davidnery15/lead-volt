import moment from 'moment';

/**
 * Create data week, day, weekday and full date.
 *
 * @param date
 * @returns {Array}
 */
export const getWeekForCalendar = (date) => {
  const week = [];

  for (let i = 0, j = 7; i < j; i++) {
    const dataWeek = {};
    const _date = moment(date).weekday(i)._d;

    dataWeek.day = moment(_date).format('DD');
    dataWeek.weekday = moment(_date)
      .weekday(i)
      .format('dd')
      .substr(0, 1);
    dataWeek.fullDate = moment(_date).format('YYYY-MM-DD');
    week.push(dataWeek);
  }

  return week;
};
