import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Grid, Row } from '@8base/boost';
import PriorityBadge from '../../../shared/components/ui/PriorityBadge';

const TaskContainer = styled.div`
  padding: 15px 24px;
  border-bottom: 1px solid #e9eff4;

  &:last-child {
    border-bottom: 0;
  }
`;

const TaskTitle = styled.h3`
  height: 20px;
  width: 486px;
  opacity: 0.9;
  color: #3db4aa;
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  padding: 0;
  cursor: pointer;
`;

const TaskText = styled.p`
  height: 19px;
  width: 446px;
  opacity: 0.9;
  color: #323c47;
  font-family: 'Poppins';
  font-size: 13px;
  line-height: 20px;
  padding: 0;
  margin-top: 2px;
`;

const AssignedByText = styled.span`
  font-weight: 600;
`;

const Task = ({ title, text, priority, assignedBy }) => {
  return (
    <TaskContainer>
      <Grid.Layout columns="70% 30%">
        <Grid.Box>
          <TaskTitle>{title}</TaskTitle>
          <TaskText>
            {assignedBy !== '' && <AssignedByText>{assignedBy} </AssignedByText>}
            {text}
          </TaskText>
        </Grid.Box>
        <Row alignItems="center" justifyContent="end">
          <PriorityBadge priority={priority} />
        </Row>
      </Grid.Layout>
    </TaskContainer>
  );
};

Task.propTypes = {
  assignedBy: PropTypes.string,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  priority: PropTypes.number.isRequired,
};

Task.defaultProps = {
  assignedBy: '',
};

export default Task;
