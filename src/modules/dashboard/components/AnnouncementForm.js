import React from 'react';
import PropTypes from 'prop-types';
import { Form, Grid, Dialog, Button, InputField, SelectField, TextAreaField } from '@8base/boost';
import ImageUpload from '../../../shared/components/ImageUpload';
import { PrimaryBtn } from '../../../shared/components/ui/buttons/PrimaryBtn';

const QuoteForm = ({ isOpen, isEdit, onCloseDialog }) => {
  return (
    <Dialog size="lg" isOpen={isOpen}>
      <Dialog.Header title={`${isEdit ? 'Edit' : 'Create'} Announcement`} onClose={onCloseDialog} />
      <Dialog.Body>
        <Form>
          <Form.Section>
            <Form.SectionBody>
              <Grid.Layout inline gap="md" columns="65% 20%">
                <Grid.Box>
                  <InputField
                    stretch
                    label="Title"
                    placeholder="Announcement Title"
                    input={{
                      name: 'title',
                      type: 'text',
                      value: '',
                    }}
                  />
                </Grid.Box>
                <Grid.Box>
                  <SelectField
                    label="Type"
                    name="type"
                    placeholder="Select Type"
                    options={[
                      {
                        label: 'type 1',
                        value: 1,
                      },
                    ]}
                    stretch={false}
                  />
                </Grid.Box>
              </Grid.Layout>
              <Grid.Layout gap="md">
                <Grid.Box>
                  <TextAreaField
                    stretch
                    rows="4"
                    label="Content"
                    input={{
                      name: 'input',
                      value: '',
                      onChange: () => null,
                    }}
                  />
                </Grid.Box>
                <Grid.Box>
                  <ImageUpload maxFiles={1} label="Add Image" accept={['image/png', 'image/jpg']} />
                </Grid.Box>
                <Grid.Box>
                  <InputField
                    stretch
                    label="Image Title"
                    placeholder="Image Title"
                    input={{
                      name: 'image_title',
                      type: 'text',
                      value: '',
                    }}
                  />
                </Grid.Box>
              </Grid.Layout>
            </Form.SectionBody>
          </Form.Section>
        </Form>
      </Dialog.Body>
      <Dialog.Footer>
        <Button color="neutral" onClick={onCloseDialog}>
          Cancel
        </Button>
        <PrimaryBtn>{isEdit ? 'Save' : 'Create'}</PrimaryBtn>
      </Dialog.Footer>
    </Dialog>
  );
};

QuoteForm.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
};

export default QuoteForm;
