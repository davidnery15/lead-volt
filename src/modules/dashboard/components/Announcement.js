import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Grid, Paper, Heading, Row } from '@8base/boost';
import styled from 'styled-components';
import AnnouncementForm from './AnnouncementForm';
import { PrimaryBtn } from '../../../shared/components/ui/buttons/PrimaryBtn';

const StyledPaper = styled(Paper)`
  position: relative;
  min-height: 264px;
  min-width: 646px;
  background-color: #f3fafa !important;
`;

const Title = styled(Heading)`
  opacity: 0.9 !important;
  color: #1e4358 !important;
  font-size: 36px !important;
  line-height: 55px !important;
  font-weight: 600 !important;
  margin-top: 10px !important;
  height: 51px !important;
`;

const SubTitle = styled(Heading)`
  color: #323c47 !important;
  font-size: 11px !important;
  letter-spacing: 1px;
  line-height: 27px;
  font-weight: 600;
  margin-top: 10px !important;
  margin-bottom: 10px !important;
`;

const Text = styled.p`
  width: 558px;
  color: #1e4358;
  font-family: 'Poppins';
  font-size: 18px;
  font-style: italic;
  line-height: 24px;
`;

const Author = styled.p`
  color: #939ea7;
  font-family: 'Poppins';
  font-size: 12px;
  line-height: 18px;
  margin-top: 15px;
  margin-left: 15px;
`;

const EditButton = styled.span`
  position: absolute;
  bottom: 20px;
  right: 20px;
  height: 20px;
  width: 29px;
  opacity: 0.9;
  color: #3db4aa;
  font-family: 'Poppins';
  font-size: 14px;
  line-height: 21px;
  cursor: pointer;
`;

const Image = styled.img`
  height: 82px;
  width: 107px;
  object-fit: contain;
  max-width: 100%;
  margin-top: 30px;
`;

const TextHelp = styled.p`
  color: #cfd4d4;
  font-family: Poppins;
  font-size: 12px;
  line-height: 18px;
  margin-top: 15px;
`;

const Quote = styled.span`
  width: 15px;
  color: #384a59;
  font-family: Poppins;
  font-size: 40px;
  font-style: italic;
  line-height: 24px;
  margin-left: -8px;
  height: 51px;
`;

const Announcement = ({ announcement }) => {
  const [isOpen, setIsOpen] = useState(false);
  const handleOnClickEdit = () => setIsOpen(true);
  const handleOnCloseDialog = () => setIsOpen(false);

  return (
    <StyledPaper padding="lg">
      {announcement && Object.keys(announcement).length ? (
        <>
          <Title type="h1">{announcement.title}</Title>
          <SubTitle>QUOTE OF THE DAY</SubTitle>
          <Row alignItems="center">
            <Quote>“</Quote>
            <Text>{announcement.content}”</Text>
          </Row>
          <Author>{announcement.author}</Author>
          <EditButton onClick={handleOnClickEdit}>Edit</EditButton>
          <AnnouncementForm isEdit={false} isOpen={isOpen} onCloseDialog={handleOnCloseDialog} />
        </>
      ) : (
        <>
          <Grid.Layout inline columns="auto auto">
            <Grid.Box>
              <Title type="h1">Morning, Nicolas!</Title>
            </Grid.Box>
            <Grid.Box style={{ alignItems: 'flex-end' }}>
              <PrimaryBtn>Launch Lead Manager</PrimaryBtn>
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout>
            <Grid.Box style={{ alignItems: 'center' }}>
              <Image alt="No News" src={require('../../../shared/assets/images/news.svg')} />
              <TextHelp>There are currently no announcements</TextHelp>
            </Grid.Box>
          </Grid.Layout>
        </>
      )}
    </StyledPaper>
  );
};

Announcement.propTypes = {
  announcement: PropTypes.object.isRequired,
};

export default Announcement;
