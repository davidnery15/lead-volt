import React from 'react';
import { Grid, Paper, Row, Select } from '@8base/boost';
import Announcement from './components/Announcement';
import {
  TableLite,
  CardIndicator,
  TableHeader,
  Title,
  PaperHeader,
  CustomLink,
  PlusBtn,
  WeekComponent,
  SecondaryTitle,
  AgentsSlider,
} from 'shared/components';
import styled from 'styled-components';
import { fetchCampaigns } from '../campaigns/campaigns-actions';
import campaignsStore, { CAMPAIGN_LIST_EVENT } from '../campaigns/campaigns-store';
import View from '@cobuildlab/react-flux-state';
import { campaignMapDashboardTable } from '../campaigns/campaigns-utils';
import moment from 'moment';
import { getWeekForCalendar } from './dashboard-utils';
import { getActiveAgency } from '../../shared/utils/agency-utils';
import Task from './components/Task';
import { IconPerformanceGreen } from 'shared/components/ui/Icons';
import { nameCellComponent } from '../../shared/components/campaigns/nameCellComponent';

const StyledLayout = styled(Grid.Layout)`
  margin-bottom: 30px;
`;

const tasks = [
  {
    name: 'Task #! name lorem ipsum dolor',
    description: 'assigned your this item 9 hours ago',
    assignedBy: 'Barry Robinson',
    priority: 2,
  },
  {
    name: 'Call with John Doe',
    description: 'Scheduled for 3:30 PM',
    priority: 1,
  },
  {
    name: 'Task #! name lorem ipsum dolor',
    description: 'assigned your this item 9 hours ago',
    assignedBy: 'Barry Robinson',
    priority: 3,
  },
];
const agents = [
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
    position: 1,
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
    position: 2,
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
    position: 3,
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
  {
    name: 'Nicolas Muiño',
    imgUrl: 'https://randomuser.me/api/portraits/women/17.jpg',
    performance: '$45,000',
  },
];

class Dashboard extends View {
  constructor(props) {
    super(props);

    this.state = {
      week: [],
      campaigns: [],
      isLoadingCampaigns: false,
      selectedDay: moment().format('YYYY-MM-DD'),
      dateReference: moment().format('YYYY-MM-DD'),
    };

    this.subscribe(campaignsStore, CAMPAIGN_LIST_EVENT, (data) => {
      const {
        campaignsList: { items },
      } = data;

      this.setState({
        isLoadingCampaigns: false,
        campaigns: items.map(campaignMapDashboardTable),
      });
    });
  }

  getAgencyId = () => {
    const agency = getActiveAgency();
    return agency.id;
  };

  componentDidMount() {
    console.log(this.getAgencyId());
    this.setState({ isLoadingCampaigns: true });
    fetchCampaigns({ first: 5, filterData: { agency: this.getAgencyId } });

    // add this code in the tasks event.
    const week = getWeekForCalendar(this.state.dateReference);

    this.setState({ week });
  }

  onSelectDay = (selectedDay) => {
    this.setState({ selectedDay });
  };

  onRightClick = () => {
    const dateReference = moment(this.state.dateReference).add(7, 'days');
    const week = getWeekForCalendar(dateReference);

    this.setState({
      week,
      dateReference,
    });
  };

  onLeftClick = () => {
    const dateReference = moment(this.state.dateReference).subtract(7, 'days');
    const week = getWeekForCalendar(dateReference);

    this.setState({
      week,
      dateReference,
    });
  };

  render() {
    const {
      state: { isLoadingCampaigns, campaigns, week, selectedDay },
      onSelectDay,
      onLeftClick,
      onRightClick,
    } = this;
    const selectedDayFormat = moment(selectedDay).format('dddd MMMM Do');
    const momentSelectedDay = moment(selectedDay);
    const fromNowDate =
      moment().diff(momentSelectedDay, 'days') >= 1
        ? momentSelectedDay.fromNow()
        : momentSelectedDay.calendar().split(' ')[0];

    return (
      <div className="p24">
        <StyledLayout columns="48% calc(52% - 24px)" gap="lg">
          <Grid.Box>
            <Grid.Layout gap="lg">
              <Grid.Box>
                <Announcement
                  announcement={{
                    title: 'Morning, Nicolas!',
                    author: '- Quote author, lorem ipsum',
                    content:
                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus gravida in sollicitudin. Suspendisse. Consenectur dolor asimet et al consenectur lorem ipsum dolor asimet.',
                  }}
                />
              </Grid.Box>
              <Grid.Box>
                <Grid.Layout gap="xl" columns="30% 30% 30%">
                  <Grid.Box>
                    <CardIndicator
                      title="126"
                      text="New Leads in the last 24 hours"
                      link={{ path: '/leads', name: 'Manage Leads' }}
                    />
                  </Grid.Box>
                  <Grid.Box>
                    <CardIndicator
                      title="16"
                      text="Sales in the last
                      month"
                      link={{ path: '/leads', name: 'Manage Leads' }}
                    />
                  </Grid.Box>
                  <Grid.Box>
                    <CardIndicator
                      title="#1"
                      text="Is your position in the sales ranking"
                      link={{ path: '/performance', name: 'Performance' }}
                    />
                  </Grid.Box>
                </Grid.Layout>
              </Grid.Box>
              <Paper>
                <TableHeader
                  title="Campaigns"
                  link={{ path: '/campaigns', name: 'View all Campaigns' }}
                />
                <TableLite
                  data={campaigns}
                  isLoading={isLoadingCampaigns}
                  columnsSize="35% 32% 23% 5%"
                  columns={[
                    {
                      name: 'CAMPAIGN NAME',
                      customBodyRender: nameCellComponent,
                    },
                    { name: 'ASSIGNED BY' },
                    { name: 'DUE DATE' },
                  ]}
                  dropdownOptions={[
                    { name: 'Details', onClick: () => {} },
                    { name: 'Edit', onClick: () => {} },
                  ]}
                  ignoreColumns={['id']}
                />
              </Paper>
            </Grid.Layout>
          </Grid.Box>
          <Grid.Box>
            <Grid.Layout gap="lg">
              <Grid.Box>
                <Paper>
                  <PaperHeader>
                    <Grid.Layout columns="55% 10% 35%">
                      <Row alignItems="center">
                        <Title>
                          Tasks for {fromNowDate}, {selectedDayFormat}
                        </Title>
                      </Row>
                      <Grid.Box>
                        <PlusBtn />
                      </Grid.Box>
                      <Row justifyContent="end" alignItems="center">
                        <CustomLink to="/tasks">View all Tasks</CustomLink>
                      </Row>
                    </Grid.Layout>
                  </PaperHeader>
                  <div className="p24">
                    <Row alignItems="center" justifyContent="center">
                      <WeekComponent
                        week={week}
                        onSelectDay={onSelectDay}
                        selectedDay={selectedDay}
                        onLeftClick={onLeftClick}
                        onRightClick={onRightClick}
                      />
                    </Row>
                  </div>
                  <div>
                    {tasks.map((task, key) => {
                      return (
                        <Task
                          key={key}
                          title={task.name}
                          text={task.description}
                          priority={task.priority}
                          assignedBy={task.assignedBy}
                        />
                      );
                    })}
                  </div>
                </Paper>
              </Grid.Box>
              <Grid.Box>
                <Paper>
                  <PaperHeader>
                    <Grid.Layout columns="60% 40%">
                      <Row alignItems="center">
                        <Title>Top Performers</Title>
                      </Row>
                      <Row alignItems="center" justifyContent="end">
                        <IconPerformanceGreen size="sm" />
                        <SecondaryTitle>Performance Overview</SecondaryTitle>
                      </Row>
                    </Grid.Layout>
                  </PaperHeader>
                  <Row justifyContent="center">
                    <div style={{ width: '220px', paddingTop: '24px' }}>
                      <Select
                        name="name"
                        value={1}
                        placeholder="Select an option"
                        options={[
                          {
                            label: 'Sales - Last Quarter',
                            value: 1,
                          },
                        ]}
                        stretch
                      />
                    </div>
                  </Row>
                  <div className="p24">
                    <AgentsSlider agents={agents} />
                  </div>
                </Paper>
              </Grid.Box>
            </Grid.Layout>
          </Grid.Box>
        </StyledLayout>
      </div>
    );
  }
}

export default Dashboard;
