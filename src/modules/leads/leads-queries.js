import gql from 'graphql-tag';

export const agentFragment = gql`
  fragment agentFragment on Agent {
    id
    user {
      id
      firstName
      lastName
    }
  }
`;

export const tasksFragment = gql`
  fragment tasksFragment on LeadTask {
    id
    title
    date
    priority
    description
    type {
      id
      name
    }
    agents {
      items {
        ...agentFragment
      }
    }
    lead {
      id
    }
  }
  ${agentFragment}
`;

export const notesFragment = gql`
  fragment notesFragment on LeadNote {
    id
    createdAt
    text
    title
    notesLeadRelation {
      id
    }
  }
`;

export const leadsFragment = gql`
  fragment leadsFragment on Lead {
    gender
    updatedAt
    phone {
      code
      number
    }
    phones
    id
    ifsScore
    firstName
    lastName
    middleName
    createdAt
    type
    birthDay
    birthMonth
    birthYear
    campaign {
      id
      name
    }
    contacts {
      items {
        id
        firstName
        lastName
        phoneNumber
      }
    }
    agents {
      items {
        ...agentFragment
      }
    }
    address {
      country
      street1
      street2
      zip
      city
      state
    }
    notesLeadRelation {
      items {
        ...notesFragment
      }
    }
    tasksLeadRelation {
      items {
        ...tasksFragment
      }
    }
    status
    priority
  }
  ${agentFragment}
  ${tasksFragment}
  ${notesFragment}
`;

export const LEADS_LIST_QUERY = gql`
  query($skip: Int, $first: Int, $filter: LeadFilter, $sort: [LeadSort!]) {
    leadsList(skip: $skip, first: $first, filter: $filter, sort: $sort) {
      count
      items {
        ...leadsFragment
      }
    }
  }
  ${leadsFragment}
`;

export const FILTER_OPTIONS_QUERY = gql`
  {
    leadSourcesList {
      items {
        id
        name
      }
    }
    agentsList {
      items {
        id
        user {
          firstName
          lastName
          id
        }
      }
    }
  }
`;

export const LEAD_AGENTS_UPDATE_MUTATION = gql`
  mutation($data: LeadUpdateInput!) {
    leadUpdate(data: $data) {
      id
      agents {
        items {
          id
          user {
            id
            firstName
            lastName
          }
        }
      }
    }
  }
`;

export const LEAD_CREATE_MUTATION = gql`
  mutation($data: LeadCreateInput!) {
    leadCreate(data: $data) {
      id
    }
  }
`;

export const LEAD_UPDATE_MUTATION = gql`
  mutation($data: LeadUpdateInput!) {
    leadUpdate(data: $data) {
      id
    }
  }
`;

export const LEAD_DELETE_MUTATION = gql`
  mutation($data: LeadDeleteInput!) {
    leadDelete(data: $data) {
      success
    }
  }
`;

export const TASK_CREATE_MUTATION = gql`
  mutation($data: LeadTaskCreateInput!) {
    leadTaskCreate(data: $data) {
      ...tasksFragment
    }
  }
  ${tasksFragment}
`;

export const NOTE_CREATE_MUTATION = gql`
  mutation($data: LeadNoteCreateInput!) {
    leadNoteCreate(data: $data) {
      ...notesFragment
    }
  }
  ${notesFragment}
`;

export const LEAD_TASK_TYPE_LIST_QUERY = gql`
  {
    leadTaskTypesList {
      items {
        id
        name
      }
    }
  }
`;

export const LEAD_DETAIL_QUERY = gql`
  query($id: ID) {
    lead(id: $id) {
      ...leadsFragment
    }
  }
  ${leadsFragment}
`;
