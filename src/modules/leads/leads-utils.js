import { TABLE_COLUMNS } from './leads-model';
import agentsStore, { AGENTS_LIST_EVENT } from '../agents/agents-store';
import { MANAGER, PRIORITY_OPTIONS } from '../../shared/constants';
import { userHasAnyRoleInAgency } from '../../shared/utils/agency-utils';

export const getLeadPropertyName = (columnName) => {
  columnName = columnName.replace(' ', '');
  const propertyName = TABLE_COLUMNS[columnName].property;
  return propertyName;
};

// export const canSort = (columnName) => {
//   columnName = columnName.replace(' ', '');
//   return TABLE_COLUMNS[columnName].property !== '';
// };

/**
 * Map to format the agents' data
 *
 * @param {Array} agents - the agents list
 * @returns {object}
 */
export const agentsMap = (agents) => {
  const assignedAgents = agents.map((item) => {
    return item.id;
  });
  const agencyAgents = agentsStore.getState(AGENTS_LIST_EVENT);
  return agencyAgents.agentsList.items.map((agent) => ({
    id: agent.id,
    label: `${agent.user.firstName} ${agent.user.lastName}`,
    checked: assignedAgents.includes(agent.id),
  }));
};

export const priorityMap = (optionList) => {
  return optionList.map((option) => ({ label: option.label, value: option.label }));
};

export const findPriority = (value) => {
  if (value === '') {
    return;
  }
  if (+value) {
    return PRIORITY_OPTIONS.find((option) => +option.value === +value).label;
  } else {
    return PRIORITY_OPTIONS.find((option) => option.label === value).value;
  }
};

/**
 * Map to format the numbers
 *
 * @param {object} lead
 * @returns {object}
 */
export const msgNumbersMap = (lead) => {
  const numbers = [];
  if (lead.phone) {
    numbers.push({
      label: `${lead.phone.code}-${lead.phone.number}`,
      value: lead.phone,
    });
  }
  if (lead.phones) {
    for (const number of lead.phones) {
      numbers.push({ label: number, value: number });
    }
  }
  for (const contact of lead.contacts.items) {
    numbers.push({
      label: `${contact.firstName} ${contact.lastName}: ${contact.phoneNumber}`,
      value: contact,
    });
  }
  return numbers;
};

/**
 * Map to format the type's data
 *
 * @param {Array} types
 * @returns {object}
 */
export const typesMap = (types) => {
  return types.map((type) => {
    return {
      label: type.name,
      value: type,
    };
  });
};

/**
 * Returns true if the user has the role Manager in the provided agency
 *
 * @param {object} user
 * @param {object} agency
 * @returns {bool}
 */
export const canSeeBatchActions = (user, agency) => {
  return userHasAnyRoleInAgency(user, agency, MANAGER);
};

/**
 * Returns true if the user has the role Manager in the provided agency
 *
 * @param {object} user
 * @param {object} agency
 * @returns {bool}
 */
export const canDeleteLeads = (user, agency) => {
  return userHasAnyRoleInAgency(user, agency, MANAGER);
};

/**
 * Returns true if the user has the role Manager in the provided agency
 *
 * @param {object} user
 * @param {object} agency
 * @returns {bool}
 */
export const canAssignAgents = (user, agency) => {
  return userHasAnyRoleInAgency(user, agency, MANAGER);
};
