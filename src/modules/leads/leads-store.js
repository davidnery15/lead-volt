import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers an error.
 *
 * @type {string}
 */
export const LEADS_ERROR_EVENT = 'onLeadError';

/**
 * Event that triggers a Lead List event.
 *
 * @type {string}
 */
export const LEADS_LIST_EVENT = 'onLeadList';

/**
 * Event that triggers a Lead List event.
 *
 * @type {string}
 */
export const LEADS_CREATE_EVENT = 'onLeadCreated';

export const FILTER_OPTIONS_EVENT = 'onFilterOptions';

export const FILTER_OPTIONS_ERROR_EVENT = 'onFilterOptionsError';

export const AGENTS_UPDATE_EVENT = 'onAgentsUpdate';

export const AGENTS_UPDATE_ERROR_EVENT = 'onAgentsUpdateError';

export const LEAD_UPDATE_ERROR_EVENT = 'onLeadUpdateError';

export const TASK_CREATE_EVENT = 'onTaskCreate';

export const TASK_ERROR_EVENT = 'onTaskError';

export const NOTE_ERROR_EVENT = 'onNoteError';

export const NOTE_CREATE_EVENT = 'onNoteCreate';

export const TASK_TYPE_LIST_EVENT = 'onTaskList';

export const TASK_TYPE_LIST_ERROR = 'onTaskListError';

export const LEAD_DETAIL_EVENT = 'onLeadDetail';

export const LEAD_DETAIL_ERROR_EVENT = 'onLeadDetailError';

class LeadStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(LEADS_ERROR_EVENT);
    this.addEvent(LEADS_LIST_EVENT);
    this.addEvent(LEADS_CREATE_EVENT);
    this.addEvent(FILTER_OPTIONS_ERROR_EVENT);
    this.addEvent(FILTER_OPTIONS_EVENT);
    this.addEvent(AGENTS_UPDATE_EVENT);
    this.addEvent(AGENTS_UPDATE_ERROR_EVENT);
    this.addEvent(LEAD_UPDATE_ERROR_EVENT);
    this.addEvent(TASK_CREATE_EVENT);
    this.addEvent(TASK_ERROR_EVENT);
    this.addEvent(NOTE_ERROR_EVENT);
    this.addEvent(NOTE_CREATE_EVENT);
    this.addEvent(TASK_TYPE_LIST_EVENT);
    this.addEvent(TASK_TYPE_LIST_ERROR);
    this.addEvent(LEAD_DETAIL_EVENT);
    this.addEvent(LEAD_DETAIL_ERROR_EVENT);
  }
}

export default new LeadStore();
