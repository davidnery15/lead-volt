import { ValidationError } from 'shared/errors';
import { isValidString, isValidNumber, isValidDate } from 'shared/validators';
import { PRIORITIES } from '../../shared/constants';
import { isValidEmail, isValidPhoneNumber } from '../../shared/validators';

const leadMessage = 'The lead must have a valid';
const messages = {
  lead: {
    firstName: leadMessage + ' first name',
    middleName: leadMessage + ' middle name',
    lastName: leadMessage + ' last name',
    gender: leadMessage + ' gender',
    birthMonth: leadMessage + ' birth month',
    birthDay: leadMessage + ' birth day',
    birthYear: leadMessage + ' birth year',
    email: leadMessage + ' email address',
    agents: 'The lead must have at least 1 Agent',
    phone: leadMessage + ' primary phone number',
    secondPhone: leadMessage + ' second phone number',
    extraPhone: leadMessage + ' extra phone number',
    addressLine1: leadMessage + ' address line 1',
    addressLine2: leadMessage + ' address line 2',
    city: leadMessage + ' city',
    zip: leadMessage + ' zip',
    state: leadMessage + ' state',
    type: leadMessage + ' lead type',
    priority: leadMessage + ' priority',
    creditScore: leadMessage + ' credit score',
  },
};

/**
 * Validate lead
 *
 * @param {object} lead
 */
export const createAndUpdateLeadValidator = (lead) => {
  let errorMessages = [];

  if (!isValidString(lead.firstName)) errorMessages.push(messages.lead.firstName);

  if (!isValidString(lead.middleName)) errorMessages.push(messages.lead.middleName);

  if (!isValidString(lead.lastName)) errorMessages.push(messages.lead.lastName);

  if (lead.gender !== 'MALE' && lead.gender !== 'FEMALE') errorMessages.push(messages.lead.gender);

  if (!isValidString(lead.birthMonth)) errorMessages.push(messages.lead.birthMonth);

  if (!isValidNumber(lead.birthDay)) errorMessages.push(messages.lead.birthDay);

  if (!isValidNumber(lead.birthYear)) errorMessages.push(messages.lead.birthYear);

  if (!isValidEmail(lead.email)) errorMessages.push(messages.lead.email);

  if (lead.agents.length === 0) errorMessages.push(messages.lead.agents);

  if (!isValidPhoneNumber(lead.phone.number)) errorMessages.push(messages.lead.phone);

  if (lead.phones !== '') {
    let secondPhone = '',
      extraPhone = '';

    if (lead.phones.includes(',')) {
      const phones = lead.phones.split(',');

      secondPhone = phones[0].trim();
      extraPhone = phones[1].trim();
    } else {
      secondPhone = lead.phones;
    }

    if (secondPhone !== '') {
      if (!isValidPhoneNumber(secondPhone)) errorMessages.push(messages.lead.secondPhone);
    }

    if (extraPhone !== '') {
      if (!isValidPhoneNumber(extraPhone)) errorMessages.push(messages.lead.extraPhone);
    }
  }

  if (!isValidString(lead.address.street1)) errorMessages.push(messages.lead.addressLine1);

  if (!isValidString(lead.address.street2)) errorMessages.push(messages.lead.addressLine2);

  if (!isValidString(lead.address.city)) errorMessages.push(messages.lead.city);

  if (!isValidNumber(lead.address.zip)) errorMessages.push(messages.lead.zip);

  if (!isValidString(lead.type)) errorMessages.push(messages.lead.type);

  if (!isValidNumber(lead.priority)) errorMessages.push(messages.lead.priority);

  if (!isValidNumber(lead.creditScore)) errorMessages.push(messages.lead.creditScore);

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};

/**
 * Validate that the Task has all the fields necessaries with the right format.
 *
 * @param {object} task - the task to be validated
 */
export const createTaskValidator = (task) => {
  let errorMessages = [];

  if (!isValidString(task.title)) errorMessages.push('The Task must have a valid title.');

  if (!isValidNumber(task.priority) && PRIORITIES.indexOf(task.priority) === -1)
    errorMessages.push('The task must have a valid priority');

  if (task.agents.length === 0) errorMessages.push('The task must have at least 1 Agent');

  if (!isValidString(task.description) || task.description.lenght > 245)
    errorMessages.push('The Task must have a valid description, between 1 and 245 characters.');

  if (!isValidDate) errorMessages.push('The task must have a valid Date');
  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};

/**
 * Validate that the Note has all the fields necessaries with the right format.
 *
 * @param {object} note - the note to be validated
 */
export const createNoteValidator = (note) => {
  let errorMessages = [];

  if (!isValidString(note.title)) errorMessages.push('The Note must have a valid title.');

  if (!isValidString(note.text) || note.text.lenght > 245)
    errorMessages.push('The Note text must be valid, between 1 and 245 characters.');

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};
