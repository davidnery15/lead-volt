import React from 'react';
import { Dropdown } from '@8base/boost';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { DropdownBodyOnTable, FilterFooter, FilterCategory } from 'shared/components';
import filterIconGrey from '../../../../shared/assets/images/filter-grey.svg';
import filterIconGreen from '../../../../shared/assets/images/filter-green.svg';

const FilterDropdown = styled(Dropdown)`
  display: inline-block;
`;

const DropdownBody = styled.div`
  background-color: #fff;
  position: relative;
  z-index: 2000;
  border: solid 1px #d0d7dd;
  border-radius: 5px;
`;

const FilterBody = styled.div`
  display: grid;
  grid-template-rows: repeat(3, min-content);
  grid-template-columns: repeat(2, [col-start] 1fr [col-end]);
  gap: 12px;
  padding: 12px;
`;

const FilterBtn = styled.div`
  display: flex;
  border-radius: 5px;
  border: solid 1px ${({ active }) => (active ? '#3db4aa' : '#d0d7dd')};
  padding: 0px 16px;
  height: 36px;
  box-sizing: border-box;
  align-items: center;
`;

const FilterIconLabel = styled.span`
  margin-left: 4px;
  font-size: 13px;
  opacity: 0.9;
  ${(props) => (props.active ? 'color: #3db4aa;' : '')}
`;

const FilterIcon = styled.img`
  width: 2rem;
  height: 2rem;
`;

const LeadFilter = (props) => {
  const categories = Object.entries(props.categories).map(([identifier, category]) => (
    <FilterCategory
      key={identifier}
      value={props.values[identifier]}
      onFilter={props.onFilter}
      category={category}
    />
  ));
  const icon = props.active ? (
    <FilterIcon src={filterIconGreen} />
  ) : (
    <FilterIcon src={filterIconGrey} />
  );

  return (
    <FilterDropdown defaultOpen={false}>
      <Dropdown.Head>
        <FilterBtn active={props.active} onClick={() => props.onFilterClick()}>
          {icon}
          <FilterIconLabel active={props.active}>Filters</FilterIconLabel>
        </FilterBtn>
      </Dropdown.Head>
      <DropdownBodyOnTable closeOnClickOutside={false} offset="sm" pin="left">
        {({ closeDropdown }) => (
          <DropdownBody>
            <FilterBody>{categories}</FilterBody>
            <FilterFooter
              onConfirm={props.onConfirm}
              onCancel={props.onCancel}
              closeDropdown={closeDropdown}
            />
          </DropdownBody>
        )}
      </DropdownBodyOnTable>
    </FilterDropdown>
  );
};

LeadFilter.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onFilterClick: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
  categories: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  onFilter: PropTypes.func.isRequired,
};

export default LeadFilter;
