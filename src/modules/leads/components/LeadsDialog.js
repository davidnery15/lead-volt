import React from 'react';
import styled from 'styled-components';

import { PropTypes } from 'prop-types';

import { YesNoDialog } from '../../../shared/components';

const BoldText = styled.span`
  font-weight: bold;
`;

const LeadsDialog = ({ isOpen, onYes, onClose, selectedLead, selectedLeads }) => {
  const text = (
    <span>
      <BoldText>
        {`Are you sure you want to delete ${
          selectedLead.length > 0 || selectedLeads.length === 1 ? 'this Lead?' : 'these Leads?'
        } `}
      </BoldText>
      <span>This action can’t be undone.</span>
    </span>
  );
  return (
    <YesNoDialog
      isOpen={isOpen}
      onYes={onYes}
      onNo={onYes}
      onClose={onClose}
      yesText="Yes, Delete"
      noText="Cancel"
      title={`Delete ${selectedLead.length > 0 || selectedLeads.length === 1 ? 'Lead' : 'Leads'}`}
      text={text}
    />
  );
};

LeadsDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onYes: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  selectedLead: PropTypes.string.isRequired,
  selectedLeads: PropTypes.array.isRequired,
};

export default React.memo(LeadsDialog);
