import React from 'react';
import { Table } from '@8base/boost';
import styled from 'styled-components';
import { agentsMap, priorityMap, findPriority, canAssignAgents } from '../leads-utils';
import { updateLeadAgents, updateLead } from '../leads-actions';
import {
  DropdownWithCheck,
  TextContact,
  DateBox,
  SelectInput,
  SimpleDropdown,
} from 'shared/components';
import { getAssignedAgents } from '../../agents/agents-utils';

import { STATUS_OPTIONS } from '../leads-model';
import { PRIORITY_OPTIONS } from '../../../shared/constants';
import { PropTypes } from 'prop-types';

const TableBodyCell = styled(Table.BodyCell)`
  word-break: break-word !important;
`;

const TextContactContainer = styled.div`
  & span p {
    text-align: start !important;
  }
`;

const LeadCell = ({ columnName, lead, agents, user, agency }) => {
  let cell;
  switch (columnName) {
  case 'ID':
    cell = (
      <TableBodyCell>
        <p>ID</p>
      </TableBodyCell>
    );
    break;
  case 'Received':
    cell = (
      <TableBodyCell>
        <DateBox date={lead.createdAt} />
      </TableBodyCell>
    );
    break;
  case 'Contact': {
    let contact;
    if (lead.contacts.items.length > 0) {
      contact = (
        <TextContactContainer>
          <TextContact>
            <p>{`${lead.contacts.items[0].firstName} ${lead.contacts.items[0].lastName}`}</p>
            <p>{lead.contacts.items[0].phoneNumber}</p>
          </TextContact>
        </TextContactContainer>
      );
    }
    cell = <TableBodyCell>{contact}</TableBodyCell>;
    break;
  }
  case 'Status':
    cell = (
      <TableBodyCell>
        <SelectInput
          onChange={(value) => {
            updateLead(lead.id, { status: value });
          }}
          value={lead.status}
          options={STATUS_OPTIONS}
          placeholder={''}
        />
      </TableBodyCell>
    );
    break;
  case 'IFS Score':
    cell = (
      <TableBodyCell>
        <p>{lead.ifsScore}</p>
      </TableBodyCell>
    );
    break;
  case 'Assigned To':
    if (agents) {
      if (canAssignAgents(user, agency)) {
        cell = (
          <TableBodyCell>
            <DropdownWithCheck
              itemList={agentsMap(lead.agents.items)}
              header={getAssignedAgents(lead.agents.items)}
              onConfirm={(agents) => {
                updateLeadAgents(lead.id, agents);
              }}
            />
          </TableBodyCell>
        );
      } else {
        cell = (
          <TableBodyCell>
            <SimpleDropdown
              itemList={agentsMap(lead.agents.items)}
              header={getAssignedAgents(lead.agents.items)}
            />
          </TableBodyCell>
        );
      }
    } else {
      cell = <TableBodyCell></TableBodyCell>;
    }
    break;
  case 'Campaign':
    cell = (
      <TableBodyCell>
        <p>{lead.campaign ? lead.campaign.name : ''}</p>
      </TableBodyCell>
    );
    break;
  case 'Last Action':
    cell = (
      <TableBodyCell>
        <DateBox date={lead.updatedAt} />
      </TableBodyCell>
    );
    break;
  case 'Priority':
    cell = (
      <TableBodyCell>
        <SelectInput
          onChange={(value) => {
            updateLead(lead.id, { priority: findPriority(value) });
          }}
          value={findPriority(lead.priority)}
          options={priorityMap(PRIORITY_OPTIONS)}
          placeholder={''}
        />
      </TableBodyCell>
    );
    break;
  default:
    cell = (
      <TableBodyCell>
        <p>default</p>
      </TableBodyCell>
    );
  }
  return cell;
};

LeadCell.PropTypes = {
  agents: PropTypes.array.isRequired,
  lead: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  agency: PropTypes.object.isRequired,
  columnName: PropTypes.string.isRequired,
};

export default LeadCell;
