import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { DialogForm } from 'shared/components';
import * as R from 'ramda';
import { useSubscription } from '@cobuildlab/react-flux-state';
import { onErrorMixinFC } from '../../../shared/mixins';
import { fetchAgents } from '../../agents/agents-actions';
import agentsStore, { AGENTS_LIST_EVENT, AGENTS_ERROR_EVENT } from '../../agents/agents-store';
import { createLead, fetchLeads } from '../leads-actions';
import leadStore, { LEADS_CREATE_EVENT, LEADS_ERROR_EVENT } from '../leads-store';
import LeadForm from './LeadForm';
import { LEAD } from '../leads-model';

const LeadFormDialog = ({ isOpen, onCloseDialog, afterCreate }) => {
  const [fields, setFields] = useState(R.clone(LEAD));
  const [agents, setAgents] = useState([]);
  const [loading, setLoading] = useState(false);
  const handleOnSubmit = () => {
    const {
      firstName,
      middleName,
      lastName,
      gender,
      birthMonth,
      birthDay,
      birthYear,
      email,
      agents,
      phone,
      secondPhone,
      extraPhone,
      firstAddress,
      secondAddress,
      zipCode,
      city,
      state,
      priority,
      leadType,
      creditScore,
    } = fields;
    const dataSecondPhone = `${secondPhone !== '' ? secondPhone : ''}`;
    const dataExtraPhone = `${extraPhone !== '' ? extraPhone : ''}`;
    const dataLead = {
      firstName,
      middleName,
      lastName,
      gender,
      birthMonth,
      birthDay,
      birthYear,
      email,
      agents: agents.map((agent) => agent.value),
      phone: {
        number: phone,
      },
      phones: `${dataSecondPhone}${extraPhone !== '' ? ', ' : ''}${dataExtraPhone}`,
      address: {
        street1: firstAddress,
        street2: secondAddress,
        zip: zipCode,
        city,
        state,
      },
      type: leadType,
      creditScore,
      priority,
    };

    setLoading(true);
    createLead(dataLead);
  };
  const handleOnChangeInput = (value, name) => {
    const newLead = R.clone(fields);
    newLead[name] = value;

    setFields(newLead);
  };

  useEffect(() => {
    fetchAgents();
  }, []);
  useSubscription(leadStore, LEADS_CREATE_EVENT, () => {
    setLoading(false);
    setFields(R.clone(LEAD)); // set default values.
    fetchLeads();
    onCloseDialog();
    afterCreate(); // after create function from parent component.
  });
  useSubscription(agentsStore, AGENTS_LIST_EVENT, (data) => {
    setAgents(data.agentsList.items);
  });
  useSubscription(leadStore, LEADS_ERROR_EVENT, (e) => {
    onErrorMixinFC(e);
    setLoading(false);
  });
  useSubscription(agentsStore, AGENTS_ERROR_EVENT, onErrorMixinFC);

  return (
    <DialogForm
      title="Add Leads"
      isOpen={isOpen}
      onCloseDialog={onCloseDialog}
      onClickCreate={handleOnSubmit}
      isLoading={loading}>
      <LeadForm lead={fields} agents={agents} handleOnChangeInput={handleOnChangeInput} />
    </DialogForm>
  );
};

LeadFormDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  afterCreate: PropTypes.func.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
};

export default LeadFormDialog;
