import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Grid, InputField, SelectField, Label } from '@8base/boost';
import styled from 'styled-components';
import {
  GENDER_OPTIONS,
  BIRTHMONTH_OPTIONS,
  BIRTHDAY_OPTIONS,
  BIRTHYEAR_OPTIONS,
  STATES_OPTIONS,
  LEAD_TYPE_OPTIONS,
} from '../leads-model';
import { Margin, SelectMultipleAgents } from 'shared/components';
import { PRIORITY_OPTIONS } from '../../../shared/constants';

const GridBoxFlexEnd = styled(Grid.Box)`
  justify-content: flex-end;
`;

const BtnAddPhone = styled.button`
  margin-top: 27px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: transparent;
  outline: none;
  cursor: pointer;
`;

const BtnDeletePhone = styled.button`
  margin-top: 28px;
  height: 20px;
  width: 20px;
  border: 1px solid #eb4235;
  border-radius: 100px;
  background: transparent;
  outline: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const BtnLess = styled.p`
  margin-left: 2px;
  height: 20px;
  width: 20px;
  color: #eb4235;
  font-size: 18px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const BtnPlus = styled.p`
  box-sizing: border-box;
  height: 16px;
  width: 16px;
  border: 0.67px solid #3db4aa;
  border-radius: 100px;
  color: #3db4aa;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const BtnText = styled.p`
  color: #3db4aa;
  font-family: Poppins;
  font-size: 12px;
  letter-spacing: 0.18px;
  line-height: 20px;
`;

const LeadForm = ({ lead, agents, handleOnChangeInput }) => {
  const [showPhone2, setShowPhone2] = useState(false);
  const [showPhone3, setShowPhone3] = useState(false);
  const handleAddInputPhone = (e) => {
    e.preventDefault();
    if (showPhone2 === false) {
      setShowPhone2(true);
    } else {
      setShowPhone3(true);
    }
  };
  const handleDeleteInputPhone2 = (e) => {
    e.preventDefault();
    setShowPhone2(false);
  };
  const handleDeleteInputPhone3 = (e) => {
    e.preventDefault();
    setShowPhone3(false);
  };

  return (
    <Form>
      <Form.Section>
        <Form.SectionBody>
          <Grid.Layout inline gap="md" columns="33.33% 33.33% calc(33.33% - 32px)">
            <Grid.Box>
              <InputField
                stretch
                label="First name"
                placeholder="First name"
                value={lead.firstName}
                input={{
                  type: 'text',
                  name: 'firstName',
                  onChange: (value) => handleOnChangeInput(value, 'firstName'),
                }}
              />
            </Grid.Box>
            <Grid.Box>
              <InputField
                stretch={true}
                label="Middle name"
                placeholder="Middle name"
                value={lead.middleName}
                input={{
                  type: 'text',
                  name: 'middleName',
                  onChange: (value) => handleOnChangeInput(value, 'middleName'),
                }}
              />
            </Grid.Box>
            <Grid.Box>
              <InputField
                stretch={true}
                label="Last name"
                placeholder="Last name"
                value={lead.lastName}
                input={{
                  type: 'text',
                  name: 'lastName',
                  onChange: (value) => handleOnChangeInput(value, 'lastName'),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="33.33% calc(66.66% - 32px)">
            <Grid.Box>
              <SelectField
                stretch={true}
                label="Gender"
                placeholder="Gender"
                input={{
                  name: 'gender',
                  value: lead.gender,
                  onChange: (value) => handleOnChangeInput(value, 'gender'),
                }}
                options={GENDER_OPTIONS}
              />
            </Grid.Box>
            <Grid.Box>
              <Grid.Layout inline gap="md" columns="38% 30% calc(32% - 16px)">
                <Grid.Box>
                  <SelectField
                    stretch={true}
                    label="Date of Birth"
                    placeholder="Month"
                    input={{
                      name: 'birthMonth',
                      value: lead.birthMonth,
                      onChange: (value) => handleOnChangeInput(value, 'birthMonth'),
                    }}
                    options={BIRTHMONTH_OPTIONS}
                  />
                </Grid.Box>
                <GridBoxFlexEnd>
                  <SelectField
                    stretch={true}
                    placeholder="Day"
                    input={{
                      name: 'birthDay',
                      value: lead.birthDay,
                      onChange: (value) => handleOnChangeInput(value, 'birthDay'),
                    }}
                    options={BIRTHDAY_OPTIONS}
                  />
                </GridBoxFlexEnd>
                <GridBoxFlexEnd>
                  <SelectField
                    stretch={true}
                    placeholder="Year"
                    input={{
                      name: 'birthYear',
                      value: lead.birthYear,
                      onChange: (value) => handleOnChangeInput(value, 'birthYear'),
                    }}
                    options={BIRTHYEAR_OPTIONS}
                  />
                </GridBoxFlexEnd>
              </Grid.Layout>
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="100%">
            <Grid.Box>
              <InputField
                stretch={true}
                label="Email Address"
                placeholder="Email"
                value={lead.email}
                input={{
                  name: 'email',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'email'),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="100%">
            <Grid.Box>
              <Margin bottom="4px">
                <Label kind="secondary" text="Assign Agents" />
              </Margin>
              <SelectMultipleAgents
                name="agents"
                placeholder="Assign agents"
                onChange={(value) => handleOnChangeInput(value, 'agents')}
                options={agents.map((agent) => ({
                  label: `${agent.user.firstName} ${agent.user.lastName}`,
                  value: agent.id,
                  imgUrl: agent.user.avatar
                    ? agent.user.avatar.previewUrl
                    : 'https://randomuser.me/api/portraits/women/17.jpg',
                }))}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="48% 32%">
            <Grid.Box>
              <InputField
                stretch={true}
                label="Primary Phone Number"
                placeholder="Phone number"
                value={lead.phone}
                input={{
                  name: 'phone',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'phone'),
                }}
              />
            </Grid.Box>
            <Grid.Box>
              <BtnAddPhone onClick={handleAddInputPhone}>
                <BtnPlus>+</BtnPlus>
                <BtnText>Add New Phone Number</BtnText>
              </BtnAddPhone>
            </Grid.Box>
          </Grid.Layout>
          {showPhone2 ? (
            <Grid.Layout inline gap="md" columns="48% 5%">
              <Grid.Box>
                <InputField
                  stretch={true}
                  label="Secondary Phone Number"
                  placeholder="Phone number"
                  value={lead.secondPhone}
                  input={{
                    name: 'secondPhone',
                    type: 'text',
                    onChange: (value) => handleOnChangeInput(value, 'secondPhone'),
                  }}
                />
              </Grid.Box>
              <Grid.Box>
                <BtnDeletePhone onClick={handleDeleteInputPhone2}>
                  <BtnLess>-</BtnLess>
                </BtnDeletePhone>
              </Grid.Box>
            </Grid.Layout>
          ) : null}
          {showPhone3 ? (
            <Grid.Layout inline gap="md" columns="48% 5%">
              <Grid.Box>
                <InputField
                  stretch={true}
                  label="Extra Phone Number"
                  placeholder="Phone number"
                  value={lead.extraPhone}
                  input={{
                    name: 'extraPhone',
                    type: 'text',
                    onChange: (value) => handleOnChangeInput(value, 'extraPhone'),
                  }}
                />
              </Grid.Box>
              <Grid.Box>
                <BtnDeletePhone onClick={handleDeleteInputPhone3}>
                  <BtnLess>-</BtnLess>
                </BtnDeletePhone>
              </Grid.Box>
            </Grid.Layout>
          ) : null}
          <Grid.Layout inline gap="md" columns="48% 48%">
            <Grid.Box>
              <InputField
                stretch={true}
                label="Address Line 1"
                placeholder="Address Line"
                value={lead.firstAddress}
                input={{
                  name: 'fistAddress',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'firstAddress'),
                }}
              />
            </Grid.Box>
            <Grid.Box>
              <InputField
                stretch={true}
                label="Address Line 2"
                placeholder="Address Line"
                value={lead.secondAddress}
                input={{
                  name: 'secondAddress',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'secondAddress'),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="48% 48%">
            <Grid.Box>
              <InputField
                stretch
                label="City"
                placeholder="City"
                value={lead.city}
                input={{
                  name: 'city',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'city'),
                }}
              />
            </Grid.Box>
            <Grid.Box>
              <InputField
                stretch
                label="ZIP"
                placeholder="ZIP"
                value={lead.zipCode}
                input={{
                  name: 'zipCode',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'zipCode'),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="48%">
            <Grid.Box>
              <SelectField
                stretch={true}
                label="State"
                placeholder="State"
                input={{
                  name: 'state',
                  value: lead.state,
                  onChange: (value) => handleOnChangeInput(value, 'state'),
                }}
                options={STATES_OPTIONS}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="48% 48%">
            <Grid.Box>
              <SelectField
                stretch={true}
                label="Lead Type"
                placeholder="Lead Type"
                input={{
                  name: 'leadType',
                  value: lead.leadType,
                  onChange: (value) => handleOnChangeInput(value, 'leadType'),
                }}
                options={LEAD_TYPE_OPTIONS}
              />
            </Grid.Box>
            <Grid.Box>
              <SelectField
                stretch={true}
                label="Priority"
                placeholder="Priority"
                input={{
                  name: 'priority',
                  value: lead.priority,
                  onChange: (value) => handleOnChangeInput(value, 'priority'),
                }}
                options={PRIORITY_OPTIONS}
              />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout inline gap="md" columns="48%">
            <Grid.Box>
              <InputField
                stretch
                label="Credit Score"
                placeholder="Credit Score"
                value={lead.creditScore}
                input={{
                  name: 'creditScore',
                  type: 'text',
                  onChange: (value) => handleOnChangeInput(value, 'creditScore'),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
        </Form.SectionBody>
      </Form.Section>
    </Form>
  );
};

LeadForm.propTypes = {
  lead: PropTypes.object.isRequired,
  agents: PropTypes.array.isRequired,
  handleOnChangeInput: PropTypes.func.isRequired,
};

export default LeadForm;
