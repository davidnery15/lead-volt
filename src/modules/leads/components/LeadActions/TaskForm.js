import React, { useState } from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import moment from 'moment';
import { SelectField, TextAreaField, InputField, DateInputField, Label } from '@8base/boost';
import { DialogForm, SelectMultipleAgents, Margin } from '../../../../shared/components';

import { findPriority, priorityMap } from '../../leads-utils';
import { PRIORITY_OPTIONS } from '../../../../shared/constants';
import { createTask } from '../../leads-actions';
import { typesMap } from '../../leads-utils';

const TaskContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 12px;
  column-gap: 15px;
  align-items: flex-end;
  grid-template-areas:
    'title title title title'
    'type date date date'
    'assignedTo assignedTo assignedTo assignedTo'
    'taskPriority taskPriority taskPriority taskPriority'
    'description description description description';
`;

const Title = styled(InputField)`
  grid-area: title;
`;

const DateBox = styled.div`
  grid-area: date;
`;

const AssignedTo = styled.div`
  grid-area: assignedTo;
`;
const TaskPriority = styled.div`
  grid-area: taskPriority;
`;
const Description = styled.div`
  grid-area: description;
  & textarea {
    height: 85px;
  }
`;
const TaskForm = ({ onClose, isOpen, agents, leadId, taskTypes }) => {
  // eslint-disable-next-line
  const [title, setTitle] = useState('');
  const [dataAgents, setdataAgents] = useState(undefined);
  const [date, setDate] = useState(undefined);
  const [priority, setPriority] = useState('');
  const [description, setDescription] = useState('');
  const [loading, setLoading] = useState(false);
  const [type, setType] = useState('');

  const taskCreateHandler = async () => {
    setLoading(true);
    const data = {
      date: date ? moment(date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
      description,
      title,
      lead: leadId,
      priority,
      agents: dataAgents ? dataAgents.map((a) => a.value) : [],
      type: type.id,
    };
    const result = await createTask(data);
    setLoading(false);
    if (result) {
      setTitle('');
      setdataAgents('');
      setDate('');
      setPriority('');
      setDescription('');
      setType('');
      onClose();
    }
  };

  return (
    <DialogForm
      createText={'Add Task'}
      isOpen={isOpen}
      title={'Add Task'}
      onClickCreate={taskCreateHandler}
      isLoading={loading}
      onCloseDialog={onClose}>
      <TaskContainer>
        <Title
          stretch={true}
          label="Title"
          placeholder=""
          input={{
            value: title,
            name: 'taskTitle',
            type: 'text',
            onChange: (value) => setTitle(value),
          }}
        />
        <SelectField
          label="Type"
          stretch
          input={{
            value: type,
            name: 'taskType',
            onChange: (selectedType) => setType(selectedType),
          }}
          options={typesMap(taskTypes)}
          placeholder={''}
        />
        <DateBox>
          <DateInputField
            label="Date"
            input={{
              value: date,
              name: 'date',
              onChange: (value) => setDate(value),
            }}
          />
        </DateBox>
        <AssignedTo>
          <Margin bottom="4px">
            <Label kind="secondary" text="Assign Agents" />
          </Margin>
          <SelectMultipleAgents
            name="agents"
            placeholder="Assign agents"
            onChange={(value) => setdataAgents(value)}
            options={agents.map((agent) => ({
              label: `${agent.user.firstName} ${agent.user.lastName}`,
              value: agent.id,
              imgUrl: agent.user.avatar
                ? agent.user.avatar.previewUrl
                : 'https://randomuser.me/api/portraits/women/17.jpg',
            }))}
          />
        </AssignedTo>
        <TaskPriority>
          <SelectField
            label="Priority"
            stretch
            input={{
              value: findPriority(priority, PRIORITY_OPTIONS),
              name: 'priority',
              onChange: (value) => setPriority(findPriority(value, PRIORITY_OPTIONS)),
            }}
            options={priorityMap(PRIORITY_OPTIONS)}
            placeholder={''}
          />
        </TaskPriority>
        <Description>
          <TextAreaField
            stretch
            label="Description"
            input={{
              value: description,
              name: 'description',
              onChange: (value) => setDescription(value),
            }}
          />
        </Description>
      </TaskContainer>
    </DialogForm>
  );
};

TaskForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  leadId: PropTypes.string.isRequired,
  taskTypes: PropTypes.array.isRequired,
  agents: PropTypes.array.isRequired,
};

export default TaskForm;
