import React, { useState } from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { useHistory } from 'react-router-dom';

import { Menu, Icon, Dropdown, Table } from '@8base/boost';
import { DropdownBodyOnTable } from '../../../../shared/components';

import informationIcon from '../../../../shared/assets/images/information.svg';
import callIcon from '../../../../shared/assets/images/call-green.svg';

import MsgForm from './MsgForm';
import TaskForm from './TaskForm';
import NoteForm from './NoteForm';

const ActionsCell = styled(Table.BodyCell)`
  display: flex;
  justify-content: space-around !important;
`;

const CustomIcon = styled.img`
  width: 23px;
  height: 23px;
  cursor: pointer;
`;

const MenuItem = styled(Menu.Item)`
  color: ${({ danger }) => (danger ? '#FF0000' : 'rgb(50, 60, 71)')}!important;
  &:hover {
    ${({ danger }) => (danger ? '' : 'color: #3db4aa !important;')}
  }
`;

const LeadAction = ({ lead, onDelete, agents, taskTypes, canDelete }) => {
  const [isNoteModalOpen, setIsNoteModalOpen] = useState(false);
  const [isTaskModalOpen, setIsTaskModalOpen] = useState(false);
  const [isMsgModalOpen, setIsMsgModalOpen] = useState(false);
  const history = useHistory();

  let taskForm;
  if (agents && taskTypes) {
    taskForm = (
      <TaskForm
        leadId={lead.id}
        taskTypes={taskTypes}
        agents={agents}
        isOpen={isTaskModalOpen}
        onClose={() => {
          setIsTaskModalOpen(false);
        }}
      />
    );
  }

  let dropdown = (
    <Dropdown defaultOpen={false}>
      <Dropdown.Head>
        <Icon name="More" color="GRAY_40" />
      </Dropdown.Head>
      <DropdownBodyOnTable>
        {({ closeDropdown }) => (
          <Menu>
            <MenuItem
              onClick={() => {
                setIsMsgModalOpen((prev) => !prev);
                closeDropdown();
              }}>
              Send Message
            </MenuItem>
            <MenuItem
              onClick={() => {
                setIsNoteModalOpen((prev) => !prev);
                closeDropdown();
              }}>
              Add Note
            </MenuItem>
            <MenuItem
              onClick={() => {
                setIsTaskModalOpen((prev) => !prev);
                closeDropdown();
              }}>
              Add Task
            </MenuItem>
          </Menu>
        )}
      </DropdownBodyOnTable>
    </Dropdown>
  );
  if (canDelete) {
    dropdown = (
      <Dropdown defaultOpen={false}>
        <Dropdown.Head>
          <Icon name="More" color="GRAY_40" />
        </Dropdown.Head>
        <DropdownBodyOnTable>
          {({ closeDropdown }) => (
            <Menu>
              <MenuItem
                onClick={() => {
                  setIsMsgModalOpen((prev) => !prev);
                  closeDropdown();
                }}>
                Send Message
              </MenuItem>
              <MenuItem
                onClick={() => {
                  setIsNoteModalOpen((prev) => !prev);
                  closeDropdown();
                }}>
                Add Note
              </MenuItem>
              <MenuItem
                onClick={() => {
                  setIsTaskModalOpen((prev) => !prev);
                  closeDropdown();
                }}>
                Add Task
              </MenuItem>
              <MenuItem
                danger
                onClick={() => {
                  onDelete();
                  closeDropdown();
                }}>
                Delete Lead
              </MenuItem>
            </Menu>
          )}
        </DropdownBodyOnTable>
      </Dropdown>
    );
  }

  return (
    <>
      <ActionsCell>
        <CustomIcon src={callIcon} alt="call icon" />
        <CustomIcon
          src={informationIcon}
          onClick={() => history.push(`/leads/${lead.id}`)}
          alt="lead detail icon"
        />
        {dropdown}
        <MsgForm
          lead={lead}
          isOpen={isMsgModalOpen}
          onClose={() => {
            setIsMsgModalOpen(false);
          }}
        />
        {taskForm}
        <NoteForm
          leadId={lead.id}
          isOpen={isNoteModalOpen}
          onClose={() => {
            setIsNoteModalOpen(false);
          }}></NoteForm>
      </ActionsCell>
    </>
  );
};

LeadAction.propTypes = {
  onDelete: PropTypes.func.isRequired,
  lead: PropTypes.object.isRequired,
  taskTypes: PropTypes.array.isRequired,
  agents: PropTypes.array.isRequired,
  canDelete: PropTypes.bool.isRequired,
};

export default LeadAction;
