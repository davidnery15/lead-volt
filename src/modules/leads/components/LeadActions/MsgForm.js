import React, { useState } from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { SelectField, TextAreaField } from '@8base/boost';
import { DialogForm } from '../../../../shared/components';
import { msgNumbersMap } from '../../leads-utils';

const MsgContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  row-gap: 12px;
`;
const MsgNumber = styled.div`
  grid-column: 1/-1;
`;
const MsgBody = styled.div`
  grid-column: 1/-1;
  & textarea {
    height: 85px;
  }
`;

const MsgForm = ({ onClose, isOpen, lead }) => {
  // eslint-disable-next-line
  const [msg, setMsg] = useState('');
  return (
    <DialogForm
      createText={'Send Text'}
      isOpen={isOpen}
      title={'Text Message'}
      onClickCreate={() => {}}
      onCloseDialog={onClose}>
      <MsgContainer>
        <MsgNumber>
          <SelectField
            stretch
            label="Number"
            input={{
              name: 'contactNumber',
              value: '',
              onChange: (value) => {},
            }}
            options={msgNumbersMap(lead)}
          />
        </MsgNumber>
        <MsgBody>
          <TextAreaField
            stretch
            label="Content"
            input={{ name: 'Content', onChange: () => null }}
          />
        </MsgBody>
        {/* <Button color="neutral">
          <Icon name="PaperClip" />
          Attach a file
        </Button> */}
      </MsgContainer>
    </DialogForm>
  );
};

MsgForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  lead: PropTypes.object.isRequired,
};

export default MsgForm;
