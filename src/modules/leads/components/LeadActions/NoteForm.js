import React, { useState } from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { TextAreaField, InputField } from '@8base/boost';
import { DialogForm } from '../../../../shared/components';
import { createNote } from '../../leads-actions';

const NoteContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 12px;
  column-gap: 15px;
`;

const TextAreaContainer = styled.div`
  & textarea {
    height: 85px;
  }
`;

const Noteform = ({ isOpen, onClose, leadId }) => {
  const [title, setTitle] = useState('');
  const [note, setNote] = useState('');
  const [loading, setLoading] = useState(false);

  const noteCreateHandler = async (value) => {
    const data = {
      text: note,
      title,
      notesLeadRelation: leadId,
    };
    setLoading(true);

    const result = await createNote(data);
    setLoading(false);
    if (result) {
      onClose();
      setTitle('');
      setNote('');
    }
  };

  return (
    <DialogForm
      isOpen={isOpen}
      onClickCreate={noteCreateHandler}
      isLoading={loading}
      onCloseDialog={onClose}
      createText={'Add Note'}
      title={'Add Note'}>
      <NoteContainer>
        <InputField
          stretch={true}
          label="Title"
          placeholder=""
          input={{
            value: title,
            name: 'title',
            type: 'text',
            onChange: (value) => setTitle(value),
          }}
        />
        <TextAreaContainer>
          <TextAreaField
            stretch
            label="Note Text"
            input={{
              value: note,
              name: 'note',
              onChange: (value) => setNote(value),
            }}
          />
        </TextAreaContainer>

        <InputField
          stretch={true}
          label="Tags"
          placeholder="Add tags here"
          value={''}
          input={{
            name: 'tags',
            type: 'text',
          }}
        />
      </NoteContainer>
    </DialogForm>
  );
};

Noteform.propTypes = {
  leadId: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default Noteform;
