import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { DropdownWithCheck, SimpleDropdown } from '../../../../shared/components';
import { updateLeadAgents } from '../../leads-actions';
import { getAssignedAgents } from '../../../agents/agents-utils';
import { agentsMap } from '../../leads-utils';
import moment from 'moment';

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 15px;
`;
const BoldedText = styled.span`
  font-weight: 600;
`;

const AgentsBox = styled.span`
  margin-right: 30px;
`;

const LeadInformation = ({ lead, agents, canAssingAgents }) => {
  let agentsCell;
  if (agents) {
    if (canAssingAgents) {
      agentsCell = (
        <DropdownWithCheck
          itemList={agentsMap(lead.agents.items)}
          header={getAssignedAgents(lead.agents.items)}
          onConfirm={(agents) => {
            updateLeadAgents(lead.id, agents);
          }}
        />
      );
    } else {
      agentsCell = (
        <SimpleDropdown
          itemList={agentsMap(lead.agents.items)}
          header={getAssignedAgents(lead.agents.items)}
        />
      );
    }
  }

  return (
    <Container>
      <span>
        <BoldedText>Created: </BoldedText>
        {moment(lead.createdAt).format('MM-DD-YYYY - LT')}
      </span>
      <AgentsBox>{agentsCell}</AgentsBox>

      <span>
        <BoldedText>Last Action: </BoldedText>
        {moment(lead.updatedAt).format('MM-DD-YYYY - LT')}
      </span>
    </Container>
  );
};

LeadInformation.defaultProps = {
  agents: null,
};

LeadInformation.propTypes = {
  lead: PropTypes.object.isRequired,
  canAssingAgents: PropTypes.bool.isRequired,
  agents: PropTypes.array,
};

export default LeadInformation;
