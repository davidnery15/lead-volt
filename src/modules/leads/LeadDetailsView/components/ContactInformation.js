import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { Paper } from '@8base/boost';
import { Title, PaperHeader, ButtonWithoutIcon } from 'shared/components';
import { formatContactData } from '../lead-details-utils';

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const PaperBody = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  padding: 20px;
  gap: 15px;
`;

const Cell = styled.div`
  display: flex;
  flex-direction: column;
  & strong {
    font-weight: 600;
  }
`;

// const RowTitle = styled.div`
//   font-size: 12px;
//   font-weight: 600;
//   color: #7d828c;
//   text-transform: uppercase;
//   letter-spacing: 0.5px;
//   border-bottom: 1px solid #eaf0f5;
//   background-color: #f4f5f6;
//   padding: 10px 20px;
// `;

const ContactInformation = ({ lead }) => {
  const cells = formatContactData(lead).map((cell) => (
    <Cell key={cell.label}>
      <strong>{cell.label}</strong>
      <span>{cell.value}</span>
    </Cell>
  ));

  return (
    <Paper>
      <PaperHeader>
        <HeaderContainer>
          <Title>Contact Information</Title>
          <ButtonWithoutIcon text="Edit Lead Info" />
        </HeaderContainer>
      </PaperHeader>
      <PaperBody>{cells}</PaperBody>
    </Paper>
  );
};

ContactInformation.propTypes = {
  lead: PropTypes.object.isRequired,
};

export default ContactInformation;
