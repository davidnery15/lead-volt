import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import {} from '@8base/boost';
import { TextContact, PriorityBadge, OptionsDropdown } from '../../../../shared/components';
import moment from 'moment';

const MiniCardContainer = styled.div`
  padding: 15px 20px;
`;
const MiniCardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const MiniCardTitle = styled.span`
  & span {
    font-weight: 600 !important;
  }
`;

const MiniCardFooter = styled.div`
  display: flex;
  margin-top: 20px;
`;

const TypeBox = styled.div`
  margin-right: auto;
  color: #9b9b9b;
  border-radius: 5px;
  padding: 2px 10px;
  font-size: 11px;
  background-color: #f4f5f6;
  display: flex;
  align-items: center;
`;

const MiniCard = ({ data, options, canSeeOptions }) => {
  const { date, title, priority, description, type } = data;
  const formatedDate = moment(date).format('MM/DD/YYYY');

  let typeBox;
  if (type) {
    typeBox = <TypeBox>{type.name}</TypeBox>;
  }

  let priorityBadge;
  if (priority) {
    priorityBadge = <PriorityBadge priority={priority} />;
  }

  let optionsDropdown;
  if (canSeeOptions) {
    optionsDropdown = <OptionsDropdown options={options} />;
  }

  return (
    <MiniCardContainer>
      <MiniCardHeader>
        <MiniCardTitle>
          <TextContact>{`${title} - ${formatedDate}`}</TextContact>
        </MiniCardTitle>
        {optionsDropdown}
      </MiniCardHeader>
      <div>{description}</div>
      <MiniCardFooter>
        {typeBox}
        {priorityBadge}
      </MiniCardFooter>
    </MiniCardContainer>
  );
};

MiniCard.defaultProps = {
  canSeeOptions: false,
};

MiniCard.propTypes = {
  data: PropTypes.object.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
      danger: PropTypes.bool,
    }),
  ).isRequired,
  canSeeOptions: PropTypes.bool,
};

export default MiniCard;
