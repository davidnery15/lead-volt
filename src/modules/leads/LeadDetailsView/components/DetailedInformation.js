import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { Paper } from '@8base/boost';
import { Title, PaperHeader, ButtonWithoutIcon } from 'shared/components';
import { formatContactData } from '../lead-details-utils';

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  padding: 20px;
  gap: 15px;
`;

const Cell = styled.div`
  display: flex;
  flex-direction: column;
  & strong {
    font-weight: 600;
  }
`;

const PaperBody = styled.div``;

const DetailedInformation = ({ lead }) => {
  const cells = formatContactData(lead).map((cell) => (
    <Cell key={cell.label}>
      <strong>{cell.label}</strong>
      <span>{cell.value}</span>
    </Cell>
  ));

  return (
    <Paper>
      <PaperHeader>
        <HeaderContainer>
          <Title>Detailed Information</Title>
          <ButtonWithoutIcon text="Edit Information" />
        </HeaderContainer>
      </PaperHeader>
      <PaperBody>
        <div>
          <Grid>{cells}</Grid>
        </div>
      </PaperBody>
    </Paper>
  );
};

DetailedInformation.propTypes = {
  lead: PropTypes.object.isRequired,
};

export default DetailedInformation;
