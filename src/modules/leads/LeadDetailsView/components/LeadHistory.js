import React from 'react';
import styled from 'styled-components';
// import { PropTypes } from 'prop-types';
import { Paper } from '@8base/boost';
import { Title, PaperHeader, DateBox } from 'shared/components';
import { CustomIconBoost } from '../../../../shared/components/ui/Icons';

import phoneIcon from '../../../../shared/assets/images/call-green.svg';

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const PaperBody = styled.div``;

const Row = styled.div`
  display: grid;
  grid-template-columns: 0.4fr 0.5fr 0.2fr;
  gap: 5px;
  padding: 15px;
`;

const DateAndIconBox = styled.div`
  display: flex;
  align-items: center;
  & p:first-child {
    font-weight: 600;
  }
`;

const CustomIcon = styled.img`
  width: 23px;
  height: 23px;
  cursor: pointer;
  margin-right: 10px;
`;

const BoldedText = styled.span`
  font-weight: 600;
`;

const DescriptionBox = styled.div`
  display: flex;
  flex-direction: column;
`;

const OptionsIcon = styled.span`
  & i {
    height: 30px !important;
    width: 30px !important;
  }
`;

const StyledPaper = styled(Paper)`
  min-width: 337px !important;
  align-self: start;
`;

const TEST_DATA = [
  { label: 'Auto #1 Year', value: '2012' },
  { label: 'Auto #2 Annual Mileage', value: '32.421' },
  { label: 'Other Numbers', value: '(562) 421-3523' },
  { label: 'Auto #1 Annual Mileage', value: '12.542' },
];

const rows = TEST_DATA.map((_, index) => (
  <Row key={index}>
    <DateAndIconBox>
      <CustomIcon src={phoneIcon} alt="lead history icon" />
      <DateBox date={new Date()} />
    </DateAndIconBox>
    <DescriptionBox>
      <BoldedText>Outbound call</BoldedText>
      <p>Missed by Agent</p>
    </DescriptionBox>
    <OptionsIcon>
      <CustomIconBoost name="More" color="GRAY_40" size="md" />
    </OptionsIcon>
  </Row>
));

const LeadHistory = () => {
  return (
    <StyledPaper>
      <PaperHeader>
        <HeaderContainer>
          <Title>Lead History</Title>
        </HeaderContainer>
      </PaperHeader>
      <PaperBody>{rows}</PaperBody>
    </StyledPaper>
  );
};

export default LeadHistory;
