import { MANAGER } from '../../../shared/constants';
import { userHasAnyRoleInAgency } from '../../../shared/utils/agency-utils';
import { findPriority } from '../leads-utils';

/**
 * function to format contact data.
 *
 * @param {object} lead - the lead which contains the data.
 * @returns {Array<object>}
 */
export const formatContactData = (lead) => {
  const {
    gender,
    phone,
    firstName,
    lastName,
    middleName,
    type,
    birthDay,
    birthMonth,
    birthYear,
    campaign,
    address: [firstAddress],
    status,
    priority,
  } = lead;

  return [
    {
      label: 'Name',
      value:
        firstName && lastName ? `${firstName} ${middleName ? middleName : ''} ${lastName}` : '',
    },
    { label: 'Lead Type', value: type },
    {
      label: 'Date of Birth',
      value: birthMonth && birthDay && birthYear ? `${birthMonth} ${birthDay}, ${birthYear}` : '',
    },
    {
      label: 'Address',
      value: firstAddress ? `${firstAddress.street1} ${firstAddress.street2}` : '',
    },
    { label: 'Gender', value: gender },
    { label: 'City', value: firstAddress ? firstAddress.city : '' },
    { label: 'Primary Phone Number', value: phone ? `(${phone.code}) ${phone.number}` : '' },
    { label: 'ZIP Code', value: firstAddress ? firstAddress.zip : '' },
    { label: 'Status', value: status },
    { label: 'Campaign', value: campaign ? campaign.name : '' },
    { label: 'Priority', value: priority ? findPriority(priority) : '' },
  ];
};

/**
 * Returns true if the user has the role Manager in the provided agency
 *
 * @param {object} user
 * @param {object} agency
 * @returns {bool}
 */
export const canSeeTaskAndNotesOptions = (user, agency) => {
  return userHasAnyRoleInAgency(user, agency, MANAGER);
};
