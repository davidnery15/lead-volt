import React, { useState, useEffect, useCallback } from 'react';
import styled from 'styled-components';
import { useSubscription } from '@cobuildlab/react-flux-state';
import { useParams } from 'react-router-dom';
import * as R from 'ramda';
import { Paper } from '@8base/boost';
import { Loader, TitleDetail, Title, PaperHeader, PlusBtn } from 'shared/components';
import leadsStore, {
  LEAD_DETAIL_EVENT,
  LEAD_DETAIL_ERROR_EVENT,
  TASK_CREATE_EVENT,
  TASK_ERROR_EVENT,
  NOTE_ERROR_EVENT,
  NOTE_CREATE_EVENT,
  TASK_TYPE_LIST_EVENT,
} from '../leads-store';
import agentsStore, { AGENTS_LIST_EVENT } from '../../agents/agents-store';
import MiniCard from './components/MiniCard';
import TaskForm from '../components/LeadActions/TaskForm';
import NoteForm from '../components/LeadActions/NoteForm';
import { fetchLeadDetail, fetchLeadTaskTypes } from '../leads-actions';
import { fetchAgents } from '../../agents/agents-actions';
import { onErrorMixinFC } from '../../../shared/mixins';
import * as toast from 'shared/components/toast/Toast';
import LeadInformation from './components/LeadInformation';
import ContactInformation from './components/ContactInformation';
import DetailedInformation from './components/DetailedInformation';
import LeadHistory from './components/LeadHistory';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/session/session-store';
import { getActiveAgency } from '../../../shared/utils/agency-utils';
import { canSeeTaskAndNotesOptions } from './lead-details-utils';
import { canAssignAgents } from '../leads-utils';

const MainGrid = styled.div`
  display: grid;
  grid-template-columns: 293px 1fr;
`;

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

// const CalendarBtn = styled.button`
//   box-sizing: border-box;
//   height: 36px;
//   width: 50px;
//   border: 1px solid #e2e9ef;
//   border-radius: 4px;
//   background-color: #ffffff;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   cursor: pointer;
// `;

const LeftGrid = styled.div`
  padding: 24px;
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 30px;
  border-right: 1px solid #d8d8d8;
  align-items: start;
  grid-auto-rows: min-content;
`;

const RightSection = styled.div`
  padding: 24px;
  display: grid;
  grid-template-columns: 1fr;
  gap: 30px;
`;

const BottomSection = styled.div`
  display: grid;
  grid-template-columns: minmax(360px, 1fr) minmax(337px, 0.5fr);
  column-gap: 30px;
`;

const MiniCardBody = styled.div`
  max-height: 200px;
  overflow-y: scroll;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const TasksBtns = styled.div`
  display: grid;
  grid-template-columns: repeat(2, min-content);
  column-gap: 15px;
`;

const LeadDetailsView = () => {
  const [lead, setLead] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [isNoteModalOpen, setIsNoteModalOpen] = useState(false);
  const [isTaskModalOpen, setIsTaskModalOpen] = useState(false);
  // const [isMsgModalOpen, setIsMsgModalOpen] = useState(false);
  const [agents, setAgents] = useState(undefined);
  const [taskTypeList, setTaskTypesList] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const [agency, setAgecy] = useState(undefined);

  const { id: leadId } = useParams();

  useSubscription(leadsStore, LEAD_DETAIL_EVENT, (data) => {
    setLead(data.lead);
  });

  useSubscription(leadsStore, LEAD_DETAIL_ERROR_EVENT, onErrorMixinFC);

  useSubscription(agentsStore, AGENTS_LIST_EVENT, (data) => {
    setAgents(data.agentsList.items);
  });

  useSubscription(leadsStore, TASK_TYPE_LIST_EVENT, (data) => {
    setTaskTypesList(data.leadTaskTypesList.items);
  });

  useSubscription(leadsStore, TASK_CREATE_EVENT, (data) => {
    if (data.leadTaskCreate.lead.id === leadId) {
      const newLead = R.clone(lead);
      newLead.tasksLeadRelation.items.push(data.leadTaskCreate);
      setLead(newLead);
    }
    toast.success('Task Created!');
  });

  useSubscription(leadsStore, NOTE_CREATE_EVENT, (data) => {
    if (data.leadNoteCreate.notesLeadRelation.id === leadId) {
      const newLead = R.clone(lead);
      newLead.notesLeadRelation.items.push(data.leadNoteCreate);
      setLead(newLead);
    }
    toast.success('Note Created!');
  });

  useSubscription(leadsStore, TASK_ERROR_EVENT, onErrorMixinFC);
  useSubscription(leadsStore, NOTE_ERROR_EVENT, onErrorMixinFC);

  const fetchInitialStates = useCallback(async () => {
    await Promise.all([fetchLeadTaskTypes(), fetchLeadDetail(leadId), fetchAgents()]);
    setLoading(false);
  }, [leadId]);

  const agencyHandler = async () => {
    const agency = await getActiveAgency();
    setAgecy(agency);
  };

  const taskOptions = [
    {
      label: 'Edit',
      onClick: () => {},
    },
    { label: 'Delete', onClick: () => {}, danger: true },
  ];

  const notesOptions = [
    {
      label: 'Edit',
      onClick: () => {},
    },
    { label: 'Delete', onClick: () => {}, danger: true },
  ];

  useEffect(() => {
    fetchInitialStates();
    agencyHandler();
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    setUser(user);
  }, [fetchInitialStates]);

  let taskForm;
  if (agents && taskTypeList) {
    taskForm = (
      <TaskForm
        leadId={leadId}
        taskTypes={taskTypeList}
        agents={agents}
        isOpen={isTaskModalOpen}
        onClose={() => {
          setIsTaskModalOpen(false);
        }}
      />
    );
  }

  let notes;
  if (lead && lead.notesLeadRelation.items.length > 0) {
    notes = lead.notesLeadRelation.items.map((note) => {
      return (
        <MiniCard
          options={notesOptions}
          canSeeOptions={canSeeTaskAndNotesOptions(user, agency)}
          key={note.id}
          data={note}
        />
      );
    });
  }
  let tasks;
  if (lead && lead.tasksLeadRelation.items.length > 0) {
    tasks = lead.tasksLeadRelation.items.map((task) => (
      <MiniCard
        options={taskOptions}
        canSeeOptions={canSeeTaskAndNotesOptions(user, agency)}
        key={task.id}
        data={task}
      />
    ));
  }

  let leadInformation;
  if (agents) {
    leadInformation = (
      <LeadInformation
        agents={agents}
        canAssingAgents={canAssignAgents(user, agency)}
        lead={lead}
      />
    );
  }

  let content = <Loader />;
  if (!loading) {
    content = (
      <React.Fragment>
        <TitleDetail text={`George Franklin`} />
        <MainGrid>
          <LeftGrid>
            <Paper>
              <PaperHeader>
                <HeaderContainer>
                  <Title>Lead Notes</Title>
                  <PlusBtn onClick={() => setIsNoteModalOpen(true)} />
                </HeaderContainer>
              </PaperHeader>
              <MiniCardBody>{notes}</MiniCardBody>
              <NoteForm
                leadId={leadId}
                isOpen={isNoteModalOpen}
                onClose={() => {
                  setIsNoteModalOpen(false);
                }}></NoteForm>
            </Paper>
            {leadInformation}
            <Paper>
              <PaperHeader>
                <HeaderContainer>
                  <Title>Tasks</Title>
                  <TasksBtns>
                    <PlusBtn onClick={() => setIsTaskModalOpen(true)} />
                  </TasksBtns>
                </HeaderContainer>
              </PaperHeader>
              <MiniCardBody>{tasks}</MiniCardBody>
              {taskForm}
            </Paper>
          </LeftGrid>
          <RightSection>
            <DetailedInformation lead={lead} />
            <BottomSection>
              <ContactInformation lead={lead} />
              <LeadHistory />
            </BottomSection>
          </RightSection>
        </MainGrid>
      </React.Fragment>
    );
  }
  return <div>{content}</div>;
};

LeadDetailsView.propTypes = {};

export default LeadDetailsView;
