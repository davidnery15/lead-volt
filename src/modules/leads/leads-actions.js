import Flux from '@cobuildlab/flux-state';
import { error, log } from '@cobuildlab/pure-logger';

import {
  LEADS_LIST_QUERY,
  FILTER_OPTIONS_QUERY,
  LEAD_AGENTS_UPDATE_MUTATION,
  LEAD_CREATE_MUTATION,
  LEAD_UPDATE_MUTATION,
  LEAD_DELETE_MUTATION,
  TASK_CREATE_MUTATION,
  NOTE_CREATE_MUTATION,
  LEAD_TASK_TYPE_LIST_QUERY,
  LEAD_DETAIL_QUERY,
} from './leads-queries';

import sessionStore, { APOLLO_CLIENT } from '../../shared/session/session-store';
import {
  LEADS_ERROR_EVENT,
  LEADS_LIST_EVENT,
  LEADS_CREATE_EVENT,
  FILTER_OPTIONS_EVENT,
  FILTER_OPTIONS_ERROR_EVENT,
  AGENTS_UPDATE_EVENT,
  AGENTS_UPDATE_ERROR_EVENT,
  LEAD_UPDATE_ERROR_EVENT,
  TASK_ERROR_EVENT,
  TASK_CREATE_EVENT,
  NOTE_ERROR_EVENT,
  NOTE_CREATE_EVENT,
  TASK_TYPE_LIST_EVENT,
  TASK_TYPE_LIST_ERROR,
  LEAD_DETAIL_EVENT,
  LEAD_DETAIL_ERROR_EVENT,
} from './leads-store';
import { sanitize8BaseReferences } from '../../shared/utils';

import { sanitize8BaseReference, sanitize8BaseEmptyFields } from '../../shared/utils';

import {
  createTaskValidator,
  createNoteValidator,
  createAndUpdateLeadValidator,
} from './lead-validators';

/**
 * Creates a filter object
 *
 * @param {object} filterData - the data of the filter
 * @param {string} search - string to find specifics leads
 * @returns {object} the filter object
 */
const createFilter = (filterData, search) => {
  return {
    AND: [
      ...(filterData.leadType !== '' ? [{ type: { equals: filterData.leadType } }] : []),
      ...(filterData.leadVendor !== ''
        ? [
          {
            leadSource: { id: { equals: filterData.leadVendor } },
          },
        ]
        : []),
      ...(filterData.dateStart !== ''
        ? [
          {
            createdAt: { gte: filterData.dateStart + 'T00:00:00Z' },
          },
        ]
        : []),
      ...(filterData.dateEnd !== ''
        ? [
          {
            createdAt: { lte: filterData.dateEnd + 'T00:00:00Z' },
          },
        ]
        : []),
      ...(filterData.assignedTo !== ''
        ? [
          {
            agents: { some: { id: { equals: filterData.assignedTo } } },
          },
        ]
        : []),
      ...(filterData.hasNotes === 'Yes'
        ? [
          {
            notes: { some: { id: { not_equals: null } } },
          },
        ]
        : []),
      ...(filterData.hasNotes === 'No'
        ? [
          {
            notes: { none: {} },
          },
        ]
        : []),
    ],

    ...(search && {
      OR: [
        {
          firstName: {
            contains: search,
          },
        },
        {
          lastName: {
            contains: search,
          },
        },
      ],
    }),
  };
};

/**
 * Returns a Sort Object
 *
 * @param {string} sortData - the category to be sorted
 * @returns {object} the sort object
 *
 */
const createSort = (sortData) => {
  return {
    ...(sortData === 'top' && { sort: [{ priority: 'DESC' }] }),
    ...(sortData === 'low' && { sort: [{ priority: 'ASC' }] }),
    ...(sortData === 'newest' && { sort: [{ createdAt: 'DESC' }] }),
    ...(sortData === 'oldest' && { sort: [{ createdAt: 'ASC' }] }),
  };
};

/**
 * function to fetch leads.
 *
 * @param {number} page - the current page of the leads
 * @param {object} data - an object which contains the filterData
 * @param {string} search  - string to find specifics leads
 */
export const fetchLeads = async (page, data, search) => {
  let filter;
  let sort;
  if (data) {
    filter = createFilter(data, search);
    if (data.sort !== '') {
      sort = createSort(data.sort).sort;
    }
  }
  const client = sessionStore.getState(APOLLO_CLIENT);
  const skip = (page - 1) * 10;
  let response;
  const variables = { first: 10, skip, ...(filter && { filter }), ...(sort && { sort }) };
  try {
    response = await client.query({
      query: LEADS_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables,
    });
  } catch (e) {
    error('fetchLeads', e);
    return Flux.dispatchEvent(LEADS_ERROR_EVENT, e);
  }
  log('fetchLeads', response);
  Flux.dispatchEvent(LEADS_LIST_EVENT, response.data);
  return response.data;
};

export const fetchFilterOptions = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: FILTER_OPTIONS_QUERY,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('filterOptions', e);
    return Flux.dispatchEvent(FILTER_OPTIONS_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(FILTER_OPTIONS_EVENT, response.data);
  return response.data;
};

/**
 * @param {string| number} leadId
 * @param {Array} agentList
 */
export const updateLeadAgents = async (leadId, agentList) => {
  const agents = agentList.map((agentId) => ({ id: agentId }));
  const data = {
    id: leadId,
    agents: {
      reconnect: agents,
    },
  };
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    response = await client.mutate({
      mutation: LEAD_AGENTS_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('filterOptions', e);
    return Flux.dispatchEvent(AGENTS_UPDATE_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(AGENTS_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Updates the lead's fields provided
 *
 * @param {string|number} leadId
 * @param {object} fields - the fields to update
 */
export const updateLead = async (leadId, fields) => {
  const data = {
    id: leadId,
    ...fields,
  };
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.mutate({
      mutation: LEAD_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('filterOptions', e);
    return Flux.dispatchEvent(LEAD_UPDATE_ERROR_EVENT, e);
  }
  return response.data;
};

/**
 * @param {Array<string>} leadList - the ids of the leads to be deleted
 */
export const deleteLeads = async (leadList) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const operations = [];

  for (const lead of leadList) {
    let operation;
    try {
      operation = client.mutate({
        mutation: LEAD_DELETE_MUTATION,
        variables: { data: { id: lead, force: true } },
      });
    } catch (e) {
      error('deleteLead', e);
      continue;
    }
    operations.push(operation);
  }
  await Promise.all(operations);
  fetchLeads();
  return;
};

/**
 * Creates a Lead
 *
 * @param {*} lead
 */
export const createLead = async (lead) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    createAndUpdateLeadValidator(lead);
  } catch (e) {
    error('createLead', e);

    return Flux.dispatchEvent(LEADS_ERROR_EVENT, e);
  }

  sanitize8BaseReferences(lead, 'agents');

  try {
    response = await client.mutate({
      mutation: LEAD_CREATE_MUTATION,
      variables: { data: lead },
    });
  } catch (e) {
    error('createLead', e);

    return Flux.dispatchEvent(LEADS_ERROR_EVENT, e);
  }

  log('createLead', response);
  Flux.dispatchEvent(LEADS_CREATE_EVENT, response.data);

  return response.data;
};

/**
 * Creates a task
 *
 * @param {object} task
 * @returns {object}
 */
export const createTask = async (task) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    createTaskValidator(task);
  } catch (e) {
    error('createTask', e);
    console.log(e);
    return Flux.dispatchEvent(TASK_ERROR_EVENT, e);
  }

  sanitize8BaseEmptyFields(task);
  sanitize8BaseReferences(task, 'agents');
  sanitize8BaseReference(task, 'lead');
  sanitize8BaseReference(task, 'type');

  try {
    response = await client.mutate({
      mutation: TASK_CREATE_MUTATION,
      variables: { data: task },
    });
  } catch (e) {
    error('createTask', e);

    return Flux.dispatchEvent(TASK_ERROR_EVENT, e);
  }

  log('createTask', response);
  Flux.dispatchEvent(TASK_CREATE_EVENT, response.data);

  return response.data;
};

/**
 * Creates a note
 *
 * @param {object} note
 * @returns {object}
 */
export const createNote = async (note) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    createNoteValidator(note);
  } catch (e) {
    error('createNote', e);
    console.log(e);
    return Flux.dispatchEvent(NOTE_ERROR_EVENT, e);
  }

  sanitize8BaseReference(note, 'notesLeadRelation');

  try {
    response = await client.mutate({
      mutation: NOTE_CREATE_MUTATION,
      variables: { data: note },
    });
  } catch (e) {
    error('createNote', e);

    return Flux.dispatchEvent(NOTE_ERROR_EVENT, e);
  }

  log('createNote', response);
  Flux.dispatchEvent(NOTE_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetch the task's types
 */
export const fetchLeadTaskTypes = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: LEAD_TASK_TYPE_LIST_QUERY,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('filterOptions', e);
    return Flux.dispatchEvent(TASK_TYPE_LIST_ERROR, e);
  }
  Flux.dispatchEvent(TASK_TYPE_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetch lead details
 *
 * @param {string} id
 * @returns {object}
 */
export const fetchLeadDetail = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (client) {
    try {
      response = await client.query({
        query: LEAD_DETAIL_QUERY,
        variables: { id },
        fetchPolicy: 'network-only',
      });
    } catch (e) {
      error('fetchLeadDetail', e);

      return Flux.dispatchEvent(LEAD_DETAIL_ERROR_EVENT, e);
    }

    log('fetchLeadDetail', response);
    Flux.dispatchEvent(LEAD_DETAIL_EVENT, response.data);

    return response.data;
  }
};
