import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Card, Table, Dropdown, Menu, Pagination, Paper } from '@8base/boost';
import styled from 'styled-components';
import * as R from 'ramda';
import { onErrorMixinFC } from '../../shared/mixins';
import { useSubscription } from '@cobuildlab/react-flux-state';
import sessionStore, { NEW_SESSION_EVENT } from '../../shared/session/session-store';
import leadStore, {
  LEADS_LIST_EVENT,
  FILTER_OPTIONS_EVENT,
  AGENTS_UPDATE_EVENT,
  TASK_TYPE_LIST_EVENT,
  TASK_ERROR_EVENT,
  TASK_CREATE_EVENT,
  NOTE_CREATE_EVENT,
  NOTE_ERROR_EVENT,
} from './leads-store';
import { fetchLeads, fetchFilterOptions, deleteLeads, fetchLeadTaskTypes } from './leads-actions';
import LeadCell from './components/LeadCell';
import LeadFilter from './components/LeadFilter/LeadFilter';
import LeadsDialog from './components/LeadsDialog';
import LeadFormDialog from './components/LeadFormDialog';
import {
  SORT_OPTIONS,
  TABLE_COLUMNS,
  ACTIVE_COLUMNS,
  FILTER_CATEGORIES,
  FILTER_DATA,
} from './leads-model';
import agentsStore, { AGENTS_LIST_EVENT } from '../agents/agents-store';
import { fetchAgents } from '../agents/agents-actions';
import { useDidMount } from '../../shared/hooks/useDidMount';
import { dynamicSort } from '../../shared/utils/index';
import sortTableIcon from '../../shared/assets/images/table-sort-grey.svg';
import { canSeeBatchActions, canDeleteLeads } from './leads-utils';
import { getActiveAgency } from '../../shared/utils/agency-utils';
// import sortListIcon from '../../shared/assets/images/sort.svg';
import {
  DropdownBodyOnTable,
  ItemFilter,
  SearchInput,
  RadioInputField,
  ListCardBody as ListCard,
} from 'shared/components';
import * as toast from '../../shared/components/toast/Toast';
import LeadActions from './components/LeadActions/LeadActions';
import leadsStore from './leads-store';

const TableBody = styled(Table.Body)`
  max-height: 50vh;
  height: 50vh;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const CustomHeader = styled.h5`
  font-size: 1.3rem;
  font-weight: 600;
  color: #9b9b9b;
  text-transform: uppercase;
`;

const CustomGrid = styled.div`
  display: grid;
  width: ${(props) => props.width};
  grid-template-columns: ${(props) => props.columns};
  column-gap: ${(props) => props.columnGap};
  grid-template-areas: ${(props) => props.areas};
  ${(props) => (props.alignItems ? `align-items:${props.alignItems} ` : '')};
`;

const CustomGridCell = styled.div`
  grid-area: ${(props) => props.area};
  ${(props) => (props.justifySelf ? `justify-self:${props.justifySelf} ` : '')};
`;

const SearchBar = styled(SearchInput)`
  width: 100% !important;
`;

const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1.2rem;
  padding: 0 1rem;

  & > div > div > *::before {
    background-color: var(--color-primary);
    transition: all 0.2s;
  }

  & > div > span {
    display: inline-block;
    padding: 0.5rem 1.5rem;
    border: solid 1px #d0d7dd;
    border-radius: 5px;
  }
`;

const CustomBtn = styled.button`
  width: 100%;
  height: 100%;
  font-size: 1.4rem;
  font-weight: 400;
  border-radius: 5px;
  border: solid 1px #d0d7dd;
  background-color: #fff;
  padding: 0.5rem 1rem;
  box-sizing: border-box;
  color: #323c47;
  cursor: pointer;

  & strong {
    font-weight: 600 !important;
  }

  &:focus {
    outline: none;
  }

  ${({ disabled }) =>
    disabled
      ? `
  cursor: not-allowed;
  
  `
      : ''}};
`;

const ListCardBody = styled(ListCard)`
  padding: 0 !important;
`;

const SortTableBtn = styled.div`
  display: flex;
  border-radius: 5px;
  border: solid 1px ${({ active }) => (active ? '#3db4aa' : '#d0d7dd')};
  padding: 0.5rem 1rem;
  box-sizing: border-box;
  &::before {
    content: '';
    display: inline-block;
    height: 2.5rem;
    width: 2rem;

    background-color: ${({ active }) => (active ? '#3db4aa' : '#aeb7be')};
    -webkit-mask-image: url(${(props) => props.src});
    -webkit-mask-size: cover;
    mask-image: url(${(props) => props.src});
    mask-size: cover;
  }

  &:focus::before {
    background-color: #3db4aa;
  }
`;

const SortTableConfirm = styled.div`
  border-top: 1px solid #d0d7dd;
  padding: 10px 15px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;
const SortTableConfirmOptions = styled.span`
  color: ${(props) => (props.active ? '#3db4aa' : '#384A59')};
  cursor: pointer;
`;

const SortTableMenu = styled(Menu)`
  padding-bottom: 0 !important;
`;

const MenuItem = styled(Menu.Item)`
  color: #323c47 !important;
  padding: 0 !important;
  height: auto !important;
`;

const RadioButton = styled.span`
  height: 22px;
  width: 22px;
  border: 1px solid #d0d7dd;
  border-radius: 4px;
  display: inline-block;
  position: absolute;
  left: 15px;

  &::after {
    height: 10px;
    width: 10px;
    content: '';
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #3db4aa;
    opacity: 0;
  }
`;

const RadioInput = styled.input`
  display: none;
  &:checked ~ label span::after {
    opacity: 1;
  }
`;

const RadioLabel = styled.label`
  padding: 10px 30px 10px 50px;
  display: flex;
  align-items: center;
  width: 100%;
  cursor: pointer;
`;

// const SortListIcon = styled.img`
//   width: 16px;
//   height: 14px;
//   cursor: pointer;
//   margin-left: 4px;
// `;

const HeaderCellContainer = styled.div`
  display: flex;
  align-items: center;
`;

const TableRow = styled(Table.BodyRow)`
  ${(props) => (props.active ? `background-color: #fcfcfc !important` : '')};
`;

const columnsSize = '5% 10% 15% 10% 10% 15% 15% 10% 10%';

const LeadsView = () => {
  const [search, setSearch] = useState('');
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [leadsList, setLeadList] = useState({});
  const [selectedLeads, setSelectedLeads] = useState([]);
  const [selectedLead, setSelectedLead] = useState('');
  const [isOpenDelete, setIsOpenDelete] = useState(false);
  const [sortTableActive, setSortTableActive] = useState(false);
  const [tableColumns, setTableColumns] = useState(R.clone(TABLE_COLUMNS));
  const [activeColumns, setActiveColumns] = useState(R.clone(ACTIVE_COLUMNS));
  const [filterActive, setFilterActive] = useState(false);
  const [filter, setFilter] = useState(R.clone(FILTER_DATA));
  const [currentFilter, setCurrentFilter] = useState(R.clone(FILTER_DATA));
  const [filterCategories, setFilterCategories] = useState(R.clone(FILTER_CATEGORIES));
  const [agents, setAgents] = useState(undefined);
  const [isOpenForm, setIsOpenForm] = useState(false);
  // eslint-disable-next-line
  const [sortedColumn, SetSortedColumn] = useState('');
  const [leadTaskTypeList, setLeadTaskTypeList] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const [agency, setAgecy] = useState(undefined);
  const didMount = useDidMount();
  const paginationCtnRef = useRef();

  useSubscription(leadStore, LEADS_LIST_EVENT, (data) => {
    setLeadList(data.leadsList);
    setLoading(false);
  });
  useSubscription(agentsStore, AGENTS_LIST_EVENT, (data) => {
    setAgents(data.agentsList);
  });
  useSubscription(leadStore, TASK_TYPE_LIST_EVENT, (data) => {
    setLeadTaskTypeList(data.leadTaskTypesList.items);
  });
  useSubscription(leadStore, FILTER_OPTIONS_EVENT, (data) => {
    const categoriesClone = R.clone(filterCategories);
    categoriesClone.leadVendor.options = data.leadSourcesList.items.map((vendor) => ({
      label: vendor.name,
      value: vendor.id,
    }));
    categoriesClone.assignedTo.options = data.agentsList.items.map((agent) => ({
      label: `${agent.user.firstName} ${agent.user.lastName}`,
      value: agent.id,
    }));
    setFilterCategories(categoriesClone);
  });
  useSubscription(leadStore, AGENTS_UPDATE_EVENT, (data) => {
    const newLeadsList = R.clone(leadsList);
    for (const lead of newLeadsList.items) {
      if (lead.id === data.leadUpdate.id) {
        lead.agents = data.leadUpdate.agents;
      }
    }
    setLeadList(newLeadsList);
  });
  useSubscription(leadsStore, TASK_CREATE_EVENT, (data) => {
    toast.success('Task Created!');
  });
  useSubscription(leadsStore, NOTE_CREATE_EVENT, (data) => {
    toast.success('Note Created!');
  });
  useSubscription(leadsStore, TASK_ERROR_EVENT, onErrorMixinFC);
  useSubscription(leadsStore, NOTE_ERROR_EVENT, onErrorMixinFC);

  const filterHandler = useCallback(
    (identifier, value) => {
      const newFilter = { ...filter };
      newFilter[identifier] = value;
      if (identifier === 'dateStart') {
        if (newFilter.dateStart > newFilter.dateEnd) {
          newFilter.dateEnd = '';
        }
      }
      setFilter(newFilter);
    },
    [filter],
  );
  const handleOpenForm = () => setIsOpenForm(true);
  const handleCloseForm = () => setIsOpenForm(false);
  const leadCheckboxChangeHandler = (id) => {
    setSelectedLeads((oldSelectedList) => {
      let newSelectedList;
      if (oldSelectedList.includes(id)) {
        newSelectedList = oldSelectedList.filter((oldId) => oldId !== id);
      } else {
        newSelectedList = oldSelectedList.concat([id]);
      }
      return newSelectedList;
    });
  };
  const headerCheckboxHandler = () => {
    setSelectedLeads((oldSelectedLeads) => {
      let newSelectedLeads;
      if (oldSelectedLeads.length === leadsList.items.length) {
        newSelectedLeads = [];
      } else {
        newSelectedLeads = leadsList.items.map((lead) => lead.id);
      }
      return newSelectedLeads;
    });
  };
  const agencyHandler = async () => {
    const agency = await getActiveAgency();
    setAgecy(agency);
  };
  const searchBtnHandler = useCallback(() => {
    if (!loading) {
      setLoading(true);
    }
    if (page !== 1) {
      setPage(1);
    }
    fetchLeads(1, currentFilter, search);
  }, [currentFilter, search, loading, page]);
  const filterClickHandler = useCallback(() => {
    if (filterActive) {
      setFilter(currentFilter);
    }
    setFilterActive(!filterActive);
  }, [currentFilter, filterActive]);
  const confirmFilterHandler = useCallback(() => {
    setFilterActive(false);
    setCurrentFilter(filter);
  }, [filter]);
  const sortHandler = (value) => {
    const newState = R.clone(currentFilter);
    newState.sort = value;
    setCurrentFilter(newState);
  };
  const tableSortHandler = (sortIdentifier) => {
    const newColumnList = { ...tableColumns };
    newColumnList[sortIdentifier] = { ...tableColumns[sortIdentifier] };
    newColumnList[sortIdentifier].active = !newColumnList[sortIdentifier].active;
    setTableColumns(newColumnList);
  };
  const confirmSortTableHandler = (closeDropdown) => {
    const newActiveList = [];
    for (const key in tableColumns) {
      const columnClone = { ...tableColumns[key] };
      if (columnClone.active) {
        newActiveList.push(columnClone.value);
      }
    }
    setActiveColumns(newActiveList);
    setSortTableActive(false);
    closeDropdown();
  };
  const cancelSortTableHandler = (closeDropdown) => {
    const newLeadsColumns = { ...tableColumns };
    for (const key in newLeadsColumns) {
      newLeadsColumns[key] = { ...tableColumns[key] };
      if (activeColumns.includes(newLeadsColumns[key].value)) {
        newLeadsColumns[key].active = true;
      }
    }
    setTableColumns(newLeadsColumns);
    setSortTableActive(false);
    closeDropdown();
  };
  const pageChangeHandler = (pageNum) => {
    setLoading(true);
    setSelectedLeads([]);
    setPage(pageNum);
    fetchLeads(pageNum);
  };
  const deleteLeadsHandler = async (leads) => {
    setLoading(true);
    setIsOpenDelete(false);
    let leadsToDelete;
    if (typeof leads === 'string') {
      leadsToDelete = [leads];
      setSelectedLead('');
      if (selectedLeads.includes(leads)) {
        setSelectedLeads((prevValue) => {
          return prevValue.filter((l) => l !== leads);
        });
      }
    } else {
      leadsToDelete = [...selectedLeads];
      setSelectedLeads([]);
    }
    await deleteLeads(leadsToDelete);
    fetchLeads();
  };
  const onDeleteLeads = useCallback(
    (leadId) => {
      if (leadId || selectedLeads.length > 0) {
        if (leadId) {
          setSelectedLead(leadId);
        }
        setIsOpenDelete(true);
      }
    },
    [selectedLeads],
  );
  const handleOnYes = () => {
    if (selectedLead.length > 0) {
      deleteLeadsHandler(selectedLead);
    } else {
      deleteLeadsHandler(selectedLeads);
    }
    setIsOpenDelete(false);
  };
  const handleOnNo = () => {
    if (selectedLead.length > 0) {
      setSelectedLead('');
    }
    setIsOpenDelete(false);
  };

  // const sortColumnsHandler = (column) => {
  //   SetSortedColumn((prevSortedColumn) => {
  //     if (prevSortedColumn === column) {
  //       return `-${column}`;
  //     }
  //     return column;
  //   });
  // };

  useEffect(() => {
    fetchLeads();
    fetchFilterOptions();
    fetchAgents();
    fetchLeadTaskTypes();
    paginationCtnRef.current.children[0].children[3].childNodes[5].textContent = ' leads';
    agencyHandler();
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    setUser(user);
  }, []);
  useEffect(() => {
    if (didMount) {
      const newFilterCategories = R.clone(filterCategories);
      newFilterCategories.dateEnd.date.min =
        filter.dateStart !== '' ? new Date(filter.dateStart + 'T00:00:00.00') : undefined;
      setFilterCategories(newFilterCategories);
    }
    // eslint-disable-next-line
  }, [filter]);
  useEffect(() => {
    if (didMount) {
      setLoading(true);
      setPage(1);
      fetchLeads(1, currentFilter, search.value);
    }
    // eslint-disable-next-line
  }, [currentFilter]);
  useEffect(() => {
    if (leadsList.count > 0 && didMount) {
      let newList = R.clone(leadsList);
      newList.items = newList.items.sort(dynamicSort(sortedColumn));
      setLeadList(newList);
    }
    // eslint-disable-next-line
  }, [sortedColumn]);

  const headerCols = activeColumns.map((col) => {
    let icon;
    // if (canSort(col)) {
    //   icon = (
    //     <SortListIcon
    //       src={sortListIcon}
    //       onClick={() => {
    //         sortColumnsHandler(getLeadPropertyName(col));
    //       }}
    //     />
    //   );
    // }
    return (
      <Table.HeaderCell key={col}>
        <HeaderCellContainer>
          {col}
          {icon}
        </HeaderCellContainer>
      </Table.HeaderCell>
    );
  });
  const tableHeader = (
    <Table.Header columns={columnsSize}>
      <Table.HeaderCell>
        {leadsList.items && (
          <RadioInputField
            left
            checked={selectedLeads.length === leadsList.items.length}
            square
            onChange={headerCheckboxHandler}
          />
        )}
      </Table.HeaderCell>
      {headerCols}
      <Table.HeaderCell>Actions</Table.HeaderCell>
    </Table.Header>
  );
  const sortColumns = Object.entries(tableColumns).map(([identifier, column]) => {
    return (
      <MenuItem key={identifier} onClick={() => {}}>
        <RadioInput id={column.value} type="radio" checked={column.active} />
        <RadioLabel for={column.value} onClick={() => tableSortHandler(identifier)}>
          <RadioButton />
          {column.value}
        </RadioLabel>
      </MenuItem>
    );
  });
  let batchActions;

  if (canSeeBatchActions(user, agency)) {
    batchActions = (
      <>
        <CustomGridCell area="batch" justifySelf="center" alignSelf="center">
          <CustomHeader>Batch Actions</CustomHeader>
        </CustomGridCell>
        <CustomGridCell area="delete">
          <CustomBtn onClick={onDeleteLeads} disabled={selectedLeads.length < 1}>
            Delete Leads
          </CustomBtn>
        </CustomGridCell>
        <CustomGridCell area="assign">
          <CustomBtn>Assign to agent</CustomBtn>
        </CustomGridCell>
      </>
    );
  }

  return (
    <div className="p24">
      <Paper>
        <Card.Header>
          <CustomGrid
            alignItems="center"
            columnGap="1.5rem"
            columns="minmax(157px, 402px) 96px 107px 112px 131px 220px 42px 130px"
            areas="'input filter batch delete assign sort btn create'"
            width="100%">
            <CustomGridCell area="input">
              <SearchBar
                placeholder="Search leads"
                value={search}
                onIconClick={searchBtnHandler}
                onChange={(value) => {
                  setSearch(value);
                }}
              />
            </CustomGridCell>
            <CustomGridCell area="filter">
              <LeadFilter
                onFilterClick={filterClickHandler}
                active={filterActive}
                onFilter={filterHandler}
                values={filter}
                categories={filterCategories}
                onConfirm={confirmFilterHandler}
                onCancel={() => {
                  setFilter(currentFilter);
                }}
              />
            </CustomGridCell>
            {batchActions}
            <CustomGridCell area="sort">
              <ItemFilter
                onChange={(value) => {
                  sortHandler(value);
                }}
                value={currentFilter.sort}
                options={SORT_OPTIONS}
                placeholder="Sort leads by"
              />
            </CustomGridCell>
            <CustomGridCell />
            <CustomGridCell area="btn" justifySelf="center">
              <Dropdown defaultOpen={false}>
                <Dropdown.Head>
                  <SortTableBtn
                    active={sortTableActive}
                    src={sortTableIcon}
                    onClick={() => setSortTableActive(!sortTableActive)}
                  />
                </Dropdown.Head>
                <DropdownBodyOnTable closeOnClickOutside={false} pin="left">
                  {({ closeDropdown }) => (
                    <SortTableMenu>
                      {sortColumns}
                      <SortTableConfirm>
                        <SortTableConfirmOptions
                          active
                          onClick={() => {
                            confirmSortTableHandler(closeDropdown);
                          }}>
                          Accept
                        </SortTableConfirmOptions>
                        <SortTableConfirmOptions
                          onClick={() => {
                            cancelSortTableHandler(closeDropdown);
                          }}>
                          Cancel
                        </SortTableConfirmOptions>
                      </SortTableConfirm>
                    </SortTableMenu>
                  )}
                </DropdownBodyOnTable>
              </Dropdown>
            </CustomGridCell>
            <CustomGridCell area="create">
              <CustomBtn color="neutral" size="sm" onClick={handleOpenForm}>
                <strong>Create Lead</strong>
              </CustomBtn>
            </CustomGridCell>
          </CustomGrid>
        </Card.Header>
        <ListCardBody>
          <Table>
            {tableHeader}
            <TableBody onScroll={(event) => {}} loading={loading} data={leadsList.items}>
              {(lead, index) => {
                // const isLast = index === leadsList.length - 1;
                // const pageLoader = isLast && loadingPage ? <Loader stretch /> : null;
                const columnList = activeColumns.map((columnName) => (
                  <LeadCell
                    key={columnName}
                    columnName={columnName}
                    lead={lead}
                    agents={agents}
                    user={user}
                    agency={agency}
                  />
                ));
                return (
                  <>
                    <TableRow
                      active={selectedLeads.includes(lead.id)}
                      columns={columnsSize}
                      key={lead.id}>
                      <Table.BodyCell>
                        <RadioInputField
                          checked={selectedLeads.includes(lead.id)}
                          square
                          left
                          onChange={() => {
                            leadCheckboxChangeHandler(lead.id);
                          }}
                        />
                      </Table.BodyCell>
                      {columnList}
                      <LeadActions
                        canDelete={canDeleteLeads(user, agency)}
                        taskTypes={leadTaskTypeList}
                        agents={agents.items}
                        onDelete={() => {
                          onDeleteLeads(lead.id);
                        }}
                        lead={lead}
                      />
                    </TableRow>
                  </>
                );
              }}
            </TableBody>
          </Table>
        </ListCardBody>
        <Table.Footer>
          <PaginationContainer ref={paginationCtnRef}>
            <Pagination
              onChange={(pageNum) => {
                pageChangeHandler(pageNum);
              }}
              defaultPage={1}
              total={leadsList.count}
              page={page}
            />
          </PaginationContainer>
        </Table.Footer>
        <LeadsDialog
          isOpen={isOpenDelete}
          onYes={handleOnYes}
          onNo={handleOnNo}
          onClose={handleOnNo}
          yesText="Yes, Delete"
          noText="Cancel"
          selectedLeads={selectedLeads}
          selectedLead={selectedLead}
        />
        <LeadFormDialog
          isOpen={isOpenForm}
          onCloseDialog={handleCloseForm}
          afterCreate={() => setLoading(true)}
        />
      </Paper>
    </div>
  );
};

export default LeadsView;
