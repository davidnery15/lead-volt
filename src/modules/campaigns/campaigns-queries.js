import gql from 'graphql-tag';

const CampaignFragment = gql`
  fragment CampaignFragment on Campaign {
    id
    name
    status
    createdAt
    type
    priority
    agents {
      items {
        id
        user {
          id
          firstName
          lastName
          email
        }
      }
    }
    emailMarketingCampaign {
      id
      name
    }
  }
`;

export const CAMPAIGN_DETAIL_QUERY = gql`
  query($id: ID!) {
    campaign(id: $id) {
      ...CampaignFragment
    }
  }
  ${CampaignFragment}
`;

export const CAMPAIGN_LIST_QUERY = gql`
  query($skip: Int, $first: Int, $filter: CampaignFilter, $sort: [CampaignSort!]) {
    campaignsList(skip: $skip, first: $first, filter: $filter, sort: $sort) {
      items {
        ...CampaignFragment
      }
    }
  }
  ${CampaignFragment}
`;

export const CAMPAIGN_CREATE_MUTATION = gql`
  mutation($data: CampaignCreateInput!) {
    campaignCreate(data: $data) {
      ...CampaignFragment
    }
  }
  ${CampaignFragment}
`;

export const CAMPAIGN_DELETE_MUTATION = gql`
  mutation($data: CampaignDeleteInput!) {
    campaignDelete(data: $data) {
      success
    }
  }
`;

export const CAMPAIGN_UPDATE_MUTATION = gql`
  mutation($data: CampaignUpdateInput!) {
    campaignUpdate(data: $data) {
      ...CampaignFragment
    }
  }
  ${CampaignFragment}
`;
