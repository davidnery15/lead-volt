import React from 'react';
import { YesNoDialog } from 'shared/components';

const DialogDeleteCampaign = (props) => {
  return (
    <YesNoDialog
      yesText="Yes, Delete"
      noText="Cancel"
      title="Delete Campaign"
      text="Are you sure you want to delete this campaign? This action can’t be undone."
      {...props}
    />
  );
};

export default DialogDeleteCampaign;
