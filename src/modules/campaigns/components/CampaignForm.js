import React from 'react';
import PropTypes from 'prop-types';
import { Form, Grid, InputField, SelectField, Label } from '@8base/boost';
import { CAMPAIGN_TYPES, PRIORITIES, CAMPAIGN_STATUSES } from '../../../shared/constants';
import { agentsMapSelectOptions, agentsSelectValues } from '../../agents/agents-utils';
import { Margin, DialogForm, SelectMultipleAgents } from 'shared/components';

const CampaignForm = ({
  isOpen,
  onCloseForm,
  onChange,
  data,
  onSubmit,
  emailsCampaign,
  agents,
  isEdit,
  isLoading,
}) => {
  return (
    <div>
      <DialogForm
        isLoading={isLoading}
        isOpen={isOpen}
        title={`${isEdit ? 'Edit' : 'Create'} a Campaign`}
        createText={isEdit ? 'Edit' : 'Create'}
        onCloseDialog={onCloseForm}
        onClickCreate={onSubmit}>
        <Form>
          <Form.SectionBody>
            <Grid.Layout gap="md" columns="70% calc(30% - 16px)">
              <Grid.Box>
                <InputField
                  stretch
                  label="Name"
                  placeholder="Name"
                  input={{
                    name: 'name',
                    type: 'text',
                    value: data.name,
                    onChange: (value) => onChange('name', value),
                  }}
                />
              </Grid.Box>
              <Grid.Box>
                <SelectField
                  label="Status"
                  name="campaign_status"
                  placeholder="Status"
                  input={{
                    name: 'campaign_status',
                    value: data.status,
                    onChange: (value) => onChange('status', value),
                  }}
                  options={CAMPAIGN_STATUSES.map((status, index) => ({
                    label: status,
                    value: index,
                  }))}
                />
              </Grid.Box>
            </Grid.Layout>
            <Grid.Layout gap="md" inline columns="70% calc(30% - 16px)">
              <Grid.Box>
                <SelectField
                  label="Type"
                  name="campaign_type"
                  placeholder="Type"
                  input={{
                    name: 'campaign_type',
                    value: data.type,
                    onChange: (value) => onChange('type', value),
                  }}
                  options={CAMPAIGN_TYPES.map((type, index) => ({
                    label: type,
                    value: index,
                  }))}
                />
              </Grid.Box>
              <Grid.Box>
                <SelectField
                  label="Priority"
                  name="priority"
                  placeholder="Priority"
                  input={{
                    name: 'priority',
                    value: data.priority,
                    onChange: (value) => onChange('priority', value),
                  }}
                  options={PRIORITIES.map((priority, index) => ({
                    label: priority,
                    value: index,
                  }))}
                />
              </Grid.Box>
            </Grid.Layout>
            <Grid.Layout gap="md">
              <Grid.Box>
                <Margin bottom="4px">
                  <Label kind="secondary" text="Assign Agents" />
                </Margin>
                <SelectMultipleAgents
                  name="agents"
                  placeholder="Assign agents"
                  defaultValue={agentsSelectValues(data.agents)}
                  onChange={(value) => onChange('agents', value)}
                  options={agents.map(agentsMapSelectOptions)}
                />
              </Grid.Box>
              <Grid.Box>
                <SelectField
                  label="Email Marketing"
                  name="emailMarketingCampaign"
                  placeholder="Email Marketing"
                  input={{
                    name: 'emailMarketingCampaignId',
                    value: data.emailMarketingCampaignId,
                    onChange: (value) => onChange('emailMarketingCampaignId', value),
                  }}
                  options={emailsCampaign.map((email) => ({
                    label: email.name,
                    value: email.id,
                  }))}
                />
              </Grid.Box>
            </Grid.Layout>
          </Form.SectionBody>
        </Form>
      </DialogForm>
    </div>
  );
};

CampaignForm.propTypes = {
  data: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  agents: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCloseForm: PropTypes.func.isRequired,
  emailsCampaign: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  isLoading: PropTypes.bool,
};

CampaignForm.defaultProps = {
  isEdit: false,
  isLoading: false,
};

export default CampaignForm;
