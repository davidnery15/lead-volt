import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '@8base/boost';
import { TextEllipsis } from 'shared/components';

const EmailMarketingCell = ({ value }) => {
  return (
    <TextEllipsis>
      <Text color="PRIMARY">{value}</Text>
    </TextEllipsis>
  );
};

EmailMarketingCell.propTypes = {
  value: PropTypes.string.isRequired,
};

export default EmailMarketingCell;
