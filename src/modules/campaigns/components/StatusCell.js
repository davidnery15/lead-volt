import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'shared/components';
import { CAMPAIGN_STATUSES } from '../../../shared/constants';
import { updateCampaign } from '../campaigns-actions';
import styled from 'styled-components';

const DropdownOption = styled.div`
  padding: 0px ${(props) => (props.isHead ? '0px' : '16px')};
`;
const component = (priority) => (props) => <DropdownOption {...props}>{priority}</DropdownOption>;

const StatusCell = ({ rowData, value }) => {
  const [status, setStatus] = useState(value);
  const handleOnChange = (value) => {
    setStatus(value);
    updateCampaign({ id: rowData.id, status: value });
  };

  return (
    <Dropdown
      minWidth="100px"
      placeholder="Select Status"
      currentOption={status}
      options={CAMPAIGN_STATUSES.map((status, index) => {
        return {
          value: status,
          Component: component(status),
        };
      })}
      onChange={handleOnChange}
    />
  );
};

StatusCell.propTypes = {
  value: PropTypes.string.isRequired,
  rowData: PropTypes.object.isRequired,
};

export default StatusCell;
