import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { IconInfo } from 'shared/components/ui/Icons';

const TableActions = ({ id, history }) => (
  <>
    <IconInfo size="md" onClick={() => history.push(`/campaigns/${id}`)} />
  </>
);

TableActions.propTypes = {
  id: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(TableActions);
