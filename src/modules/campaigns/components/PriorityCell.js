import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'shared/components';
import { PRIORITIES } from '../../../shared/constants';
import styled from 'styled-components';
import { updateCampaign } from '../campaigns-actions';

const DropdownOption = styled.div`
  padding: 0px ${(props) => (props.isHead ? '0px' : '16px')};
`;
const component = (priority) => (props) => <DropdownOption {...props}>{priority}</DropdownOption>;

const PriorityCell = ({ rowData, value }) => {
  const [priority, setPriority] = useState(value);
  const handleOnChange = (value) => {
    setPriority(value);
    updateCampaign({ id: rowData.id, priority: (value + 1).toString() });
  };

  return (
    <Dropdown
      minWidth="100px"
      placeholder="Select Priority"
      currentOption={priority}
      options={PRIORITIES.map((priority, index) => {
        return {
          value: index,
          Component: component(priority),
        };
      })}
      onChange={handleOnChange}
    />
  );
};

PriorityCell.propTypes = {
  value: PropTypes.string.isRequired,
  rowData: PropTypes.object.isRequired,
};

export default PriorityCell;
