import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';
import {
  CAMPAIGN_LIST_QUERY,
  CAMPAIGN_CREATE_MUTATION,
  CAMPAIGN_DELETE_MUTATION,
  CAMPAIGN_DETAIL_QUERY,
  CAMPAIGN_UPDATE_MUTATION,
} from './campaigns-queries';

import sessionStore, { APOLLO_CLIENT } from '../../shared/session/session-store';
import {
  CAMPAIGN_ERROR_EVENT,
  CAMPAIGN_LIST_EVENT,
  CAMPAIGN_CREATE_EVENT,
  CAMPAIGN_DELETE_EVENT,
  CAMPAIGN_DETAIL_EVENT,
  CAMPAIGN_UPDATE_EVENT,
} from './campaigns-store';
import {
  sanitize8BaseReference,
  sanitize8BaseReconnects,
  sanitize8BaseReferences,
  sanitize8BaseEmptyFields,
} from '../../shared/utils';
import { createAndUpdateCampaignValidator } from './campaigns-validators';
import { CAMPAIGN_STATUSES } from '../../shared/constants';

/**
 * create filters for campaigns query.
 *
 * @param {object} filterData createdAt, updatedAt, assignedTo and status filters.
 * @param {string} search string from search input.
 * @returns {object} graphql filter object.
 */
const createFilter = (filterData, search) => {
  return {
    ...(filterData && {
      AND: [
        ...(filterData.dateCreatedStart && filterData.dateCreatedStart !== ''
          ? [
            {
              createdAt: { gte: filterData.dateCreatedStart + 'T00:00:00Z' },
            },
          ]
          : []),
        ...(filterData.dateCreatedEnd && filterData.dateCreatedEnd !== ''
          ? [
            {
              createdAt: { lte: filterData.dateCreatedEnd + 'T00:00:00Z' },
            },
          ]
          : []),
        ...(filterData.assignedTo && filterData.assignedTo !== ''
          ? [
            {
              agents: { some: { id: { equals: filterData.assignedTo } } },
            },
          ]
          : []),
        ...(filterData.lastUpdatedStart && filterData.lastUpdatedStart !== ''
          ? [
            {
              updatedAt: { gte: filterData.lastUpdatedStart + 'T00:00:00Z' },
            },
          ]
          : []),
        ...(filterData.lastUpdatedEnd && filterData.lastUpdatedEnd !== ''
          ? [
            {
              updatedAt: { lte: filterData.lastUpdatedEnd + 'T00:00:00Z' },
            },
          ]
          : []),
        ...(filterData.status && filterData.status !== ''
          ? [
            {
              status: { equals: CAMPAIGN_STATUSES[filterData.status] },
            },
          ]
          : []),
        ...(filterData.agency && filterData.agency !== ''
          ? [
            {
              agency: { id: { equals: filterData.agency } },
            },
          ]
          : []),
      ],
    }),
    ...(search && {
      name: {
        contains: search,
      },
    }),
  };
};

/**
 * create sort object for campaigns query.
 *
 * @param {string} sortData string from select sort.
 * @returns {object} graphql sort object.
 */
const createSort = (sortData) => {
  return {
    ...(sortData === 'top' && { sort: [{ priority: 'DESC' }] }),
    ...(sortData === 'low' && { sort: [{ priority: 'ASC' }] }),
    ...(sortData === 'newest' && { sort: [{ createdAt: 'DESC' }] }),
    ...(sortData === 'oldest' && { sort: [{ createdAt: 'ASC' }] }),
  };
};

/**
 * Creates a campaign
 *
 * @param {object} campaign data from the create form.
 * @returns {object} the response data
 */
export const createCampaign = async (campaign) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    createAndUpdateCampaignValidator(campaign);
  } catch (e) {
    error('createCampaign', e);

    return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
  }

  sanitize8BaseEmptyFields(campaign);
  sanitize8BaseReference(campaign, 'emailMarketingCampaign');
  sanitize8BaseReferences(campaign, 'agents');

  try {
    response = await client.mutate({
      mutation: CAMPAIGN_CREATE_MUTATION,
      variables: { data: campaign },
    });
  } catch (e) {
    error('createCampaign', e);

    return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
  }

  log('createCampaign', response);
  Flux.dispatchEvent(CAMPAIGN_CREATE_EVENT, response.data);

  return response.data;
};

/**
 * Action for fetch the campaigns
 *
 * @param {object} data filter data that contains search, filters and sort.
 * @returns {object} the response data
 */
export const fetchCampaigns = async (data = {}) => {
  const { first, search, filterData, sortData } = data;
  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = createFilter(filterData, search);
  const sort = sortData ? createSort(sortData).sort : null;
  let response;

  if (client) {
    const variables = {
      ...(first && { first }),
      ...(filter && { filter }),
      ...(sort && { sort }),
    };

    try {
      response = await client.query({
        query: CAMPAIGN_LIST_QUERY,
        fetchPolicy: 'network-only',
        variables,
      });
    } catch (e) {
      error('fetchCampaigns', e);

      return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
    }

    log('fetchCampaigns', response);
    Flux.dispatchEvent(CAMPAIGN_LIST_EVENT, response.data);

    return response.data;
  }
};

/**
 * Deletes a campaign by id
 *
 * @param {string} id identifier of the campaign
 * @returns {object} the response data
 */
export const deleteCampaign = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (client) {
    try {
      response = await client.mutate({
        mutation: CAMPAIGN_DELETE_MUTATION,
        variables: { data: { id } },
      });
    } catch (e) {
      error('campaignDelete', e);

      return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
    }

    log('campaignDelete', response);
    Flux.dispatchEvent(CAMPAIGN_DELETE_EVENT, response.data);

    return response.data;
  }
};

/**
 * Show details of a campaign
 *
 * @param {string} id identifier of the campaign
 * @returns {object} the response data
 */
export const fetchCampaignDetail = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (client) {
    try {
      response = await client.query({
        query: CAMPAIGN_DETAIL_QUERY,
        variables: { id },
      });
    } catch (e) {
      error('fetchCampaignDetail', e);

      return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
    }

    log('fetchCampaignDetail', response);
    Flux.dispatchEvent(CAMPAIGN_DETAIL_EVENT, response.data);

    return response.data;
  }
};

/**
 * Updates a campaign
 *
 * @param {object} campaign data from the update form.
 * @param {boolean} validate make the validate or not.
 * @returns {object} the response data
 */
export const updateCampaign = async (campaign, validate) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (validate) {
    try {
      createAndUpdateCampaignValidator(campaign);
    } catch (e) {
      error('updateCampaign', e);

      return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
    }
  }

  sanitize8BaseEmptyFields(campaign);
  sanitize8BaseReconnects(campaign, 'agents');
  sanitize8BaseReference(campaign, 'emailMarketingCampaign');

  try {
    response = await client.mutate({
      mutation: CAMPAIGN_UPDATE_MUTATION,
      variables: { data: campaign },
    });
  } catch (e) {
    error('updateCampaign', e);

    return Flux.dispatchEvent(CAMPAIGN_ERROR_EVENT, e);
  }

  log('updateCampaign', response);
  Flux.dispatchEvent(CAMPAIGN_UPDATE_EVENT, response.data);

  return response.data;
};
