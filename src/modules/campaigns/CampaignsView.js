import React from 'react';
import { Paper, Grid, SelectField } from '@8base/boost';
import CampaignForm from './components/CampaignForm';
import { onChangeDataMixin, onErrorMixin, onChangeFilterMixin } from '../../shared/mixins';
import model, { SORT_OPTIONS } from './campaigns-model';
import * as R from 'ramda';
import { fetchCampaigns, createCampaign } from './campaigns-actions';
import campaignsStore, {
  CAMPAIGN_LIST_EVENT,
  CAMPAIGN_ERROR_EVENT,
  CAMPAIGN_CREATE_EVENT,
  CAMPAIGN_DELETE_EVENT,
} from './campaigns-store';
import { CAMPAIGN_TYPES, CAMPAIGN_STATUSES } from '../../shared/constants';
import { campaignMap, campaignMapTable } from './campaigns-utils';
import { fetchEmailsCampaign } from './emailsMarketingCampaign/emails-actions';
import emailsStore, { EMAILS_CAMPAIGN_LIST_EVENT } from './emailsMarketingCampaign/emails-store';
import { fetchAgents } from '../agents/agents-actions';
import agentsStore, { AGENTS_LIST_EVENT, AGENTS_ERROR_EVENT } from '../agents/agents-store';
import CampaignTableActions from './components/CampaignTableActions';
import { Table, SearchInput, ButtonIcon, Margin } from 'shared/components';
import DialogDeleteCampaign from './components/DialogDeleteCampaign';
import CampaignClass from './CampaignClass';
import * as toast from 'shared/components/toast/Toast';
import styled from 'styled-components';
import CampaignFilter from './CampaignFilter';
import { FILTER_DATA, FILTER_CATEGORIES } from './campaigns-model';
import EmailMarketingCell from './components/EmailMarketingCell';
import PriorityCell from './components/PriorityCell';
import StatusCell from './components/StatusCell';
import { nameCellComponent } from '../../shared/components/campaigns/nameCellComponent';
import { getActiveAgency } from '../../shared/utils/agency-utils';

const StyledLayout = styled(Grid.Layout)`
  padding: 16px;
`;

const StyledBox = styled(Grid.Box)`
  display: grid;
  align-items: flex-end;
  justify-content: flex-end;
  flex-direction: row !important;
`;

const priorityCellComponent = (rowData, value) => {
  const component = () => <PriorityCell rowData={rowData} value={value} />;

  return component;
};
const emailMarketingCellComponent = (rowData, value) => {
  const component = () => <EmailMarketingCell value={value} />;

  return component;
};
const statusCellComponent = (rowData, value) => {
  const component = () => <StatusCell rowData={rowData} value={value} />;

  return component;
};

export class Campaigns extends CampaignClass {
  constructor(props) {
    super(props);

    this.state = {
      isOpenForm: false,
      data: R.clone(model),
      campaigns: [],
      isLoading: false,
      isLoadingForm: false,
      emailsCampaign: [],
      agents: [],
      isOpenDelete: false,
      currentCampaignId: null,
      filters: {
        search: '',
        ...R.clone(FILTER_DATA),
      },
      sort: '',
      isOpenFilters: false,
      filterCategories: R.clone(FILTER_CATEGORIES),
      currentFilters: R.clone(FILTER_DATA),
    };
    this.handleOnChangeData = onChangeDataMixin.bind(this);
    this.handleOnChangeFilter = onChangeFilterMixin.bind(this);
    this.onError = onErrorMixin.bind(this);

    this.subscribe(campaignsStore, CAMPAIGN_LIST_EVENT, (data) => {
      this.setState({
        isLoading: false,
        campaigns: data.campaignsList.items.map(campaignMap),
      });
    });
    this.subscribe(campaignsStore, CAMPAIGN_ERROR_EVENT, this.onError);
    this.subscribe(campaignsStore, CAMPAIGN_CREATE_EVENT, (data) => {
      const { campaigns } = this.state;
      const {
        id,
        name,
        createdAt,
        type,
        priority,
        agents,
        emailMarketingCampaign,
        status,
      } = data.campaignCreate;
      const newCampaign = campaignMap({
        id,
        name,
        status,
        createdAt,
        type,
        priority,
        agents,
        emailMarketingCampaign,
      });

      toast.success('Campaigns', 'Campaign Successfully Created');
      this.setState({
        data: R.clone(model),
        isLoadingForm: false,
        isOpenForm: false,
        campaigns: [...campaigns, newCampaign],
      });
    });
    this.subscribe(emailsStore, EMAILS_CAMPAIGN_LIST_EVENT, (data) => {
      this.setState({
        emailsCampaign: data.emailMarketingCampaignsList.items,
      });
    });
    this.subscribe(agentsStore, AGENTS_LIST_EVENT, (data) => {
      const activeAgency = getActiveAgency();
      const filterCategoriesClone = R.clone(this.state.filterCategories);

      filterCategoriesClone.assignedTo.options = data.agentsList.items.map((agent) => ({
        label: `${agent.user.firstName} ${agent.user.lastName}`,
        value: agent.id,
      }));

      this.setState({
        filterCategories: filterCategoriesClone,
        agents: data.agentsList.items.filter((agent) => {
          return agent.agency.id === activeAgency.id;
        }),
      });
    });
    this.subscribe(agentsStore, AGENTS_ERROR_EVENT, this.onError);
    this.subscribe(campaignsStore, CAMPAIGN_DELETE_EVENT, (data) => {
      const { currentCampaignId, campaigns } = this.state;

      if (data.campaignDelete.success) {
        this.setState({
          isLoading: false,
          currentCampaignId: null,
          campaigns: campaigns.filter((campaign) => campaign.id !== currentCampaignId),
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    });
  }

  getAgencyId = () => {
    const agency = getActiveAgency();
    return agency.id;
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    fetchCampaigns({ filterData: { agency: this.getAgencyId } });
    fetchAgents();
  }

  handleOnCloseForm = () => {
    this.setState({ isOpenForm: false });
  };

  handleOnClickCreate = () => {
    this.setState({ isOpenForm: true });
    fetchEmailsCampaign();
  };

  handleOnSubmitForm = () => {
    this.setState({ isLoadingForm: true });
    // eslint-disable-next-line
    const data = R.clone(this.state.data);
    const campaign = {
      name: data.name,
      status: CAMPAIGN_STATUSES[data.status],
      type: CAMPAIGN_TYPES[data.type],
      priority: (data.priority + 1).toString(),
      agents: data.agents.map((agent) => agent.value),
      emailMarketingCampaign: data.emailMarketingCampaignId,
    };

    createCampaign(campaign);
  };

  handleOnClickBtnSearch = () => {
    this.setState({ isLoading: true });
    const { filters } = this.state;

    fetchCampaigns({ search: filters.search });
  };

  handleOnClickFilterBtn = () => {
    this.setState({
      isOpenFilters: true,
    });
  };

  handleOnFilter = (identifier, value) => {
    const newFilters = { ...this.state.filters };

    newFilters[identifier] = value;

    this.setState({
      filters: newFilters,
    });
  };

  handleOnConfirmFilters = () => {
    this.setState(
      {
        isLoading: true,
        isOpenFilters: false,
        currentFilters: this.state.filters,
      },
      () => {
        fetchCampaigns({
          search: this.state.filters.search,
          filterData: this.state.currentFilters,
        });
      },
    );
  };

  render() {
    const {
      state: {
        isOpenForm,
        data,
        campaigns,
        isLoading,
        emailsCampaign,
        agents,
        isOpenDelete,
        isLoadingForm,
        isOpenFilters,
        filters,
        currentFilters,
        filterCategories,
        sort,
      },
      handleOnChangeData,
      handleOnCloseForm,
      handleOnSubmitForm,
      handleOnClickCreate,
      handleOnNo,
      handleOnYes,
      handleOnChangeFilter,
      handleOnClickBtnSearch,
      handleOnClickFilterBtn,
      handleOnFilter,
      handleOnConfirmFilters,
    } = this;

    return (
      <div className="p24">
        <Paper>
          <StyledLayout inline columns="30% 8% calc(62% - 32px)" gap="md">
            <Grid.Box>
              <SearchInput
                onIconClick={handleOnClickBtnSearch}
                onChange={(value) => handleOnChangeFilter('search', value)}
                placeholder="Search campaigns"
              />
            </Grid.Box>
            <Grid.Box>
              <CampaignFilter
                onFilterClick={handleOnClickFilterBtn}
                active={isOpenFilters}
                onFilter={handleOnFilter}
                values={filters}
                categories={filterCategories}
                onConfirm={handleOnConfirmFilters}
                onCancel={() => {
                  this.setState({
                    isOpenFilters: false,
                    filters: currentFilters,
                  });
                }}
              />
            </Grid.Box>
            <StyledBox>
              <Margin right="16px" style={{ width: '204px' }}>
                <SelectField
                  stretch
                  input={{
                    name: 'sort',
                    value: sort,
                    onChange: (value) => {
                      this.setState(
                        {
                          sort: value,
                          isLoading: true,
                        },
                        () => {
                          fetchCampaigns({
                            search: filters.search,
                            sortData: this.state.sort,
                            filterData: currentFilters,
                          });
                        },
                      );
                    },
                  }}
                  placeholder="Sort campaigns by"
                  options={SORT_OPTIONS}
                />
              </Margin>
              <ButtonIcon
                text="Create Campaign"
                onClick={handleOnClickCreate}
                iconSvg={require('../../shared/assets/images/campaigns-gray.svg')}
              />
            </StyledBox>
          </StyledLayout>
          <Table
            withCheck
            withPagination
            withLabelActions
            data={campaigns.map(campaignMapTable)}
            isLoading={isLoading}
            columnsSize="4% 10% 15% 10% 10% 16% 12% 7% 7% 9%"
            columns={[
              {
                name: 'NAME',
                customBodyRender: nameCellComponent,
              },
              { name: 'CREATED' },
              { name: 'TYPE' },
              {
                name: 'PRIORITY',
                customBodyRender: priorityCellComponent,
              },
              { name: 'ASSIGNED TO' },
              {
                name: 'MARKETING CAMPAIGN',
                customBodyRender: emailMarketingCellComponent,
              },
              { name: 'LEADS' },
              {
                name: 'STATUS',
                customBodyRender: statusCellComponent,
              },
            ]}
            Actions={CampaignTableActions}
            dropdownOptions={[
              {
                name: 'Delete Campaign',
                color: 'DANGER',
                onClick: (id) => {
                  this.setState({
                    isOpenDelete: true,
                    currentCampaignId: id,
                  });
                },
              },
            ]}
            ignoreColumns={['id']}
            leftDropdown="-130px"
          />
          <CampaignForm
            data={data}
            isLoading={isLoadingForm}
            agents={agents}
            isOpen={isOpenForm}
            onChange={handleOnChangeData}
            onSubmit={handleOnSubmitForm}
            onCloseForm={handleOnCloseForm}
            emailsCampaign={emailsCampaign}
          />
          <DialogDeleteCampaign
            isOpen={isOpenDelete}
            onYes={handleOnYes}
            onNo={handleOnNo}
            onClose={handleOnNo}
          />
        </Paper>
      </div>
    );
  }
}

export default Campaigns;
