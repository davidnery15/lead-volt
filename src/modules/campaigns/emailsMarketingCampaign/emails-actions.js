import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';

import { EMAILS_CAMPAIGN_LIST_QUERY } from './emails-queries';

import sessionStore, { APOLLO_CLIENT } from '../../../shared/session/session-store';
import { EMAILS_CAMPAIGN_ERROR_EVENT, EMAILS_CAMPAIGN_LIST_EVENT } from './emails-store';

export const fetchEmailsCampaign = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    response = await client.query({
      query: EMAILS_CAMPAIGN_LIST_QUERY,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchEmailsCampaign', e);

    return Flux.dispatchEvent(EMAILS_CAMPAIGN_ERROR_EVENT, e);
  }

  log('fetchEmailsCampaign', response);
  Flux.dispatchEvent(EMAILS_CAMPAIGN_LIST_EVENT, response.data);

  return response.data;
};
