import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Emails Campaign error.
 *
 * @type {string}
 */
export const EMAILS_CAMPAIGN_ERROR_EVENT = 'onEmailsCampaignError';

/**
 * Event that triggers a Emails Campaigns List event.
 *
 * @type {string}
 */
export const EMAILS_CAMPAIGN_LIST_EVENT = 'onEmailsCampaignList';

class EmailsCampaignStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(EMAILS_CAMPAIGN_ERROR_EVENT);
    this.addEvent(EMAILS_CAMPAIGN_LIST_EVENT);
  }
}

export default new EmailsCampaignStore();
