import gql from 'graphql-tag';

export const EMAILS_CAMPAIGN_LIST_QUERY = gql`
  query {
    emailMarketingCampaignsList {
      items {
        id
        name
      }
    }
  }
`;
