import moment from 'moment';
import { CAMPAIGN_TYPES, CAMPAIGN_STATUSES } from '../../shared/constants';
import { getAssignedAgents } from '../agents/agents-utils';

/**
 * Map for showing the data formatted
 *
 * @param {object} campaign the data of the campaign.
 * @returns {object} new data of the campaign with formatted values.
 */
export const campaignMap = (campaign) => {
  return {
    ...campaign,
    agents: campaign.agents.items,
    createdAtFormat: moment(campaign.createdAt).format('MM-DD-YYYY, hh:mm a'),
    agentsFormat: campaign.agents.items.length ? getAssignedAgents(campaign.agents.items) : '',
    leads: '53',
    endsFormat: moment().format('MM-DD-YYYY, hh:mm a'),
    vendor: 'Quotestorm',
    emailMarketingCampaignId: campaign.emailMarketingCampaign.id,
    emailMarketingCampaignName: campaign.emailMarketingCampaign.name,
    type: CAMPAIGN_TYPES.indexOf(campaign.type),
    status: CAMPAIGN_STATUSES.indexOf(campaign.status),
    priority: campaign.priority - 1, // convert to array index.
  };
};

export const campaignMapTable = ({
  id,
  name,
  createdAtFormat,
  type,
  priority,
  agentsFormat,
  emailMarketingCampaignName,
  leads,
  status,
}) => {
  return {
    id,
    name,
    createdAtFormat,
    type: CAMPAIGN_TYPES[type],
    priority,
    agentsFormat,
    emailMarketingCampaignName,
    leads,
    status: CAMPAIGN_STATUSES[status],
  };
};

export const campaignMapDashboardTable = (campaign) => {
  const { id, name, agentsFormat } = campaignMap(campaign);

  return {
    id,
    name,
    agentsFormat,
    endsFormat: moment().format('MM-DD-YYYY'),
  };
};
