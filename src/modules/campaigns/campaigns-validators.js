import { ValidationError } from 'shared/errors';
import { isValidString, isValidNumber } from 'shared/validators';
import { CAMPAIGN_TYPES, CAMPAIGN_STATUSES } from '../../shared/constants';

export const createAndUpdateCampaignValidator = (campaign) => {
  let errorMessages = [];

  if (!isValidString(campaign.name)) errorMessages.push('The Campaign must have a valid name');

  if (!isValidNumber(campaign.status) && CAMPAIGN_STATUSES.indexOf(campaign.status) === -1)
    errorMessages.push('The Campaign must have a valid status');

  if (!isValidNumber(campaign.type) && CAMPAIGN_TYPES.indexOf(campaign.type) === -1)
    errorMessages.push('The Campaign must have a valid type');

  if (!isValidNumber(campaign.priority))
    errorMessages.push('The campaign must have a valid priority');

  if (campaign.agents.length === 0) errorMessages.push('The campaign must have at least 1 Agent');

  if (!isValidString(campaign.emailMarketingCampaign))
    errorMessages.push('The Campaign must have a email marketing');

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};
