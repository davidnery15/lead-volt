import { CAMPAIGN_STATUSES } from '../../shared/constants';

export default {
  id: null,
  name: '',
  type: null,
  priority: null,
  agents: [],
  emailMarketingCampaignId: '',
  vendor: '',
  status: null,
};

export const FILTER_DATA = {
  dateCreatedStart: '',
  dateCreatedEnd: '',
  lastUpdatedStart: '',
  lastUpdatedEnd: '',
  assignedTo: '',
  status: '',
};

export const FILTER_CATEGORIES = {
  dateCreatedStart: {
    title: 'Date Created Start',
    placeholher: 'Select Date',
    date: {},
    type: 'date',
    identifier: 'dateCreatedStart',
  },
  dateCreatedEnd: {
    title: 'Date Created End',
    placeholher: 'Select Date',
    date: {},
    type: 'date',
    identifier: 'dateCreatedEnd',
  },
  lastUpdatedStart: {
    title: 'Last Updated Start',
    placeholher: 'Select Date',
    date: {},
    type: 'date',
    identifier: 'lastUpdatedStart',
  },
  lastUpdatedEnd: {
    title: 'Last Updated End',
    placeholher: 'Select Date',
    date: {},
    type: 'date',
    identifier: 'lastUpdatedEnd',
  },
  assignedTo: {
    title: 'Assigned to',
    placeholher: 'Select',
    identifier: 'assignedTo',
    options: [],
  },
  status: {
    title: 'Status',
    placeholher: 'Select',
    identifier: 'status',
    options: CAMPAIGN_STATUSES.map((status, index) => {
      return {
        value: index,
        label: status,
      };
    }),
  },
};

export const SORT_OPTIONS = [
  { label: 'Priority: Top First', value: 'top' },
  { label: 'Priority: Low First', value: 'low' },
  { label: 'Date: Newest First', value: 'newest' },
  { label: 'Date: Oldest First', value: 'oldest' },
];
