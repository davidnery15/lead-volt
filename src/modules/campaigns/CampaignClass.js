import View from '@cobuildlab/react-flux-state';
import { deleteCampaign } from './campaigns-actions';

/**
 * This component container is for use common methods
 * in campaigns views.
 *
 * THIS IS NOT A COMPONENT VIEW.
 */
export class CampaignClass extends View {
  /**
   * handle onYes prop in YesNoDialog.
   *
   * @memberof CampaignClass
   */
  handleOnYes = () => {
    const { currentCampaignId } = this.state;

    if (currentCampaignId !== null) {
      this.setState({ isLoading: true, isOpenDelete: false });
      deleteCampaign(currentCampaignId);
    }
  };

  /**
   *  handle onNo prop in YesNoDialog.
   *
   * @memberof CampaignClass
   */
  handleOnNo = () => {
    this.setState({ isOpenDelete: false });
  };
}

export default CampaignClass;
