import React from 'react';
import { withRouter } from 'react-router-dom';
import { fetchCampaignDetail, updateCampaign } from './campaigns-actions';
import campaignsStore, {
  CAMPAIGN_DETAIL_EVENT,
  CAMPAIGN_UPDATE_EVENT,
  CAMPAIGN_DELETE_EVENT,
  CAMPAIGN_ERROR_EVENT,
} from './campaigns-store';
import model from './campaigns-model';
import * as R from 'ramda';
import {
  Paper,
  Grid,
  Row,
  Form,
  Select as SelectBoost,
  Text,
  Progress,
  Label,
  Dropdown,
  Menu,
} from '@8base/boost';
import { CustomIconBoost } from '../../shared/components/ui/Icons';
import { campaignMap } from './campaigns-utils';
import { getAssignedAgents } from '../agents/agents-utils';
import { fetchEmailsCampaign } from './emailsMarketingCampaign/emails-actions';
import emailsStore, { EMAILS_CAMPAIGN_LIST_EVENT } from './emailsMarketingCampaign/emails-store';
import { onChangeDataMixin } from '../../shared/mixins';
import styled from 'styled-components';
import { fetchAgents } from '../agents/agents-actions';
import agentsStore, { AGENTS_LIST_EVENT } from '../agents/agents-store';
import { agentsSelectValues } from '../agents/agents-utils';
import CampaignForm from './components/CampaignForm';
import { CAMPAIGN_TYPES, PRIORITIES, CAMPAIGN_STATUSES } from '../../shared/constants';
import {
  Loader,
  TitleDetail,
  Title,
  PaperHeader,
  ButtonIcon,
  ButtonWithoutIcon,
  Margin,
  DropdownWithCheck,
  DropdownBodyOnTable,
} from 'shared/components';
import CampaignClass from './CampaignClass';
import DialogDeleteCampaign from './components/DialogDeleteCampaign';
import { onErrorMixin } from '../../shared/mixins';
import * as toast from 'shared/components/toast/Toast';
import { getActiveAgency } from '../../shared/utils/agency-utils';

const StyledProgress = styled(Progress)`
  margin: 12px 0px;

  div > div {
    height: 126px !important;
    border-radius: 5px;
  }

  div > div > div {
    border-top-left-radius: 5px !important;
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
    border-bottom-left-radius: 5px !important;
  }
`;

const CustomDropdownWrap = styled.div`
  border: 1px solid #ced7dd;
  border-radius: 4px;
  padding: 2px 10px 2px 8px;
  min-height: 36px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

class CampaignDetailsView extends CampaignClass {
  constructor(props) {
    super(props);

    this.state = {
      data: R.clone(model),
      isLoading: false,
      isLoadingForm: false,
      agents: [],
      emailsCampaign: [],
      isOpenForm: false,
      campaign: R.clone(model),
      isOpenDelete: false,
    };
    this.handleOnChangeData = onChangeDataMixin.bind(this);
    this.onError = onErrorMixin.bind(this);

    this.subscribe(campaignsStore, CAMPAIGN_DETAIL_EVENT, (data) => {
      this.setState({
        isLoading: false,
        data: campaignMap(data.campaign),
        campaign: campaignMap(data.campaign),
      });
    });
    this.subscribe(campaignsStore, CAMPAIGN_ERROR_EVENT, this.onError);
    this.subscribe(emailsStore, EMAILS_CAMPAIGN_LIST_EVENT, (data) => {
      this.setState({
        emailsCampaign: data.emailMarketingCampaignsList.items,
      });
    });
    this.subscribe(agentsStore, AGENTS_LIST_EVENT, (data) => {
      const activeAgency = getActiveAgency();

      this.setState({
        agents: data.agentsList.items.filter((agent) => {
          return agent.agency.id === activeAgency.id;
        }),
      });
    });
    this.subscribe(campaignsStore, CAMPAIGN_UPDATE_EVENT, (data) => {
      toast.success('Campaigns', 'Campaign Successfully Updated');
      this.setState({
        isLoadingForm: false,
        isOpenForm: false,
        campaign: campaignMap(data.campaignUpdate),
      });
    });
    this.subscribe(campaignsStore, CAMPAIGN_DELETE_EVENT, () => {
      this.props.history.push('/campaigns');
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    if (id) {
      this.setState({ isLoading: true, currentCampaignId: id });
      fetchCampaignDetail(id);
      fetchEmailsCampaign();
      fetchAgents();
    }
  }

  toggleOpenForm = () => {
    this.setState({ isOpenForm: !this.state.isOpenForm });
  };

  handleOnSubmitForm = () => {
    this.setState({ isLoadingForm: true });
    const data = R.clone(this.state.data);
    const { id } = this.props.match.params;
    const campaign = {
      id,
      name: data.name,
      type: CAMPAIGN_TYPES[data.type],
      priority: (data.priority + 1).toString(),
      agents: data.agents ? data.agents.map((agent) => agent.value || agent.id) : [],
      emailMarketingCampaign: data.emailMarketingCampaignId,
      status: CAMPAIGN_STATUSES[data.status],
    };

    updateCampaign(campaign, true);
  };

  render() {
    const {
      state: {
        data,
        isLoading,
        emailsCampaign,
        agents,
        isOpenForm,
        campaign,
        isOpenDelete,
        isLoadingForm,
      },
      toggleOpenForm,
      handleOnChangeData,
      handleOnSubmitForm,
      handleOnNo,
      handleOnYes,
    } = this;
    const agentsFormat = getAssignedAgents(data.agents);

    return (
      <div>
        {isLoading ? (
          <Loader />
        ) : (
          <>
            <TitleDetail text={campaign.name} />
            <div className="p24">
              <Paper>
                <PaperHeader>
                  <Grid.Layout columns="60% 40%">
                    <Grid.Box style={{ justifyContent: 'center' }}>
                      <Title>Campaign Information</Title>
                    </Grid.Box>
                    <Grid.Box>
                      <Row gap="md" justifyContent="end">
                        <ButtonIcon
                          text="Create a Report"
                          iconSvg={require('../../shared/assets/images/campaigns-gray.svg')}
                        />
                        <ButtonWithoutIcon text="Edit Campaign Info" onClick={toggleOpenForm} />
                        <Dropdown defaultOpen={false}>
                          <Dropdown.Head>
                            <CustomIconBoost name="More" color="GRAY_40" size="lg" />
                          </Dropdown.Head>
                          <DropdownBodyOnTable left="-120px">
                            <Menu>
                              <Menu.Item>
                                <Text
                                  color="DANGER"
                                  onClick={() => {
                                    this.setState({ isOpenDelete: true });
                                  }}>
                                  Delete Campaign
                                </Text>
                              </Menu.Item>
                            </Menu>
                          </DropdownBodyOnTable>
                        </Dropdown>
                      </Row>
                    </Grid.Box>
                  </Grid.Layout>
                </PaperHeader>
                <Form style={{ padding: '24px' }}>
                  <Grid.Layout columns="20% 8% 25% 47%">
                    <Grid.Box>
                      <Margin bottom="16px">
                        <Margin bottom="6px">
                          <Text weight="semibold">Marketing Campaign</Text>
                        </Margin>
                        <SelectBoost
                          stretch
                          name="emailMarketingCampaign"
                          placeholder="Email Marketing Campaign"
                          value={campaign.emailMarketingCampaignId}
                          onChange={(value) => handleOnChangeData('emailMarketing', value)}
                          options={emailsCampaign.map((email) => ({
                            label: email.name,
                            value: email.id,
                          }))}
                        />
                      </Margin>
                      <Margin bottom="6px">
                        <Text weight="semibold">Assigned to</Text>
                      </Margin>
                      <CustomDropdownWrap>
                        {agents.length > 0 && (
                          <DropdownWithCheck
                            header={agentsFormat}
                            onConfirm={(value) => {
                              const newAgents = value.map((agentId) => {
                                return agents.find((agent) => agent.id === agentId);
                              });

                              handleOnChangeData('agents', newAgents);
                            }}
                            itemList={agentsSelectValues(agents).map((agent) => {
                              return {
                                ...agent,
                                checked: !!campaign.agents.find(({ id }) => id === agent.id),
                              };
                            })}
                          />
                        )}
                      </CustomDropdownWrap>
                    </Grid.Box>
                    <Grid.Box />
                    <Grid.Box>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Type:</Text>
                          <Text>{CAMPAIGN_TYPES[campaign.type]}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Created:</Text>
                          <Text>{campaign.createdAtFormat}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Ends:</Text>
                          <Text>{campaign.endsFormat}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Vendor:</Text>
                          <Text>{campaign.vendor}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Priority:</Text>
                          <Text>{PRIORITIES[campaign.priority]}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Leads:</Text>
                          <Text>{campaign.leads}</Text>
                        </Row>
                      </Margin>
                      <Margin bottom="6px">
                        <Row>
                          <Text weight="semibold">Status:</Text>
                          <Text>{CAMPAIGN_STATUSES[campaign.status]}</Text>
                        </Row>
                      </Margin>
                    </Grid.Box>
                    <Grid.Box>
                      <Text weight="semibold">Campaign Progress</Text>
                      <Margin bottom="6px">
                        <StyledProgress
                          value={50}
                          color="PRIMARY"
                          valueText={
                            <Label kind="secondary">
                              $1,4000,000/
                              <br />
                              <span style={{ fontWeight: 600 }}>$2,500,000 Goal</span>
                            </Label>
                          }
                        />
                      </Margin>
                      <Label kind="secondary">12 days left</Label>
                    </Grid.Box>
                  </Grid.Layout>
                </Form>
              </Paper>
            </div>
            <CampaignForm
              isEdit
              isLoading={isLoadingForm}
              isOpen={isOpenForm}
              onCloseForm={toggleOpenForm}
              onChange={handleOnChangeData}
              data={data}
              onSubmit={handleOnSubmitForm}
              emailsCampaign={emailsCampaign}
              agents={agents}
            />
            <DialogDeleteCampaign
              isOpen={isOpenDelete}
              onYes={handleOnYes}
              onNo={handleOnNo}
              onClose={handleOnNo}
            />
          </>
        )}
      </div>
    );
  }
}

export default withRouter(CampaignDetailsView);
