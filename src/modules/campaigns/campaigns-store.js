import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers the creation of an Campaign
 *
 * @type {string}
 */
export const CAMPAIGN_CREATE_EVENT = 'onCreateCampaign';

/**
 * Event that triggers an Campaign Update event
 *
 * @type {string}
 */
export const CAMPAIGN_UPDATE_EVENT = 'onCampaignUpdate';

/**
 * Event that triggers a Campaign error.
 *
 * @type {string}
 */
export const CAMPAIGN_ERROR_EVENT = 'onCampaignError';

/**
 * Event that triggers a Campaigns List event.
 *
 * @type {string}
 */
export const CAMPAIGN_LIST_EVENT = 'onCampaignList';

/**
 * Event that triggers a Campaigns Delete event.
 *
 * @type {string}
 */
export const CAMPAIGN_DELETE_EVENT = 'onCampaignDelete';

/**
 * Event that triggers a Campaigns Details event.
 *
 * @type {string}
 */
export const CAMPAIGN_DETAIL_EVENT = 'onCampaignDetail';

class CampaignStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(CAMPAIGN_CREATE_EVENT);
    this.addEvent(CAMPAIGN_UPDATE_EVENT);
    this.addEvent(CAMPAIGN_ERROR_EVENT);
    this.addEvent(CAMPAIGN_LIST_EVENT);
    this.addEvent(CAMPAIGN_DELETE_EVENT);
    this.addEvent(CAMPAIGN_DETAIL_EVENT);
  }
}

export default new CampaignStore();
