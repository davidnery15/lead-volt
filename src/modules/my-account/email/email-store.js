import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Update to user signature.
 *
 * @type {string}
 */
export const USER_SIGNATURE_UPDATE_EVENT = 'onUserSignatureUpdate';

export const USER_SIGNATURE_UPDATE_ERROR_EVENT = 'onUserSignatureUpdateError';

/**
 * Event that triggers a User Signature event.
 *
 * @type {string}
 */
export const USER_SIGNATURE_EVENT = 'onUserSignatureEvent';

export const USER_SIGNATURE_ERROR_EVENT = 'onUserSignatureError';

class EmailStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(USER_SIGNATURE_UPDATE_EVENT);
    this.addEvent(USER_SIGNATURE_UPDATE_ERROR_EVENT);
    this.addEvent(USER_SIGNATURE_EVENT);
    this.addEvent(USER_SIGNATURE_ERROR_EVENT);
  }
}

export default new EmailStore();
