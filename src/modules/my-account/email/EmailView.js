import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import * as toast from 'shared/components/toast/Toast';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { useSubscription } from '@cobuildlab/react-flux-state';

import { updateUserSignature, fetchUserSignature } from './email-actions';
import emailStore, {
  USER_SIGNATURE_UPDATE_EVENT,
  USER_SIGNATURE_UPDATE_ERROR_EVENT,
  USER_SIGNATURE_EVENT,
  USER_SIGNATURE_ERROR_EVENT,
} from './email-store';

import { Card, Grid, Loader } from '@8base/boost';

import { RadioInputField } from 'shared/components';
import { PrimaryBtn } from '../../../shared/components/ui/buttons/PrimaryBtn';
import SettingsMenu from '../SettingsMenu';
import { onErrorMixinFC } from '../../../shared/mixins';

const CardMargin = styled(Card)`
  margin: 20px 20px 20px 270px;
`;

const CardBodyHeight = styled(Card.Body)`
  min-height: 335px;
`;

const CardFooterFlexEnd = styled(Card.Footer)`
  display: flex;
  justify-content: flex-end;
`;

const TitleCardHeader = styled.p`
  font-size: 16px;
  font-weight: bold;
  line-height: 28px;
`;

const TitleCardBody = styled.p`
  font-size: 13px;
  font-weight: bold;
`;

const EmailView = () => {
  const [isEnableSignature, setIsEnableSignature] = useState(false);
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const [loaderComponent, setLoaderComponent] = useState(true);
  const [loaderUpdate, setLoaderUpdate] = useState(false);

  useEffect(() => {
    fetchUserSignature();
  }, []);

  const handleChangeEnableSignature = () => {
    setIsEnableSignature(!isEnableSignature);
  };
  const handleChangeEditorState = (editorState) => {
    setEditorState(editorState);
  };

  useSubscription(emailStore, USER_SIGNATURE_UPDATE_EVENT, () => {
    setLoaderUpdate(false);
    toast.success('Signature Successfully Updated');
  });

  useSubscription(emailStore, USER_SIGNATURE_UPDATE_ERROR_EVENT, (error) => {
    setLoaderUpdate(false);
    onErrorMixinFC(error);
  });

  useSubscription(emailStore, USER_SIGNATURE_EVENT, (data) => {
    setIsEnableSignature(data.user.isEnableSignature);
    if (data.user.signature !== null) {
      const blocksFromHtml = htmlToDraft(data.user.signature);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
      setEditorState(EditorState.createWithContent(contentState));
    }
    setLoaderComponent(false);
  });

  useSubscription(emailStore, USER_SIGNATURE_ERROR_EVENT, (error) => {
    console.log(error);
  });

  const handleUpdateSignature = () => {
    setLoaderUpdate(true);
    const htmlToString = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    const data = {
      signature: htmlToString,
      isEnableSignature,
    };
    updateUserSignature(data);
  };

  return (
    <>
      <SettingsMenu />
      {loaderComponent ? (
        <Loader size="lg" style={{ left: '55%', top: '20vh' }} />
      ) : (
        <CardMargin>
          <Card.Header>
            <TitleCardHeader>Email</TitleCardHeader>
          </Card.Header>
          <CardBodyHeight>
            <Grid.Layout columns="100%">
              <Grid.Box>
                <TitleCardBody>Email Signature</TitleCardBody>
              </Grid.Box>
            </Grid.Layout>
            <Grid.Layout inline gap="xxl" columns="22% 74%">
              <Grid.Box>
                <RadioInputField
                  label="Enable Email Signature"
                  checked={isEnableSignature}
                  onChange={handleChangeEnableSignature}
                />
              </Grid.Box>
              <Grid.Box>
                <Editor editorState={editorState} onEditorStateChange={handleChangeEditorState} />
              </Grid.Box>
            </Grid.Layout>
          </CardBodyHeight>
          <CardFooterFlexEnd>
            <PrimaryBtn onClick={handleUpdateSignature} loading={loaderUpdate}>
              Save Changes
            </PrimaryBtn>
          </CardFooterFlexEnd>
        </CardMargin>
      )}
    </>
  );
};

export { EmailView };
