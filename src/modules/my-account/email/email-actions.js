import Flux from '@cobuildlab/flux-state';
import { error, log } from '@cobuildlab/pure-logger';

import { UPDATE_USER } from '../../auth/queries';
import { USER_SIGNATURE_QUERY } from './email-queries';

import sessionStore, {
  APOLLO_CLIENT,
  NEW_SESSION_EVENT,
} from '../../../shared/session/session-store';
import {
  USER_SIGNATURE_UPDATE_EVENT,
  USER_SIGNATURE_UPDATE_ERROR_EVENT,
  USER_SIGNATURE_EVENT,
  USER_SIGNATURE_ERROR_EVENT,
} from './email-store';

import { updateUserSignatureValidator } from './email-validators';

/**
 * Update user signature
 *
 * @param data
 * @param {object} fields - the fields to update the user signature
 */
export const updateUserSignature = async (data) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  data.id = user.id;
  let response;

  try {
    updateUserSignatureValidator(data);
  } catch (e) {
    error('updateUserSignature', e);
    console.log(e);
    return Flux.dispatchEvent(USER_SIGNATURE_UPDATE_ERROR_EVENT, e);
  }

  try {
    response = await client.mutate({
      mutation: UPDATE_USER,
      variables: { data },
    });
  } catch (e) {
    error('updateUserSignature', e);
    return Flux.dispatchEvent(USER_SIGNATURE_UPDATE_ERROR_EVENT, e);
  }

  log('updateUserSignature', response);
  Flux.dispatchEvent(USER_SIGNATURE_UPDATE_EVENT, response.data);

  return response.data;
};

/**
 * Fetch user signature
 *
 * @returns {object} the response data
 */
export const fetchUserSignature = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  let response;

  try {
    response = await client.query({
      query: USER_SIGNATURE_QUERY,
      fetchPolicy: 'network-only',
      variables: { id: user.id },
    });
  } catch (e) {
    error('fetchUserSignature', e);

    return Flux.dispatchEvent(USER_SIGNATURE_ERROR_EVENT, e);
  }

  log('fetchUserSignature', response);
  Flux.dispatchEvent(USER_SIGNATURE_EVENT, response.data);

  return response.data;
};
