import { ValidationError } from 'shared/errors';

export const updateUserSignatureValidator = (data) => {
  let errorMessages = [];

  if (data.signature.length > 2000) {
    errorMessages.push('Very long Signature');
  }

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};
