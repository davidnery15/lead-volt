import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Paper, Table } from '@8base/boost';

/**
 * Show user roles.
 *
 * @param  {object} roleRelationArray - User relationships with roles.
 * @param  {object} titleHeader - Check if it is company or agency.
 * @param  {object} userRole - Role of the user.
 */

const RoleTable = ({ roleRelationArray, titleHeader, userRole }) => {
  return (
    <>
      {roleRelationArray.length > 0 ? (
        <Grid.Layout inline gap="md" columns="50%">
          <Grid.Box>
            <Paper>
              <Table>
                <Table.Header columns="repeat(2, 1fr)">
                  <Table.HeaderCell>Role</Table.HeaderCell>
                  <Table.HeaderCell>{titleHeader}</Table.HeaderCell>
                </Table.Header>
                <Table.Body>
                  {roleRelationArray.map((data) => {
                    return (
                      <Table.BodyRow columns="repeat(2, 1fr)" key={data.id}>
                        <Table.BodyCell>{userRole}</Table.BodyCell>
                        <Table.BodyCell>
                          {`${titleHeader === 'Company' ? data.company.name : data.agency.name}`}
                        </Table.BodyCell>
                      </Table.BodyRow>
                    );
                  })}
                </Table.Body>
              </Table>
            </Paper>
          </Grid.Box>
        </Grid.Layout>
      ) : null}
    </>
  );
};

RoleTable.propTypes = {
  roleRelationArray: PropTypes.array.isRequired,
  titleHeader: PropTypes.string.isRequired,
  userRole: PropTypes.string.isRequired,
};

export { RoleTable };
