import React, { useState, useEffect } from 'react';
import { updateUser } from '../../auth/auth.actions';
import sessionStore, {
  USER_ERROR_EVENT,
  NEW_SESSION_EVENT,
  USER_UPDATE_EVENT,
} from '../../../shared/session/session-store';
import { useSubscription } from '@cobuildlab/react-flux-state';
import styled from 'styled-components';
import { onErrorMixinFC } from '../../../shared/mixins';
import {
  Card,
  Grid,
  Avatar,
  Form,
  InputField,
  SelectField,
  Loader,
  Dialog,
  Paragraph,
  Button,
} from '@8base/boost';
import { FileInput } from '@8base-react/file-input';
import { TIMEZONE_OPTIONS } from './general-model';
import { RoleTable } from './components/RoleTable';
import { PrimaryBtn } from '../../../shared/components/ui/buttons/PrimaryBtn';
import SettingsMenu from '../SettingsMenu';

const CardMargin = styled(Card)`
  margin: 20px 20px 20px 270px;
`;

const CardFooterFlexEnd = styled(Card.Footer)`
  display: flex;
  justify-content: flex-end;
`;

const TitleCard = styled.p`
  font-size: 16px;
  font-weight: bold;
  line-height: 28px;
`;

const AvatarGridBox = styled(Grid.Box)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const AvatarText = styled.button`
  margin-top: 20px;
  height: 20px;
  width: 100%;
  color: #3db4aa;
  font-size: 13px;
  line-height: 18px;
  text-align: center;
  background: transparent;
  outline: none;
  cursor: pointer;
`;

const GeneralView = () => {
  const [user, setUser] = useState('');

  const [id, setId] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [timezone, setTimezone] = useState('');
  const [cellPhone, setCellPhone] = useState('');
  const [workPhone, setWorkPhone] = useState('');
  const [workPhoneExt, setWorkPhoneExt] = useState('');
  const [avatar, setAvatar] = useState(null);

  const [downloadUrl, setDownloadUrl] = useState('');

  const [agentRelation, setAgentRelation] = useState([]);
  const [adminRelation, setAdminRelation] = useState([]);
  const [managerRelation, setManagerRelation] = useState([]);

  const [isOpenUpdate, setIsOpenUpdate] = useState(false);
  const [loaderUpdate, setLoaderUpdate] = useState(false);

  const handleChangeFirstName = (e) => setFirstName(e.target.value);
  const handleChangeLastName = (e) => setLastName(e.target.value);
  const handleChangeEmail = (e) => setEmail(e.target.value);
  const handleChangeCellPhone = (e) => setCellPhone(e.target.value);
  const handleChangeWorkPhone = (e) => setWorkPhone(e.target.value);
  const handleChangeWorkPhoneExtra = (e) => setWorkPhoneExt(e.target.value);

  const handleOpenUpdate = () => setIsOpenUpdate(true);
  const handleCloseUpdate = () => setIsOpenUpdate(false);

  useEffect(() => {
    const user = sessionStore.getState(NEW_SESSION_EVENT).user;
    setId(user.id);
    setFirstName(user.firstName);
    setLastName(user.lastName);
    setEmail(user.email);
    setTimezone(user.timezone);
    setCellPhone(user.cellPhone);
    setWorkPhone(user.workPhone);
    setWorkPhoneExt(user.workPhoneExt);
    setAgentRelation(user.userAgentRelation.items);
    setAdminRelation(user.userAdminRelation.items);
    setManagerRelation(user.userManagerRelation.items);
    setDownloadUrl(user.avatar !== null ? user.avatar.downloadUrl : null);
    setUser(user);
  }, []);

  useSubscription(sessionStore, USER_UPDATE_EVENT, (data) => {
    // console.log(data);
    setLoaderUpdate(false);
    handleCloseUpdate();
    window.location.reload();
  });

  useSubscription(sessionStore, USER_ERROR_EVENT, (error) => {
    setLoaderUpdate(false);
    handleCloseUpdate();
    onErrorMixinFC(error);
  });

  const handleSubmit = () => {
    setLoaderUpdate(true);
    const userData = {
      id,
      firstName,
      lastName,
      email,
      timezone,
      cellPhone,
      workPhone,
      workPhoneExt,
      avatar,
    };
    updateUser(userData);
  };

  return (
    <>
      <SettingsMenu />
      {user === '' ? (
        <Loader size="lg" style={{ left: '55%', top: '20vh' }} />
      ) : (
        <CardMargin>
          <Card.Header>
            <TitleCard>My Account / Settings</TitleCard>
          </Card.Header>
          <Card.Body>
            <Grid.Layout inline gap="xxl" columns="24% 74%">
              <AvatarGridBox>
                <Avatar
                  size="lg"
                  src={
                    downloadUrl !== null
                      ? downloadUrl
                      : 'https://randomuser.me/api/portraits/women/17.jpg'
                  }
                  style={{
                    width: 200,
                    height: 200,
                    border: '3px solid #3db4aa',
                  }}
                />
                <FileInput
                  onChange={(picture, file, ...rest) => {
                    const reader = new FileReader();
                    reader.addEventListener(
                      'load',
                      () => {
                        setDownloadUrl(reader.result);
                      },
                      false,
                    );
                    reader.readAsDataURL(file);
                    setAvatar(picture);
                  }}
                  value={'input.value'}
                  maxFiles={1}>
                  {({ pick }) => {
                    return <AvatarText onClick={pick}>Change Profile Image</AvatarText>;
                  }}
                </FileInput>
              </AvatarGridBox>
              <Grid.Box>
                <Form>
                  <Form.Section>
                    <Form.SectionTitle>Personal Information</Form.SectionTitle>
                    <Form.SectionBody>
                      <Grid.Layout inline gap="md" columns="47% 48%">
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            label="First Name"
                            onChange={handleChangeFirstName}
                            input={{
                              name: 'name',
                              type: 'text',
                              value: firstName,
                            }}
                          />
                        </Grid.Box>
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            label="Last name"
                            onChange={handleChangeLastName}
                            input={{
                              name: 'lastName',
                              type: 'text',
                              value: lastName,
                            }}
                          />
                        </Grid.Box>
                      </Grid.Layout>
                      <Grid.Layout inline gap="md" columns="47% 48%">
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            disabled={true}
                            label="Email Address"
                            onChange={handleChangeEmail}
                            input={{
                              name: 'email',
                              type: 'text',
                              value: email,
                            }}
                          />
                        </Grid.Box>
                        <Grid.Box>
                          <SelectField
                            stretch={true}
                            label="Location / Timezone"
                            input={{
                              name: 'timezone',
                              value: timezone,
                              onChange: (value) => setTimezone(value),
                            }}
                            options={TIMEZONE_OPTIONS}
                          />
                        </Grid.Box>
                      </Grid.Layout>
                      <Grid.Layout inline gap="md" columns="31% 31% 31%">
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            label="Cell Phone"
                            onChange={handleChangeCellPhone}
                            input={{
                              name: 'cellPhone',
                              type: 'number',
                              value: cellPhone,
                            }}
                          />
                        </Grid.Box>
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            label="Work Phone"
                            onChange={handleChangeWorkPhone}
                            input={{
                              name: 'workPhone',
                              type: 'number',
                              value: workPhone,
                            }}
                          />
                        </Grid.Box>
                        <Grid.Box>
                          <InputField
                            stretch={true}
                            label="Work Phone Extra"
                            onChange={handleChangeWorkPhoneExtra}
                            input={{
                              name: 'workPhoneExt',
                              type: 'number',
                              value: workPhoneExt,
                            }}
                          />
                        </Grid.Box>
                      </Grid.Layout>
                      <RoleTable
                        roleRelationArray={agentRelation}
                        titleHeader="Agency"
                        userRole="Agent"
                      />
                      <RoleTable
                        roleRelationArray={adminRelation}
                        titleHeader="Company"
                        userRole="Admin"
                      />
                      <RoleTable
                        roleRelationArray={managerRelation}
                        titleHeader="Agency"
                        userRole="Manager"
                      />
                    </Form.SectionBody>
                  </Form.Section>
                </Form>
              </Grid.Box>
            </Grid.Layout>
          </Card.Body>
          <CardFooterFlexEnd>
            <PrimaryBtn onClick={handleOpenUpdate}>Save Changes</PrimaryBtn>
          </CardFooterFlexEnd>
          <Dialog size="sm" isOpen={isOpenUpdate} data-e2e-id="default-dialog">
            <Dialog.Header title="Update Changes" onClose={handleCloseUpdate} />
            <Dialog.Body>
              <Paragraph>Are you sure update the changes?</Paragraph>
            </Dialog.Body>
            <Dialog.Footer>
              <Button color="neutral" variant="outlined" onClick={handleCloseUpdate}>
                Cancel
              </Button>
              <Button loading={loaderUpdate} onClick={handleSubmit}>
                Update
              </Button>
            </Dialog.Footer>
          </Dialog>
        </CardMargin>
      )}
    </>
  );
};

export { GeneralView };
