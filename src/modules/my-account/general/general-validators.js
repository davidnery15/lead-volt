import { ValidationError } from 'shared/errors';
import { isValidString, isValidNumber, isValidEmail } from 'shared/validators';

/**
 * Validate the user data.
 *
 * @param {object} user info.
 * @returns {Array} If there is an error it adds to the array.
 */

export const updateUserValidator = (userData) => {
  let errorMessages = [];

  if (!isValidString(userData.firstName)) {
    errorMessages.push('The User must have a valid First Name');
  }

  if (!isValidString(userData.lastName)) {
    errorMessages.push('The User must have a valid Last Name');
  }

  if (!isValidEmail(userData.email)) {
    errorMessages.push('The User must have a valid Email');
  }

  if (!isValidString(userData.timezone)) {
    errorMessages.push('The User must have a valid Timezone');
  }

  if (!isValidNumber(userData.cellPhone)) {
    errorMessages.push('The User must have a valid cellphone');
  }

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};
