import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import styled from 'styled-components';

export const routes = [
  {
    name: 'General',
    path: '/settings/general',
  },
  {
    name: 'Email',
    path: '/settings/email',
  },
  {
    name: 'Status Mapping',
    path: '/settings/status-mapping',
  },
  {
    name: 'Billing',
    path: '/settings/billing',
  },
];

const MenuContainer = styled.div`
  height: 100vh;
  width: 250px;
  background-color: #F4F5F6;
  display: flex;
  flex-direction: column;
  padding: 24px 0 0 0;
  box-shadow: inset 0 3px 3px 0 rgba(50,50,93,.14);
  position: absolute;
  top: 88px
  left: 56px;
`;

const MenuTitle = styled.p`
  margin: 0 0 10px 24px;
  color: #323c47;
  font-size: 13px;
  font-weight: bold;
  line-height: 20px;
`;

const List = styled.ul`
  width: 100%;
  list-style: none;
`;

const Li = styled.li`
  width: 99%;
  height: 32px;

  &:first-child {
    background-color: #fff;
    border-left: 2.5px solid #3db4aa;
    a {
      color: #3db4aa;
    }
  }

  &:hover {
    background-color: #fff;
    border-left: 2.5px solid #3db4aa;
  }
`;

const StyledLink = styled(Link)`
  color: #323c47;
  text-decoration: none;
  height: 100%;
  padding-left: 24px;
  display: flex;
  align-items: center;

  &:hover {
    color: #3db4aa;
  }
`;

const SettingsMenu = ({ history }) => {
  const [pathnameActive, setPathnameActive] = useState('/settings/general');

  history.listen((location, action) => {
    const { pathname } = location;
    setPathnameActive(pathname);
  });

  return (
    <MenuContainer>
      <MenuTitle>Settings</MenuTitle>
      <List>
        {routes.map((route) => {
          const isActive = pathnameActive.startsWith(route.path);

          return (
            <Li key={route.path} active={isActive}>
              <StyledLink to={route.path} active={isActive}>
                {route.name}
              </StyledLink>
            </Li>
          );
        })}
      </List>
    </MenuContainer>
  );
};

SettingsMenu.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(SettingsMenu);
