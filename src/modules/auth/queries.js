import gql from 'graphql-tag';

export const UserFragment = gql`
  fragment UserFragment on User {
    email
    firstName
    id
    lastName
    cellPhone
    workPhone
    workPhoneExt
    timezone
    avatar {
      id
      downloadUrl
    }
  }
`;

export const AgencyFragment = gql`
  fragment FragmentAgency on Agency {
    id
    name
    company {
      id
      name
    }
  }
`;

export const CompanyFragment = gql`
  fragment CompanyFragment on Company {
    id
    name
    agencyCompanyRelation {
      items {
        id
        name
      }
    }
  }
`;

export const AgencyInvitationFragment = gql`
  fragment AgencyInvitationFragment on AgencyInvitation {
    id
    createdBy {
      ...UserFragment
    }
    agency {
      ...FragmentAgency
    }
    email
    type
    status
  }
  ${UserFragment}
  ${AgencyFragment}
`;

export const CompanyInvitationFragment = gql`
  fragment CompanyInvitationFragment on CompanyInvitation {
    id
    createdBy {
      ...UserFragment
    }
    company {
      ...CompanyFragment
    }
    email
    status
  }
  ${UserFragment}
  ${CompanyFragment}
`;

export const MetaFragment = gql`
  fragment MetaFragment on Meta {
    id
    name
    value
  }
`;

export const SESSION_QUERY = gql`
  query(
    $id: ID
    $agencyInvitationFilter: AgencyInvitationFilter!
    $companyInvitationFilter: CompanyInvitationFilter!
  ) {
    user(id: $id) {
      ...UserFragment
      metaUserRelation {
        count
        items {
          ...MetaFragment
        }
      }
      userAgentRelation {
        count
        items {
          id
          agency {
            ...FragmentAgency
          }
        }
      }
      userAdminRelation {
        count
        items {
          id
          company {
            ...CompanyFragment
          }
        }
      }
      userManagerRelation {
        count
        items {
          id
          agency {
            ...FragmentAgency
          }
        }
      }
    }
    agencyInvitationsList(filter: $agencyInvitationFilter) {
      count
      items {
        ...AgencyInvitationFragment
      }
    }
    companyInvitationsList(filter: $companyInvitationFilter) {
      count
      items {
        ...CompanyInvitationFragment
      }
    }
  }
  ${UserFragment}
  ${AgencyFragment}
  ${CompanyFragment}
  ${AgencyInvitationFragment}
  ${CompanyInvitationFragment}
  ${MetaFragment}
`;

export const USER_QUERY = gql`
  query User($id: ID) {
    user(id: $id) {
      ...UserFragment
    }
  }
  ${UserFragment}
`;

export const UPDATE_USER = gql`
  mutation($data: UserUpdateInput!) {
    userUpdate(data: $data) {
      id
    }
  }
`;

export const META_QUERY = gql`
  query Metas($userId: ID) {
    metasList(filter: { user: { id: { equals: $userId } } }) {
      count
      items {
        ...MetaFragment
      }
    }
  }
  ${MetaFragment}
`;

export const CREATE_META_MUTATION = gql`
  mutation CreateMeta($data: MetaCreateInput!) {
    metaCreate(data: $data) {
      ...MetaFragment
    }
  }
  ${MetaFragment}
`;

export const UPDATE_META_MUTATION = gql`
  mutation UpdateMeta($data: MetaUpdateInput!) {
    metaUpdate(data: $data) {
      ...MetaFragment
    }
  }
  ${MetaFragment}
`;
