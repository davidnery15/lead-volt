import React from 'react';
import PropTypes from 'prop-types';
import { withAuth } from '@8base/app-provider';
import MainLoader from 'shared/components/ui/MainLoader';

class AuthContainerClass extends React.Component {
  async componentDidMount() {
    const { auth } = this.props;

    await auth.authClient.authorize();
  }

  render() {
    return <MainLoader />;
  }
}

AuthContainerClass.propTypes = {
  auth: PropTypes.object.isRequired,
};

const AuthContainer = withAuth(AuthContainerClass);

export { AuthContainer };
