import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';
import {
  USER_QUERY,
  SESSION_QUERY,
  UPDATE_USER,
  META_QUERY,
  CREATE_META_MUTATION,
  UPDATE_META_MUTATION,
} from './queries';
import sessionStore, {
  SESSION_ERROR,
  APOLLO_CLIENT,
  NEW_SESSION_EVENT,
  USER_UPDATE_EVENT,
  USER_ERROR_EVENT,
} from '../../shared/session/session-store';
import { META_ONBOARDING_STEP_NAME } from '../../shared/constants/meta';
import { sanitize8BaseDocumentUpdate } from '../../shared/utils';
import { updateUserValidator } from '../my-account/general/general-validators';
import * as R from 'ramda';

/**
 * Fetches a User Profile by Id or the logged User if no Id provided.
 *
 * @param id
 * @returns {Promise<void>}
 */
export const fetchUser = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  try {
    if (id === 0) {
      response = await client.query({ query: USER_QUERY, fetchPolicy: 'network-only' });
    } else {
      response = await client.query({
        query: USER_QUERY,
        fetchPolicy: 'network-only',
        variables: { id },
      });
    }
  } catch (e) {
    error('fetchUser', e);
    throw e;
  }

  log('fetchUser', response.data.user);

  return response.data.user;
};

/**
 * Update User Profile data.
 *
 * @returns {Promise<void>}
 * @param userData
 */
export const updateUser = async (userData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (userData.avatar !== null) {
    sanitize8BaseDocumentUpdate(userData, 'avatar');
  }

  try {
    updateUserValidator(userData);
  } catch (e) {
    error('updateUser', e);
    return Flux.dispatchEvent(USER_ERROR_EVENT, e);
  }

  try {
    response = await client.mutate({
      mutation: UPDATE_USER,
      variables: { data: userData },
    });
  } catch (e) {
    error('updateUser', e);
    return Flux.dispatchEvent(USER_ERROR_EVENT, e);
  }

  log('updateUser', response);
  Flux.dispatchEvent(USER_UPDATE_EVENT, response.data);

  return response.data;
};

/**
 * Fetches the logged User Profile.
 *
 * @returns {Promise<void>}
 */
export const fetchSession = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let user = null;

  try {
    user = await fetchUser();
  } catch (e) {
    error('fetchSession', e);
    Flux.dispatchEvent(SESSION_ERROR, e);

    throw e;
  }

  let response;
  try {
    response = await client.query({
      query: SESSION_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        id: user.id,
        agencyInvitationFilter: { email: { equals: user.email } },
        companyInvitationFilter: { email: { equals: user.email } },
      },
    });
  } catch (e) {
    error('fetchSession', e);

    return Flux.dispatchEvent(SESSION_ERROR, e);
  }

  const session = R.clone(response.data);

  log('fetchSession', session);
  Flux.dispatchEvent(NEW_SESSION_EVENT, session);

  return session;
};

/**
 * Fetches the current user meta information.
 *
 * @returns {Promise<void>}
 */
export const fetchMeta = async (): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  let response;
  try {
    response = await client.query({
      query: META_QUERY,
      fetchPolicy: 'network-only',
      variables: { userId: user.id },
    });
  } catch (e) {
    Flux.dispatchEvent(SESSION_ERROR, e);
  }
  console.log('fetchMeta', response.data);
  return response.data.metasList.items;
};

/**
 * Creates a meta value in the API for the current User.
 *
 * @param name the name of the meta
 * @param value the valie of the meta
 * @returns {Promise<void>}
 */
export const createMeta = async (name: string, value: string): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const data = { name, value, user: { connect: { id: user.id } } };
  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_META_MUTATION,
      variables: { data },
    });
  } catch (e) {
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  log(`createMeta:`, response.data.metaCreate);
  return response.data.metaCreate;
};

/**
 * Updates a meta value in the API for the current User.
 *
 * @param name the name of the meta
 * @param value the value of the meta
 * @returns {Promise<void>}
 */
export const updateMeta = async (name: string, value: string): void => {
  const metas = await fetchMeta();
  let metaToBeUpdated = metas.find((meta) => meta.name === name);
  console.log('updateMeta:', name, value, metas, metaToBeUpdated);

  if (!metaToBeUpdated) {
    log(`updateMeta: Non existing meta value, creating...`);
    metaToBeUpdated = await createMeta(name, value);
  }

  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: metaToBeUpdated.id, value: String(value) };
  let response;

  try {
    response = await client.mutate({
      mutation: UPDATE_META_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('updateMeta', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  console.log('updateMeta', response.data);
  return response.data.metaUpdate;
};

/**
 * Creates The first data necessary for the User when it starts the session.
 *
 * @param step the step in which the User should begin
 * @returns {Promise<void>}
 */
export const createDefaultMeta = async (step = 0): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const data = {
    id: user.id,
    metaUserRelation: {
      create: [
        {
          name: META_ONBOARDING_STEP_NAME,
          value: String(step),
        },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({ mutation: UPDATE_USER, variables: { data } });
  } catch (e) {
    error('createDefaultMeta', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }

  log('createDefaultMeta', response);
  return response.data;
};
