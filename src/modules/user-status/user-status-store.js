import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a User Status error.
 *
 * @type {string}
 */
export const USER_STATUS_ERROR_EVENT = 'onUserStatusError';

/**
 * Event that triggers a User Status create.
 *
 * @type {string}
 */
export const USER_STATUS_CREATE_EVENT = 'onUserStatusCreate';

/**
 * Event that triggers a User Status List.
 *
 * @type {string}
 */
export const USER_STATUS_LIST_EVENT = 'onUserStatusList';

class UserStatusStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(USER_STATUS_ERROR_EVENT);
    this.addEvent(USER_STATUS_CREATE_EVENT);
    this.addEvent(USER_STATUS_LIST_EVENT);
  }
}

export default new UserStatusStore();
