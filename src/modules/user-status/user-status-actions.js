import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';

import { USER_STATUS_CREATE_MUTATION, USER_STATUS_LIST_QUERY } from './user-status-queries';

import sessionStore, { APOLLO_CLIENT } from '../../shared/session/session-store';
import {
  USER_STATUS_CREATE_EVENT,
  USER_STATUS_ERROR_EVENT,
  USER_STATUS_LIST_EVENT,
} from './user-status-store';
import { sanitize8BaseEmptyFields, sanitize8BaseReference } from '../../shared/utils';

/**
 * create filters for user statuses query.
 *
 * @param {object} filterData createdAt and user filters.
 * @returns {object} graphql filter object.
 */
const createFilter = (filterData) => {
  return {
    ...(filterData && {
      AND: [
        ...(filterData.user && filterData.user !== ''
          ? [
            {
              user: { id: { equals: filterData.user } },
            },
          ]
          : []),
      ],
    }),
  };
};

/**
 * Creates a user status
 *
 * @param {object} userStatus data from dialog profile
 * @returns {object} the response data
 */
export const createUserStatus = async (userStatus) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  sanitize8BaseEmptyFields(userStatus);
  sanitize8BaseReference(userStatus, 'user');

  try {
    response = await client.mutate({
      mutation: USER_STATUS_CREATE_MUTATION,
      variables: { data: userStatus },
    });
  } catch (e) {
    error('createUserStatus', e);

    return Flux.dispatchEvent(USER_STATUS_ERROR_EVENT, e);
  }

  log('createUserStatus', response);
  Flux.dispatchEvent(USER_STATUS_CREATE_EVENT, response.data);

  return response.data;
};

/**
 * Action for fetch the user statuses
 *
 * @param {object} data filter data filters.
 * @returns {object} the response data
 */
export const fetchUserStatuses = async (data = {}) => {
  const { filterData } = data;
  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = createFilter(filterData);
  let response;

  if (client) {
    const variables = {
      ...(filter && { filter }),
    };

    try {
      response = await client.query({
        query: USER_STATUS_LIST_QUERY,
        fetchPolicy: 'network-only',
        variables,
      });
    } catch (e) {
      error('fetchUserStatuses', e);

      return Flux.dispatchEvent(USER_STATUS_ERROR_EVENT, e);
    }

    log('fetchUserStatuses', response);
    Flux.dispatchEvent(USER_STATUS_LIST_EVENT, response.data);

    return response.data;
  }
};
