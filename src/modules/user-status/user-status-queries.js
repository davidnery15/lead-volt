import gql from 'graphql-tag';

export const USER_STATUS_CREATE_MUTATION = gql`
  mutation UserStatusCreateMutation($data: UserStatusCreateInput!) {
    userStatusCreate(data: $data) {
      status
    }
  }
`;

export const USER_STATUS_LIST_QUERY = gql`
  query UserStatusQuery($filter: UserStatusFilter!) {
    userStatusesList(filter: $filter) {
      items {
        status
        user {
          id
          firstName
          lastName
        }
        createdAt
      }
    }
  }
`;
