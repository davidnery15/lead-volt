import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/session/session-store';
import {
  AGENCY_INVITATION_ERROR_EVENT,
  DECLINE_AGENCY_INVITATION_EVENT,
  ACCEPT_AGENCY_INVITATION_EVENT,
} from './agency-invitation-store';
import {
  UPDATE_AGENCY_INVITATION_MUTATION,
  CREATE_AGENT_MUTATION,
  CREATE_MANAGER_MUTATION,
} from './agency-invitation-queries';
import {
  canAcceptAgencyInvitation,
  canDeclineAgencyInvitation,
} from './agency-invitation-permissions';
import {
  AGENCY_INVITATION_REJECTED,
  AGENCY_INVITATION_APPROVED,
  AGENCY_AGENT_INVITATION,
  AGENCY_MANAGER_INVITATION,
} from '../../shared/constants/agency-invitations';
import Flux from '@cobuildlab/flux-state';
import { log, error } from '@cobuildlab/pure-logger';

/**
 * Creates an agency Agent.
 *
 * @param  {string}  agencyId - The agencyId to create the agent.
 * @param  {string}  userId  - The userId to create the agent.
 * @returns {Promise}  The created agent data.
 */
export const createAgent = async (agencyId, userId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {
    agency: { connect: { id: agencyId } },
    user: { connect: { id: userId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_AGENT_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAgent', e);
    return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
  }
  log('createAgent', response);

  return response;
};

/**
 * Creates an agency Manager.
 *
 * @param  {string}  agencyId - The agencyId to create the manager.
 * @param  {string}  userId  - The userId to create the manager.
 * @returns {Promise}  The created manager data.
 */
export const createManager = async (agencyId, userId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {
    agency: { connect: { id: agencyId } },
    user: { connect: { id: userId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_MANAGER_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createManager', e);
    return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
  }
  log('createManager', response);

  return response;
};

/**
 * Accept agency invitation.
 *
 * @param  {object}  invitation - The invitation data.
 * @returns {Promise} The updated invitation.
 */
export const acceptAgencyInvitation = async (invitation) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (!canAcceptAgencyInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      AGENCY_INVITATION_ERROR_EVENT,
      new Error(`Permission Denied. Can't accept this agency invitation.`),
    );
  }

  const {
    id,
    type,
    agency: { id: agencyId },
  } = invitation;
  const data = {
    id,
    status: AGENCY_INVITATION_APPROVED,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_AGENCY_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('acceptAgencyInvitation', e);
    return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
  }

  if (type === AGENCY_AGENT_INVITATION) {
    try {
      await createAgent(agencyId, user.id);
    } catch (e) {
      return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
    }
  } else if (type === AGENCY_MANAGER_INVITATION) {
    try {
      await createManager(agencyId, user.id);
    } catch (e) {
      return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
    }
  }

  log('acceptAgencyInvitation', response);
  Flux.dispatchEvent(ACCEPT_AGENCY_INVITATION_EVENT, response.data);

  return response.data;
};

/**
 * Decline agency invitation.
 *
 * @param  {object}  invitation - The invitation data.
 * @returns {Promise} The updated invitation.
 */
export const declineAgencyInvitation = async (invitation) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (!canDeclineAgencyInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      AGENCY_INVITATION_ERROR_EVENT,
      new Error(`Permission Denied. Can't decline this agency invitation.`),
    );
  }

  const { id } = invitation;
  const data = {
    id,
    status: AGENCY_INVITATION_REJECTED,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_AGENCY_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('declineAgencyInvitation', e);
    return Flux.dispatchEvent(AGENCY_INVITATION_ERROR_EVENT, e);
  }

  log('declineAgencyInvitation', response);
  Flux.dispatchEvent(DECLINE_AGENCY_INVITATION_EVENT, response.data);

  return response.data;
};

/**
 * Create user manager in one or more agencies
 *
 * @param {Array} agencies
 * @param {string} userId
 * @returns {promise}
 */
export const createManagerAgencies = (agencies, userId) => {
  const requests = [];

  agencies.forEach((agency) => {
    requests.push(createManager(agency.id, userId));
  });

  return Promise.all(requests);
};
