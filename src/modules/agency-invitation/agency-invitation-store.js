import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Agency invitation error.
 *
 * @type {string}
 */
export const AGENCY_INVITATION_ERROR_EVENT = 'onAgencyInvitationError';

/**
 * Event that triggers a decline agency invitation.
 *
 * @type {string}
 */
export const DECLINE_AGENCY_INVITATION_EVENT = 'onDeclineAgencyInvitation';

/**
 * Event that triggers a acccept agency invitation.
 *
 * @type {string}
 */
export const ACCEPT_AGENCY_INVITATION_EVENT = 'onAcceptAgencyInvitation';

class AgencyInvitationStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(AGENCY_INVITATION_ERROR_EVENT);
    this.addEvent(DECLINE_AGENCY_INVITATION_EVENT);
    this.addEvent(ACCEPT_AGENCY_INVITATION_EVENT);
  }
}

export default new AgencyInvitationStore();
