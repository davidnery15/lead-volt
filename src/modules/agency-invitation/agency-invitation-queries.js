import gql from 'graphql-tag';

export const UPDATE_AGENCY_INVITATION_MUTATION = gql`
  mutation UpdateAgencyInvitation($data: AgencyInvitationUpdateInput!) {
    agencyInvitationUpdate(data: $data) {
      id
      email
    }
  }
`;

export const CREATE_AGENT_MUTATION = gql`
  mutation CreateAgent($data: AgentCreateInput!) {
    agentCreate(data: $data) {
      id
    }
  }
`;

export const CREATE_MANAGER_MUTATION = gql`
  mutation CreateManager($data: ManagerCreateInput!) {
    managerCreate(data: $data) {
      id
    }
  }
`;
