import { AGENCY_INVITATION_PENDING } from '../../shared/constants/agency-invitations';

/**
 * Can accept invitation permision.
 *
 * @param  {object} user - The logged user.
 * @param  {object} invitation - The invitation to check.
 * @returns {boolean} If the user can accept the invitation.
 */
export const canAcceptAgencyInvitation = (user, invitation) => {
  const { email, status } = invitation;
  const { email: userEmail } = user;

  if (status !== AGENCY_INVITATION_PENDING) return false;

  return email.toLowerCase() === userEmail.toLowerCase();
};

/**
 * Can decline invitation permision.
 *
 * @param  {object} user - The logged user.
 * @param  {object} invitation - The invitation to check.
 * @returns {boolean} If the user can decline the invitation.
 */
export const canDeclineAgencyInvitation = (user, invitation) => {
  const { email, status } = invitation;
  const { email: userEmail } = user;

  if (status !== AGENCY_INVITATION_PENDING) return false;

  return email.toLowerCase() === userEmail.toLowerCase();
};
