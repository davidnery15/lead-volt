import React from 'react';
import { AppProvider } from '@8base/react-sdk';
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import { BoostProvider, AsyncContent, createTheme } from '@8base/boost';
import { ProtectedRoute, Layout, Session } from './shared/components';
import AuthClient from 'shared/auth';
import { AuthCallback } from './modules/auth';
import DashboardView from './modules/dashboard/DashboardView';
import DesignSystemView from './modules/design-system/DesignSystemView';
import CampaignsView from './modules/campaigns/CampaignsView';
import CampaignDetailsView from './modules/campaigns/CampaignDetailsView';
import { OnboardingView } from './modules/onboarding';
import Email from './modules/email/Email';
import LeadsView from './modules/leads/LeadsView';
import LeadDetailView from './modules/leads/LeadDetailsView/LeadDetailsView';
import { GeneralView } from './modules/my-account/general/GeneralView';
import { EmailView } from './modules/my-account/email/EmailView';
import { toast, ToastContainer } from 'react-toastify';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { BLACK, PRIMARY, TEXT_LIGHT_GRAY, GRAY_30 } from './shared/constants/colors';

const workspaceEndpoint = process.env.REACT_APP_WORKSPACE_ENDPOINT;
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

const theme = createTheme({
  /** Change the pallet of the color. */
  COLORS: {
    BLACK,
    PRIMARY,
    TEXT_LIGHT_GRAY,
    GRAY_30,
  },
  /** Change the custom components styles if it needed. */
  components: {
    dialog: {
      root: {
        'div:first-child > span': {
          fontSize: '16px',
        },
      },
    },
  },
});

class App extends React.PureComponent {
  onRequestSuccess = ({ operation }) => {
    const message = operation.getContext();

    if (message) {
      // eslint-disable-next-line no-console
      console.error(message);
    }
  };

  onRequestError = ({ graphQLErrors }) => {
    const hasGraphQLErrors = Array.isArray(graphQLErrors) && graphQLErrors.length > 0;

    if (hasGraphQLErrors) {
      graphQLErrors.forEach((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    }
  };

  renderContent = () => {
    return (
      <AsyncContent stretch>
        <Switch>
          <Route path="/auth" component={AuthCallback} />
          <Route>
            <Session>
              <Layout>
                <Switch>
                  <ProtectedRoute exact path="/email" component={Email} />
                  <ProtectedRoute exact path="/onboarding/:screen?" component={OnboardingView} />
                  <ProtectedRoute exact path="/dashboard" component={DashboardView} />
                  <ProtectedRoute exact path="/design-system" component={DesignSystemView} />
                  <ProtectedRoute exact path="/campaigns" component={CampaignsView} />
                  <ProtectedRoute exact path="/leads" component={LeadsView} />
                  <ProtectedRoute exact path="/leads/:id" component={LeadDetailView} />
                  <ProtectedRoute exact path="/campaigns/:id" component={CampaignDetailsView} />
                  <ProtectedRoute exact path="/settings/general" component={GeneralView} />
                  <ProtectedRoute exact path="/settings/email" component={EmailView} />
                  <Redirect to="/dashboard" />
                </Switch>
              </Layout>
            </Session>
          </Route>
          <Route render={() => <Redirect to="/auth" />} />
        </Switch>
      </AsyncContent>
    );
  };

  render() {
    const { onRequestError, onRequestSuccess } = this;

    return (
      <BrowserRouter>
        <BoostProvider theme={theme}>
          <Elements stripe={stripePromise}>
            <AppProvider
              uri={workspaceEndpoint}
              authClient={AuthClient}
              onRequestSuccess={onRequestSuccess}
              onRequestError={onRequestError}>
              {this.renderContent}
            </AppProvider>
          </Elements>
          <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
        </BoostProvider>
      </BrowserRouter>
    );
  }
}

export { App };
